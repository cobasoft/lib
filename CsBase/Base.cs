// License and Copyright: see License.cs
using System;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
//
namespace Cobasoft {
	//
	//
	///<summary> Contains miscellaneous tool methods. </summary>
	public static partial class Base {
		//
		//
		public static readonly RegexOptions RxOptionsCS = RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.Singleline;
		public static readonly RegexOptions RxOptions = RxOptionsCS | RegexOptions.IgnoreCase;
		//
		//
		public static uint GetHResult( Exception ex ) {
			return (uint) System.Runtime.InteropServices.Marshal.GetHRForException( ex );
		}
		//
		///<summary>Convert a byte array to a string of hexadecimal characters. Can be used to convert DB timestamp.</summary>
		public static string BuildHexString( byte[] array ) {
			if ( array.Length == 0 ) {
				return null;
			}
			System.Text.StringBuilder sb = new StringBuilder( array.Length * 40 );
			foreach ( byte by in array ) {
				sb.Append( ( (int) by ).ToString( "X2" ) );
			}
			return sb.ToString();
		}
		///<summary>Generic swap method</summary>
		public static void Swap<T>( ref T lhs, ref T rhs ) {
			T temp;
			temp = lhs;
			lhs = rhs;
			rhs = temp;
		}
		//
		///<summary>Get Signum of value: https://en.wikipedia.org/wiki/Sign_function</summary>
		public static Tri Sgn<T>( T value ) where T : IComparable<T> {
			var t = value.CompareTo( default( T ) );
			return t < 0 ? Tri.Negative : t > 0 ? Tri.Positive : Tri.Zero;
		}
		//
		//
		private static RandomNumberGenerator _RNG = null;
		//
		///<summary>Get a random ulong</summary>
		public static ulong RandomULong() {
			if ( _RNG is null ) {
				_RNG = RandomNumberGenerator.Create();
			}
			var a = new byte[ 8 ];
			_RNG.GetBytes( a );
			var l = BitConverter.ToUInt64( a, 0 );
			return l;
		}
		///<summary>Get a random key as string, containing only readable characters from A-Z.</summary>
		public static string RandomKey() {
			//do {
			var ul = RandomULong();
			var s = KeyCode.EncodeKey( ul );
			return s;
			//if ( s.Contains( '@' ) || s.Contains( '$' ) ) return s;
			//} while ( true );
		}
		//
		///<summary>Check if any parameter is filled.</summary>
		public static bool AnyFilled( params string[] v ) {
			if ( v is not null ) {
				for ( int i = 0, n = v.Length; i < n; i++ ) {
					if ( !v[ i ].IsNullOrEmpty() ) return true;
				}
			}
			return false;
		}
		//
		///<summary>Check that all strings are not null or empty.</summary>
		public static bool AllFilled( params string[] v ) {
			if ( v is not null ) {
				for ( int i = 0, n = v.Length; i < n; i++ ) {
					if ( v[ i ].IsNullOrEmpty() ) return false;
				}
				return true;
			}
			return false;
		}
		//
		//
		//
		//
		public struct UnitName {
			//
			public long Value;
			public string Shortcut;
			public string Name;
			//
			public UnitName( long p1, string p2, string p3 ) {
				this.Value = p1;
				this.Shortcut = p2;
				this.Name = p3;
			}
		}
		//
		//
		// http://shareittips.com/5-our-stuffs/useful-info/computer-storage-space-measurements/
		//
		private static UnitName[] UnitNames =  {
			new UnitName( 1, "B", "Byte" ),
			new UnitName(  1000L, "kB", "Kilobyte" ),
			new UnitName(  1000L * 1000L, "MB", "Megabyte" ),
			new UnitName(  1000L * 1000L * 1000L, "GB", "Gigabyte" ),
			new UnitName(  1000L * 1000L * 1000L * 1000L, "TB", "Terabyte" ),
			new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L, "PB", "Petabyte" ),
			new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "EB", "Exabyte" ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "ZB", "Zettabyte" ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "YB", "Yottabyte" ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "BB", "Brontobyte" ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "PB", "Geopbyte"  ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "SB", "Saganbyte" ),
			//new UnitName(  1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L * 1000L, "CB", "EpicByte" )
		};
		//
		//
		///<summary>Provide more readable formatting for large file-size numbers.</summary>
		public static string ByteFormat( long value, int decimals = 3 ) {
			try {
				if ( 0 == value ) return "0 B";
				decimals = Math.Max( 0, Math.Min( 9, decimals ) );
				var format = 0 == decimals ? CS.N0 : "0." + new string( CC.N0, decimals );
				var I = UnitNames.Length;
				for ( int i = 0; i < I; i++ ) {
					var abs = Math.Abs( value );
					var un = UnitNames[ i ];
					if ( i == ( I - 1 ) || ( abs >= un.Value && abs < UnitNames[ i + 1 ].Value ) ) {
						double d = (double) value / (double) un.Value;
						return d.ToString( 0 == i ? CS.N0 : format ) + CS.SP + un.Shortcut;
					}
				}
				return value.ToString( "N" ) + CS.SP + "B";
			} catch ( Exception ex ) {
				Log.Exception( "ByteFormat", ex );
			}
			return CS.Empty;
		}
		//
		//
		///<summary>Provide more readable formatting for large file-size numbers.</summary>
		public static string ByteFormat( object o, int decimals = 3 ) {
			if ( o is not null ) {
				if ( o is long ) {
					return ByteFormat( (long) o, decimals );
				}
			}
			return CS.Empty;
		}
		//
		//
		///<summary>Produces an integer array filled with values from the given range, incremented by step.</summary>
		public static int[] NumberSequence( int from, int to, int step ) {
			var n = 1 + ( to - from ) / step;
			var a = new int[ n ];
			for ( int i = from; i <= to; i += step ) {
				a[ i - 1 ] = i;
			}
			return a;
		}
		//
		///<summary>Contains WebUtility.HtmlEncode, but also encodes CR and LF.</summary>
		public static string HtmlEncode( string s ) {
			if ( s is null || 0 == s.Length ) return CS.Empty;
			s = WebUtility.HtmlEncode( s );
			if ( s.Contains( CC.CR ) || s.Contains( CC.LF ) ) {
				s = s.Replace( CS.CrLf, CS.htmlBR ).Replace( CS.CR, CS.htmlBR ).Replace( CS.LF, CS.htmlBR );
			}
			return s;
		}
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Combine a and b into 64-bit key.</summary>
		public static long MakeKey( uint a, uint b ) {
			ulong k = ( (ulong) a ) << 32 | b;
			return (long) k;
		}
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Combine a and b into 64-bit key.</summary>
		public static long MakeKey( int a, int b ) {
			return MakeKey( (uint) a, (uint) b );
		}
		//
		///<summary>Split 64-bit value into 32-bit parts.</summary>
		public static void SplitKey( long key, out int a, out int b ) {
			a = (int) ( ( (ulong) key ) >> 32 );
			b = (int) key;
		}
#if NET7_0_OR_GREATER
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Combine a and b into 128-bit key.</summary>
		public static Int128 MakeKey16( ulong a, ulong b ) {
			UInt128 k = ( (UInt128) a ) << 64 | b;
			return (Int128) k;
		}
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Combine a and b into 128-bit key.</summary>
		public static Int128 MakeKey16( long a, long b ) {
			return MakeKey16( (ulong) a, (ulong) b );
		}
		//
		///<summary>Split 128-bit value into 64-bit parts.</summary>
		public static void SplitKey16( Int128 key, out long a, out long b ) {
			a = (long) ( ( (UInt128) key ) >> 64 );
			b = (long) key;
		}
#endif
		//
		//
		//
		//
		public struct CharStr {
			public char C;
			public string S;
			//
			public CharStr( char c, string s ) {
				this.C = c;
				this.S = s;
			}
		}
		public static string ReplaceCharacters( CharStr[] a, string s ) {
			var sb = new StringBuilder(); var found = false;
			for ( var i = 0; i < s.Length; i++ ) {
				var c = s[ i ];
				for ( var j = 0; j < a.Length; j++ ) {
					var f = a[ j ].C;
					var t = a[ j ].S;
					if ( c == f ) {
						sb.Append( t );
						found = true;
						break;
					}
				}
				if ( !found ) sb.Append( c );
			}
			return sb.ToString();
		}
		//
		//
		//
		//
		// GET VALUE FROM OBJECT
		//
		//
		///<summary>Convert object to long.</summary>
		public static long GetLong( object o, long fallback = 0 ) {
			if ( o is null ) return fallback;
			if ( o is long ) return (long) o;
			if ( o is int ) return (int) o;
			if ( o is string ) {
				var l = ( o as string ).ToLong();
				return l;
			}
			return fallback;
		}
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Convert object to int.</summary>
		public static int GetInt( object o, int fallback = 0 ) {
			return (int) GetLong( o, fallback );
		}
		//
		//
		///<summary>Convert seconds into ticks</summary>
		public static long Second2Ticks( long v ) {
			return 10000000L * v;
		}
		///<summary>Convert ticks into seconds</summary>
		public static long Ticks2Second( long v ) {
			return v / 10000000L;
		}
		//
		///<summary>Convert deciseconds into ticks</summary>
		public static long DeciSecond2Ticks( long v ) {
			return 1000000L * v;
		}
		///<summary>Convert ticks into deciseconds</summary>
		public static long Ticks2DeciSecond( long v ) {
			return v / 1000000L;
		}
		//
		///<summary>Convert msecs into ticks</summary>
		public static long Msec2Ticks( long v ) {
			return 10000L * v;
		}
		///<summary>Convert ticks into msecs</summary>
		public static long Ticks2Msec( long v ) {
			return v / 10000L;
		}
		///<summary>Format ticks</summary>
		public static string FormatTicks( long v ) {
			return new DateTime( v ).ymd2hms();
		}
	}
	//
	//
	//
	//
	//
	///<summary>A pair of objects.</summary>
	public class Pair<Ta, Tb> {
		// Fields
		public Ta A;
		public Tb B;
		//
		public Pair() {
			A = default( Ta );
			B = default( Tb );
		}
		public Pair( Ta a, Tb b ) {
			A = a;
			B = b;
		}
		//
		public override string ToString() {
			var sa = new string[] {
				CS.B1,
				A is null ? CS.null_ : A.ToString(),
				CS.CmSp,
				B is null ? CS.null_ : B.ToString(),
				CS.B2 };
			return String.Join( CS.Empty, sa );
		}
	}
	//
	///<summary>A quadruple of objects.</summary>
	public class Quadruple<Ta, Tb, Tc, Td> {
		// Fields
		public Ta A;
		public Tb B;
		public Tc C;
		public Td D;
		//
		public Quadruple() {
			A = default( Ta );
			B = default( Tb );
			C = default( Tc );
			D = default( Td );
		}
		public Quadruple( Ta a, Tb b, Tc c, Td d ) {
			A = a;
			B = b;
			C = c;
			D = d;
		}
		//
		public override string ToString() {
			var sa = new string[] { CS.B1, A.ToString(), B.ToString(), C.ToString(), D.ToString(), CS.B2 };
			return String.Join( CS.CM, sa );
		}
	}
	//
	///<summary>A sextuple of objects.</summary>
	public class Sextuple<Ta, Tb, Tc, Td, Te, Tf> {
		// Fields
		public Ta A;
		public Tb B;
		public Tc C;
		public Td D;
		public Te E;
		public Tf F;
		//
		public Sextuple() {
			A = default( Ta );
			B = default( Tb );
			C = default( Tc );
			D = default( Td );
			E = default( Te );
			F = default( Tf );
		}
		public Sextuple( Ta a, Tb b, Tc c, Td d, Te e, Tf f ) {
			A = a;
			B = b;
			C = c;
			D = d;
			E = e;
			F = f;
		}
		//
		public override string ToString() {
			var sa = new string[] { CS.B1, A.ToString(), B.ToString(), C.ToString(), D.ToString(), E.ToString(), F.ToString(), CS.B2 };
			return String.Join( CS.CM, sa );
		}
	}
	//
	//
	//
	///<summary>Encode a long, with only readable characters, fixed length.</summary>
	public static class KeyCode {
		//
		public const int Length = 11;
		//
		// This is a modified base64 lookup table, URL compatible.
		private readonly static char[] codes = new char[ 64 ] {
		'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
		'0','1','2','3','4','5','6','7','8','9','@','$' };
		//
		public readonly static string Codes = new string( codes );
		//
		//
		public static byte[] Encode( ulong key ) {
			var result = new byte[ Length ];
			ushort us;
			for ( int i = 0; i < Length; i++ ) {
				us = (ushort) ( key % 64 );
				key = key >> 6;
				result[ i ] = (byte) codes[ us ];
			}
			return result;
		}
		//
		public static ulong Decode( byte[] ca ) {
			if ( ca.Length != Length ) throw new ArgumentException( "The input array has a bad length." );
			ulong result = 0;
			ushort us = 0; byte c;
			for ( int i = 0; i < Length; i++ ) {
				c = ca[ i ];
				if ( c >= 'A' && c <= 'Z' ) us = (ushort) ( c - 'A' );
				else if ( c >= 'a' && c <= 'z' ) us = (ushort) ( c - (byte) 'a' + 26 );
				else if ( c >= '0' && c <= '9' ) us = (ushort) ( c - (byte) '0' + 52 );
				else if ( c == '@' ) us = 62;
				else if ( c == '!' ) us = 62; // backward compatibility
				else if ( c == '$' ) us = 63;
				else throw new ArgumentException( "The input array contains bad characters." );
				result |= (ulong) us << ( i * 6 );
			}
			return result;
		}
		//
		// KeyCode Convenience Methods
		public static string EncodeKey( ulong key ) {
			return Encoding.ASCII.GetString( Encode( key ) );
		}
		//
		public static ulong DecodeKey( string key ) {
			var ca = Encoding.ASCII.GetBytes( key );
			return Decode( ca );
		}
	}
	//
	//
	///<summary>Long primary-key interface.</summary>
	public interface IPrimaryKey8 {
		//
		///<summary>Get long primary key.</summary>
		public long GetPK8();
	}
	///<summary>Int128 primary-key interface.</summary>
	public interface IPrimaryKey16 {
		//
		///<summary>Get Int128 primary key.</summary>
		public long GetPK16();
	}
	//
}
