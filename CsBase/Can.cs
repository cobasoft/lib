﻿// License and Copyright: see License.cs
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
//
namespace Cobasoft {
	//
	[Serializable()]
	[Flags]
	public enum Can : ushort {
		Nothing = 0x0000,           // No rights.
		List = 0x0001,              // See object in a list. Right to see something.	
		Info = 0x0002,              // See object properties. Right to see the properties of something. Equivalent to read-only.
		View = 0x0004,              // See object contents. (Download contents).
		Print = 0x0008,             // Print objects contents. (Kind of equivalent with View).
		Share = 0x0010,             // Put objects into collections. Share something.
		Classify = 0x0020,          // Edit object properties.
		Edit = 0x0040,              // Edit object contents. Right to edit/modify something.
		Create = 0x0080,            // Create new objects.
		Upload = 0x0100,            // Create new objects by uploading.
		Version = 0x0200,           // Versioning: Check-out, edit, check-in contents.
		Remove = 0x0400,            // Remove objects without destroying them permanently.
		Destroy = 0x0800,           // Destroy objects permanently.
		Everything = 0xFFFF,        // Administrator.
		AllRead = Can.List | Can.Info | Can.View | Can.Print,
		AllWrite = Can.Classify | Can.Edit,
		AllCreate = Can.Create | Can.Upload,
		AllDelete = Can.Remove | Can.Destroy,
		UptoInfo = Can.List | Can.Info,
		UptoView = Can.UptoInfo | Can.View,
		UptoPrint = Can.UptoView | Can.Print,
		UptoShare = Can.UptoPrint | Can.Share,
		UptoClassify = Can.UptoShare | Can.Classify,
		UptoEdit = Can.UptoClassify | Can.Edit,
		UptoCreate = Can.UptoEdit | Can.Create,
		UptoUpload = Can.UptoCreate | Can.Upload,
		UptoVersion = Can.UptoUpload | Can.Version,
		UptoRemove = Can.UptoVersion | Can.Remove,
		UptoDestroy = Can.UptoRemove | Can.Destroy,
	};
	//
	//
	//
	public static class CanEx {
		//
		public static readonly string Nothing = "Nothing";
		public static readonly string List = "List";
		public static readonly string Info = "Info";
		public static readonly string View = "View";
		public static readonly string Print = "Print";
		public static readonly string Share = "Share";
		public static readonly string Classify = "Classify";
		public static readonly string Edit = "Edit";
		public static readonly string Create = "Create";
		public static readonly string Upload = "Upload";
		public static readonly string Version = "Version";
		public static readonly string Remove = "Remove";
		public static readonly string Destroy = "Destroy";
		public static readonly string Everything = "Everything";
		//
		public static readonly string nothing = "nothing";
		public static readonly string list = "list";
		public static readonly string info = "info";
		public static readonly string view = "view";
		public static readonly string print = "print";
		public static readonly string share = "share";
		public static readonly string classify = "classify";
		public static readonly string edit = "edit";
		public static readonly string create = "create";
		public static readonly string upload = "upload";
		public static readonly string version = "version";
		public static readonly string remove = "remove";
		public static readonly string destroy = "destroy";
		public static readonly string everything = "everything";
		//
		public static readonly string[] CanUpper = new string[] {
			List,Info,View,Print,Share,Classify,Edit,Create,Upload,Version,Remove,Destroy,Everything
		};
		//
		public static readonly string[] CanLower = new string[] {
			list,info,view,print,share,classify,edit,create,upload,version,remove,destroy,everything
		};
		//
		///<summary>Serialize Can to JSON. If name is true, the object is prefixed by the name.</summary>
		public static JsonBuilder ToJson( this JsonBuilder jb, bool name, Can can ) {
			if ( name ) jb.N( "can" );
			jb.Object();
			for ( int i = 0; i < CanLower.Length; i++ ) {
				jb.N( CanLower[ i ] ).V( Bit.Get( (ushort) can, i ) );
			}
			return jb.Close();
		}
		//
		///<summary>Check if all bits are matched.</summary>
		public static bool All( this Can a, Can b ) {
			return Bit.All( (ushort) a, (ushort) b );
		}
		//
		///<summary>Check if any bit is matched.</summary>
		public static bool Any( this Can a, Can b ) {
			return Bit.Any( (ushort) a, (ushort) b );
		}
		//
		///<summary>Set one or more bit. It never clears bits!</summary>
		public static Can Set( this Can a, Can b ) {
			return (Can) ( (ushort) a | (ushort) b );
		}
		//
		///<summary>Clear one or more bit.</summary>
		public static Can Clear( this Can a, Can b ) {
			return (Can) ( (ushort) a & ~(ushort) b );
		}
	}
	//
	//
	//
	///<summary>Provide permissions lookup.</summary>
	public class Permissions {
		//
		///<summary>Reader-writer lock.</summary>
		protected ReaderWriterLockSlim _rwl = new ReaderWriterLockSlim( LockRecursionPolicy.SupportsRecursion );
		//
		///<summary>The permissions dictionary.</summary>
		private readonly Dictionary<long, Can> _dictionary = new Dictionary<long, Can>();
		//
		///<summary>The login to user-ID dictionary.</summary>
		private readonly Dictionary<string, int> _logins = new Dictionary<string, int>();
		//
		///<summary>The user-id to login dictionary.</summary>
		private readonly Dictionary<int, string> _ids = new Dictionary<int, string>();
		//
		///<summary>The alias to user-ID dictionary.</summary>
		private readonly Dictionary<string, int> _aliases = new Dictionary<string, int>();
		//
		///<summary>Add 'Can' permission to the dictionary, for the user ID and table. The name is trimmed and made lowercase.</summary>
		public void Set( int table, string name, int userID, Can can ) {
			var key = Base.MakeKey( table, userID );
			name = name.Trim().ToLowerInvariant();
			using ( new Lock( _rwl, Lock.LockType.Writer ) ) {
				_dictionary[ key ] = can;
				_logins[ name ] = userID;
				_ids[ userID ] = name;
			}
		}
		//
		///<summary>Get 'Can' permission from the dictionary, for the user ID and table.</summary>
		public Can Get( int table, int userID ) {
			var key = Base.MakeKey( table, userID );
			using ( new Lock( _rwl, Lock.LockType.Reader ) ) {
				if ( _dictionary.ContainsKey( key ) ) {
					return _dictionary[ key ];
				}
			}
			return Can.Nothing;
		}
		//
		///<summary>Get 'Can' permission from the dictionary, for the login and table.</summary>
		public Can Get( int table, string name ) {
			name = name.Trim().ToLowerInvariant();
			using ( new Lock( _rwl, Lock.LockType.Reader ) ) {
				if ( _logins.ContainsKey( name ) ) {
					var userID = _logins[ name ];
					var key = Base.MakeKey( table, userID );
					if ( _dictionary.ContainsKey( key ) ) {
						return _dictionary[ key ];
					}
				}
			}
			return Can.Nothing;
		}
		//
		//
		//
		static readonly Regex _RxLoginDomain1 = new Regex( ".*\\\\", Base.RxOptions );
		static readonly Regex _RxLoginDomain2 = new Regex( "@.*", Base.RxOptions );
		//
		//
		///<summary>Convert username into canonical form. Lowercase, replace all umlauts, remove domain and spaces.</summary>
		public static string MakeCanonicalLogin( string name ) {
			if ( Log.Deny( name.IsNullOrEmpty() ) ) return null;
			name = name.ExpandUmlauts();
			name = name.ToLowerInvariant();
			if ( name.Contains( CC.SP ) ) name = name.Replace( CS.SP, CS.Empty );
			if ( name.Contains( CC.BS ) ) name = _RxLoginDomain1.Replace( name, CS.Empty );
			if ( name.Contains( CC.AT ) ) name = _RxLoginDomain2.Replace( name, CS.Empty );
			return name;
		}
		//
		///<summary>Look up the name, get the userID.</summary>
		public int Lookup( string name ) {
			if ( name.IsNullOrEmpty() ) return 0;
			name = MakeCanonicalLogin( name );
			using ( new Lock( _rwl, Lock.LockType.Reader ) ) {
				if ( !_logins.TryGetValue( name, out int id ) ) {
					if ( !_aliases.TryGetValue( name, out id ) ) {
						Log.WarningV( Log.CM(), "Permissions: User not found", name );
						return 0;
					}
				}
				return id;
			}
		}
		//
		///<summary>Look up the id, get the name.</summary>
		public string Lookup( int id ) {
			if ( 0 < id ) {
				using ( new Lock( _rwl, Lock.LockType.Reader ) ) {
					if ( _ids.ContainsKey( id ) ) {
						return _ids[ id ];
					}
				}
			}
			//Log.WarningV( Log.CM(), "Permissions: User not found", name );
			return null;
		}
		//
		///<summary>Store Login/UserID correlation. All names are stored unchanged. You should use MakeCanonicalLogin on all names before calling this method.</summary>
		public void Users( string[] userNames, int[] userIDs ) {
			Log.DataV( Log.CM(), "userNames", userNames );
			if ( Log.Assert( userNames is not null && userIDs is not null && userNames.Length == userIDs.Length ) ) {
				using ( new Lock( _rwl, Lock.LockType.Writer ) ) {
					// For every name, include it in the dictionary, if it does not already exist.
					for ( int i = 0, n = userNames.Length; i < n; i++ ) {
						var name = userNames[ i ];
						var id = userIDs[ i ];
						if ( name.IsFilled() ) {
							if ( !_logins.ContainsKey( name ) ) {
								_logins[ name ] = id;
								_ids[ id ] = name;
							}
						}
					}
				}
			}
		}
		//
		///<summary>Store Login aliases. All names are stored unchanged. You should use MakeCanonicalLogin on all names before calling this method.</summary>
		public void Aliases( string[] userNames, int[] userIDs ) {
			Log.DataV( Log.CM(), "Permissions: domain", userNames );
			if ( Log.Assert( userNames is not null && userIDs is not null && userNames.Length == userIDs.Length ) ) {
				using ( new Lock( _rwl, Lock.LockType.Writer ) ) {
					// Put aliases into the dictionary.
					for ( int i = 0, n = userNames.Length; i < n; i++ ) {
						var name = userNames[ i ];
						var id = userIDs[ i ];
						if ( name.IsFilled() ) {
							if ( !_aliases.ContainsKey( name ) ) {
								_aliases[ name ] = id;
							}
						}
					}
				}
			}
		}
	}
	//
	//
	//
	//
	public static class UserCan {
		//
		private static readonly Permissions _permissions = new Permissions();
		//
		///<summary>Add 'Can' permission to the dictionary, for the user ID and table.</summary>
		public static void Store( int table, string name, int userID, Can can ) {
			if ( Log.Assert( 0 < table && name.IsFilled() && 0 < userID ) ) {
				_permissions.Set( table, name, userID, can );
			}
		}
		//
		///<summary>Store the login / userID correlation.</summary>
		public static void Users( string[] userNames, int[] userIDs, string domain ) {
			Log.DataV( Log.CM(), "UserCan: userNames", userNames.Length, "userIDs", userIDs.Length, "domain", domain );
			if ( Log.Assert( userNames.IsFilled() && userIDs.IsFilled() && userNames.Length == userIDs.Length && domain.IsFilled() ) ) {
				_permissions.Users( userNames, userIDs );
			}
		}
		//
		///<summary>Store aliases for login / userID correlation.</summary>
		public static void Aliases( string[] userNames, int[] userIDs ) {
			Log.DataV( Log.CM(), "UserCan: userNames", userNames.Length, "userIDs", userIDs.Length );
			if ( Log.Assert( userNames.IsFilled() && userIDs.IsFilled() && userNames.Length == userIDs.Length ) ) {
				_permissions.Aliases( userNames, userIDs );
			}
		}
		//
		///<summary>Look up the name, get the userID.</summary>
		public static int Lookup( string name ) {
			return _permissions.Lookup( name );
		}
		//
		///<summary>Look up the id, get the user name.</summary>
		public static string Lookup( int id ) {
			return _permissions.Lookup( id );
		}
		//
		///<summary>Get 'Can' permission from the dictionary, for the user ID and table.</summary>
		public static Can Get( int table, int userID ) {
			return _permissions.Get( table, userID );
		}
		//
		///<summary>Get 'Can' permission from the dictionary, for the user and table.</summary>
		public static Can Get( int table, string name ) {
			return _permissions.Get( table, name );
		}
		//
		///<summary>Check 'Can' permission from the dictionary, for the user and table.</summary>
		public static bool Any( int table, int userID, Can can ) {
			return _permissions.Get( table, userID ).Any( can );
		}
		//
		///<summary>Check 'Can' permission from the dictionary, for the user and table.</summary>
		public static bool Any( int table, string name, Can can ) {
			return _permissions.Get( table, name ).Any( can );
		}
		//
		///<summary>Check 'Can' permission from the dictionary, for the user and table.</summary>
		public static bool All( int table, int userID, Can can ) {
			return _permissions.Get( table, userID ).All( can );
		}
		//
		///<summary>Check 'Can' permission from the dictionary, for the user and table.</summary>
		public static bool All( int table, string name, Can can ) {
			return _permissions.Get( table, name ).All( can );
		}
		//
		///<summary>Check if this user is administrator for this table.</summary>
		public static bool Admin( int table, int userID ) {
			return _permissions.Get( table, userID ).All( Can.Everything );
		}
		//
		///<summary>Check if this user is administrator for this table.</summary>
		public static bool Admin( int table, string name ) {
			return _permissions.Get( table, name ).All( Can.Everything );
		}
	}
}
