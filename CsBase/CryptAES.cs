﻿// License and Copyright: see License.cs
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
//
namespace Cobasoft {
	//
	///<summary>Simple string encryption and decryption.</summary>
	public sealed class CryptAES : IDisposable {
		//
		private static byte[] _Default = null;
		//
		private SymmetricAlgorithm _alg;
		//
		static CryptAES() {
			_Default = Encoding.ASCII.GetBytes( "@Aes.Encryption@" );
			Log.Assert( 16 == _Default.Length );
		}
		//
		///<summary>Constructor.</summary>
		public CryptAES( byte[] key ) {
			// Define the secret key
			if ( Log.Deny( key is null || 16 != key.Length ) ) key = null;
			if ( key.IsNullOrEmpty() ) key = _Default;
			// Set the key and iv
			const int bytes = 128 / 8; // 16
			var iv = new byte[ bytes ];
			Array.Copy( key, 0, iv, 0, bytes );
			// Initialize the algorithm
			_alg = Aes.Create();
			_alg.KeySize = 128;
			_alg.BlockSize = 128;
			_alg.Key = key;
			_alg.IV = iv;
			_alg.Padding = PaddingMode.Zeros;
		}
		//
		///<summary>Constructor.</summary>
		public CryptAES( string key ) : this( Encoding.ASCII.GetBytes( key ) ) { }
		//
		//
		///<summary>
		///Encrypt a string, using the default key and the RC2 encoding.
		///</summary>
		public byte[] Encrypt( string message ) {
			if ( message is null || message.Length == 0 ) return null;
			try {
				// convert the message into a byte array
				byte[] plaintext = Encoding.Unicode.GetBytes( message );
				// create a memory stream to hold the cipher text
				MemoryStream memory_stream = new MemoryStream();
				// create a cryptostream, using the Encryptor
				CryptoStream stream = new CryptoStream(
					memory_stream,
					_alg.CreateEncryptor(),
					CryptoStreamMode.Write );
				// encrypt the plaintext by writing it to the cryptostream
				stream.Write( plaintext, 0, plaintext.Length );
				stream.FlushFinalBlock();
				// extract the cipher from the cryptostream
				byte[] cipher = memory_stream.ToArray();
				// return the result
				return cipher;
			} catch ( Exception ex ) {
				Log.Exception( "Encrypt", ex );
			}
			return null;
		}
		//
		//
		///<summary>
		///Encrypt a string, using the default key and the RC2 encoding.
		///Return the encrypted wide array as hexadecimal string.
		///</summary>
		public string EncryptHex( string message ) {
			return HexEx.ToHex( Encrypt( message ) );
		}
		///<summary>
		///Decrypts a byte array, using the default key and the RC2 encoding.
		///</summary>
		public string Decrypt( byte[] cipher ) {
			if ( cipher is null || cipher.Length == 0 ) return null;
			try {
				// create a memory stream to hold the plaintext
				MemoryStream memory_stream = new MemoryStream();
				// create a cryptostream, using an XTEA Decryptor
				CryptoStream stream = new CryptoStream(
					memory_stream,
					_alg.CreateDecryptor(),
					CryptoStreamMode.Write );
				// decrypt the cipher by writing it to the cryptostream
				stream.Write( cipher, 0, cipher.Length );
				stream.FlushFinalBlock();
				// extract the plaintext from the memory stream
				byte[] plaintext = memory_stream.ToArray();
				var s = Encoding.Unicode.GetString( plaintext );
				return s.Trim( '\0' );
			} catch ( Exception ex ) {
				Log.Exception( "Decrypt", ex );
			}
			return null;
		}
		//
		//
		///<summary>
		///	Message is a byte array, coded as hexadecimal string.
		/// Message is decoded, using the default key and the RC2 encoding.
		///</summary>
		public string DecryptHex( string message ) {
			return Decrypt( HexEx.FromHex( message ) );
		}
		//
		//
		public void Dispose() {
			if ( _alg is not null ) {
				_alg.Dispose();
				this._alg = null;
			}
		}
		//
		// Convert the input string to a byte array and compute the hash.
		// You can use ArrayEx.IsEqual or ArrayEx.CompareTo for the comparison of hashes.
		public static byte[] Hash( SHA1 alg, string value ) {
			return alg.ComputeHash( Encoding.UTF8.GetBytes( value ) );
		}
		//
	}
	//
	//
	//
	//
	/////<summary>Basic asymmetric signature</summary>
	//public sealed class DsaSign : IDisposable {
	//	//
	//	private ECDsaCng _alg;
	//	private byte[] _publicKey;
	//	private byte[] _privateKey;
	//	//
	//	//
	//	public byte[] PublicKey { get { return _publicKey; } }
	//	public byte[] PrivateKey { get { return _privateKey; } }
	//	//
	//	//
	//	public void Dispose() {
	//		if ( _alg is not null ) {
	//			_alg.Dispose();
	//			_alg = null;
	//		}
	//	}
	//	//
	//	//
	//	///<summary>Initialize with random key.</summary>
	//	public DsaSign() {
	//		_alg = new ECDsaCng();
	//		_publicKey = _alg.Key.Export( CngKeyBlobFormat.GenericPublicBlob );
	//		_privateKey = _alg.Key.Export( CngKeyBlobFormat.GenericPrivateBlob );
	//	}
	//	//
	//	//
	//	///<summary>Initialize with given key.</summary>
	//	public DsaSign( byte[] pattern, bool @private ) {
	//		var key = CngKey.Import( pattern, @private ? CngKeyBlobFormat.GenericPrivateBlob : CngKeyBlobFormat.GenericPublicBlob );
	//		_alg = new ECDsaCng( key );
	//	}
	//	//
	//	//
	//	public bool IsEqual( DsaSign other ) {
	//		var pk1 = this._publicKey;
	//		var sk1 = this._privateKey;
	//		var pk2 = this._publicKey;
	//		var sk2 = this._privateKey;
	//		return pk1.IsEqual( pk2 ) && sk1.IsEqual( sk2 );
	//	}
	////
	////
	//public byte[] Encrypt( string message ) {
	//	if ( message is null || message.Length == 0 ) return null;
	//	try {
	//		// convert the message into a byte array
	//		byte[] plaintext = Encoding.Unicode.GetBytes( message );
	//		// Sign message
	//		var signature = _alg.SignData( plaintext,h );
	//		// return the result
	//		return cipher;
	//	} catch ( Exception ex ) {
	//		Log.Exception( "Encrypt", ex );
	//	}
	//	return null;
	//}
	////
	////
	//public string Decrypt( byte[] cipher ) {
	//	if ( cipher is null || cipher.Length == 0 ) return null;
	//	try {
	//		// create a memory stream to hold the plaintext
	//		MemoryStream memory_stream = new MemoryStream();
	//		// create a cryptostream, using an XTEA Decryptor
	//		CryptoStream stream = new CryptoStream(
	//			memory_stream,
	//			_alg.CreateDecryptor(),
	//			CryptoStreamMode.Write );
	//		// decrypt the cipher by writing it to the cryptostream
	//		stream.Write( cipher, 0, cipher.Length );
	//		stream.FlushFinalBlock();
	//		// extract the plaintext from the memory stream
	//		byte[] plaintext = memory_stream.ToArray();
	//		var s = Encoding.Unicode.GetString( plaintext );
	//		return s.Trim( '\0' );
	//	} catch ( Exception ex ) {
	//		Log.Exception( "Decrypt", ex );
	//	}
	//	return null;
	//}
	////
	//public static byte[] Encrypt2( string message ) {
	//	if ( message is null || message.Length == 0 ) return null;
	//	var c = new AesEncryption();
	//	return c.Encrypt( message );
	//}
	////
	////
	//public static string Decrpyt2( byte[] cipher ) {
	//	if ( cipher is null || cipher.Length == 0 ) return null;
	//	var c = new AesEncryption();
	//	return c.Decrypt( cipher );
	//}
	////
	//}
	//
}
