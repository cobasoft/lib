﻿// License and Copyright: see License.cs
//using System;
//using System.Security.Cryptography;
//using System.Text;
////
//namespace Cobasoft {
//	//
//	///<summary>Basic asymmetric encryption and decryption</summary>
//	public sealed class CryptRSA : IDisposable {
//		//
//		private RSACng _alg;
//		private byte[] _publicKey;
//		private byte[] _privateKey;
//		//
//		//
//		public byte[] PublicKey { get { return _publicKey; } }
//		public byte[] PrivateKey { get { return _privateKey; } }
//		//
//		//
//		public void Dispose() {
//			if ( _alg is not null ) {
//				_alg.Dispose();
//				_alg = null;
//			}
//		}
//		//
//		//
//		///<summary>Initialize with random key.</summary>
//		public CryptRSA() {
//			_alg = new RSACng();
//			_publicKey = _alg.Key.Export( CngKeyBlobFormat.GenericPublicBlob );
//			_privateKey = _alg.Key.Export( CngKeyBlobFormat.GenericPrivateBlob );
//		}
//		//
//		//
//		///<summary>Initialize with given key.</summary>
//		public CryptRSA( byte[] pattern, bool @private ) {
//			var key = CngKey.Import( pattern, @private ? CngKeyBlobFormat.GenericPrivateBlob : CngKeyBlobFormat.GenericPublicBlob );
//			_alg = new RSACng( key );
//		}
//		//
//		//
//		public bool IsEqual( CryptRSA other ) {
//			var pk1 = this._publicKey;
//			var sk1 = this._privateKey;
//			var pk2 = this._publicKey;
//			var sk2 = this._privateKey;
//			return pk1.IsEqual( pk2 ) && sk1.IsEqual( sk2 );
//		}
//		//
//		//
//		public byte[] Encrypt( string message ) {
//			if ( message is null || message.Length == 0 ) return null;
//			try {
//				// Convert the message into a byte array
//				byte[] bytes = Encoding.UTF8.GetBytes( message );
//				// Encrypt message
//				var cipher = _alg.Encrypt( bytes, RSAEncryptionPadding.OaepSHA1 );
//				// return the result
//				return cipher;
//			} catch ( Exception ex ) {
//				Log.Exception( ex );
//			}
//			return null;
//		}
//		//
//		//
//		public string Decrypt( byte[] cipher ) {
//			if ( cipher is null || cipher.Length == 0 ) return null;
//			try {
//				// Encrypt message
//				var bytes = _alg.Decrypt( cipher, RSAEncryptionPadding.OaepSHA1 );
//				var s = Encoding.UTF8.GetString( bytes );
//				s = s.Trim( '\0' );
//				return s;
//			} catch ( Exception ex ) {
//				Log.Exception( "Decrypt", ex );
//			}
//			return null;
//		}
//		//
//		public static byte[] Encrypt2( string message ) {
//			if ( message is null || message.Length == 0 ) return null;
//			using ( var c = new CryptRSA() ) {
//				return c.Encrypt( message );
//			}
//		}
//		//
//		//
//		public static string Decrpyt2( byte[] cipher ) {
//			if ( cipher is null || cipher.Length == 0 ) return null;
//			using ( var c = new CryptRSA() ) {
//				return c.Decrypt( cipher );
//			}
//		}
//		//
//		//

//	}
//	//
//}
