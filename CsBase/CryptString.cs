﻿// License and Copyright: see License.cs
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
//
namespace Cobasoft {
	///<summary>
	/// Very simple string encryption and decryption
	/// </summary>
	public sealed class CryptString : IDisposable {
		//
		public static byte[] Key;
		//
		private SymmetricAlgorithm m_Alg;
		private byte[] m_IV;
		//
		///<summary> Constructor. </summary>
		public CryptString() {
			// create a new instance of the algorithm
			m_Alg = RC2.Create();
			// define the secret key
			if ( Key.IsNullOrEmpty() ) {
				Log.Warning( "CryptString: default key used" );
				Key = Encoding.Default.GetBytes( "!@#CryptString#@" );
			}
			m_IV = new byte[ 8 ];
			Array.Copy( Key, 0, m_IV, 0, 8 );
			// set the key and iv
			m_Alg.KeySize = 128;
			m_Alg.BlockSize = 64;
			m_Alg.Key = Key;
			m_Alg.IV = m_IV;
		}
		///<summary>
		/// Encrypt a string, using the default key and the RC2 encoding.
		/// </summary>
		public byte[] Encrpyt( string message ) {
			if ( message is null || message.Length == 0 ) return null;
			try {
				// convert the message into a byte array
				byte[] plaintext = Encoding.Default.GetBytes( message );
				// create a memory stream to hold the cipher text
				MemoryStream memory_stream = new MemoryStream();
				// create a cryptostream, using the Encryptor
				CryptoStream stream = new CryptoStream(
					memory_stream,
					m_Alg.CreateEncryptor(),
					CryptoStreamMode.Write );
				// encrypt the plaintext by writing it to the cryptostream
				stream.Write( plaintext, 0, plaintext.Length );
				stream.FlushFinalBlock();
				// extract the ciphertext from the cryptostream
				byte[] ciphertext = memory_stream.ToArray();
				// return the result
				return ciphertext;

			} catch ( Exception ex ) {
				Log.Exception( "Encrypt", ex );
			}
			return null;
		}
		///<summary>
		/// Decrypts a byte array, using the default key and the RC2 encoding.
		/// </summary>
		public string Decrypt( byte[] ciphertext ) {
			if ( ciphertext is null || ciphertext.Length == 0 ) return null;
			try {
				// create a memory stream to hold the plaintext
				MemoryStream memory_stream = new MemoryStream();
				// create a cryptostream, using an XTEA Decryptor
				CryptoStream stream = new CryptoStream(
					memory_stream,
					m_Alg.CreateDecryptor(),
					CryptoStreamMode.Write );
				// decrypt the ciphertext by writing it to the cryptostream
				stream.Write( ciphertext, 0, ciphertext.Length );
				stream.FlushFinalBlock();
				// extract the plaintext from the memory stream
				byte[] plaintext = memory_stream.ToArray();
				return Encoding.Default.GetString( plaintext );
			} catch ( Exception ex ) {
				Log.Exception( "Decrypt", ex );
			}
			return null;
		}
		//
		public static byte[] Encrypt2( string message ) {
			if ( message is null || message.Length == 0 ) return null;
			var c = new CryptString();
			return c.Encrpyt( message );
		}
		///<summary>
		/// Decrypts a byte array, using the default key and the RC2 encoding.
		/// </summary>
		public static string Decrypt2( byte[] ciphertext ) {
			if ( ciphertext is null || ciphertext.Length == 0 ) return null;
			var c = new CryptString();
			return c.Decrypt( ciphertext );
		}
		//
		public void Dispose() {
			if ( m_Alg is not null ) {
				m_Alg.Dispose();
				this.m_Alg = null;
			}
		}
		//
	}
}
