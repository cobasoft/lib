// License and Copyright: see License.cs
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Globalization;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
//
namespace Cobasoft {
	//
	// TASK: IndexOf and Contains must use StringComparison.OrdinalIgnoreCase
	//
	//
	public static partial class ArrayEx {
		//
		///<summary>Append one item to the end of an array. If the array is null, a new one is created.</summary>
		public static T[] Append<T>( this T[] a, T v ) {
			int l = 0;
			if ( a is not null )
				l = a.Length;
			int n = l + 1;
			var b = new T[ n ];
			if ( a is not null )
				Array.Copy( a, b, l );
			b[ l ] = v;
			return b;
		}
		//
		///<summary>Insert one item into an array. If the array is null, a new one is created.</summary>
		public static T[] Insert<T>( this T[] a, int pos, T v ) {
			int l = 0;
			if ( a is not null )
				l = a.Length;
			int n = l + 1;
			var b = new T[ n ];
			if ( a is not null ) {
				Array.Copy( a, b, pos );
				Array.Copy( a, pos, b, pos + 1, l - pos );
			}
			b[ pos ] = v;
			return b;
		}
		//
		///<summary>Delete one item from an array. If the array is null, null is returned.</summary>
		public static T[] Delete<T>( this T[] a, int pos ) {
			if ( a is null )
				return null;
			int l = a.Length;
			int n = l - 1;
			var b = new T[ n ];
			Array.Copy( a, b, pos );
			Array.Copy( a, pos + 1, b, pos, l - pos - 1 );
			return b;
		}
		//
		///<summary>Join two arrays together. At least one array must exist.</summary>
		public static T[] Append<T>( this T[] a1, T[] a2 ) {
			if ( a1 is null && a2 is null ) return null;
			int l1 = 0;
			if ( a1 is not null ) l1 = a1.Length;
			int l2 = 0;
			if ( a2 is not null ) l2 = a2.Length;
			var b = new T[ l1 + l2 ];
			if ( a1 is not null ) Array.Copy( a1, b, l1 );
			if ( a2 is not null ) Array.Copy( a2, 0, b, l1, l2 );
			return b;
		}
		//
		///<summary>Join an array into a string.</summary>
		public static string Join<T>( this T[] a1, string separator = null, int len = -1 ) {
			if ( a1 is null ) return null;
			if ( -1 == len ) len = a1.Length;
			if ( 0 == len ) return CS.Empty;
			var a2 = new string[ len ];
			T e;
			for ( int i = 0; i < len; i++ ) {
				e = a1[ i ];
				a2[ i ] = e is null ? CS.Empty : e.ToString();
			}
			if ( separator is null ) { separator = CS.Empty; }
			return string.Join( separator, a2 );
		}
		//
		//
		///<summary>Create a new array as concatenation of all given arrays.</summary>
		public static T[] Concatenate<T>( params T[][] args ) {
			if ( args is null ) return null;
			// Get argument lengths
			var count = args.Length;
			if ( 0 == count ) return null;
			var lengths = new int[ count ];
			// Compute all and total lengths
			var total = 0;
			for ( int i = 0; i < count; i++ ) {
				lengths[ i ] = args[ i ] is null ? 0 : args[ i ].Length;
				total += lengths[ i ];
			}
			if ( 1 > total ) return null;
			// Create target array
			T[] a = new T[ total ];
			// Copy all together
			total = 0;
			for ( int i = 0; i < count; i++ ) {
				if ( args[ i ] is not null ) {
					args[ i ].CopyTo( a, total );
				}
				total += lengths[ i ];
			}
			return a;
		}
		//
		///<summary>Shallow copy the array. Faster than Clone().</summary>
		public static T[] CloneFast<T>( this T[] a ) {
			if ( a is null ) return null;
			T[] b = new T[ a.Length ];
			a.CopyTo( b, 0 );
			return b;
		}
		//
		///<summary>Sort.</summary>
		public static T[] Sort<T>( this T[] a ) {
			Array.Sort( a );
			return a;
		}
		//
		///<summary>Remove duplicates. The array MUST be sorted before calling this method.</summary>
		public static T[] Unique<T>( this T[] a ) where T : IComparable<T> {
			if ( a is not null && 1 < a.Length ) {
				int write = 0; int read = 1; int last = a.Length;
				while ( read < last ) {
					if ( 0 == a[ write ].CompareTo( a[ read ] ) ) read++;
					else { a[ ++write ] = a[ read++ ]; }
				}
				Array.Resize( ref a, write + 1 );
			}
			return a;
		}
		//	int unique( int[] a, int n )   //sorted array
		//{
		//		int k = 0;
		//		for ( int i = 1; i < n; i++ ) {
		//			if ( a[ k ] != a[ i ] ) {              //comparing the first 2 unequal numbers
		//				a[ k + 1 ] = a[ i ];                 //shifting to the left if distinct
		//				k++;
		//			}
		//		}
		//		return ( k + 1 );             //return the last index of new array i.e. the number of distinct elements
		//	}
		///<summary>Check if array is not null and its length is greater than 0.</summary>
		public static bool IsFilled<T>( this T[] a ) {
			return a is not null && a.Length > 0;
		}
		//
		///<summary>Check if array is null or its length is 0.</summary>
		public static bool IsEmpty<T>( this T[] a ) {
			return a is null || a.Length <= 0;
		}
		///<summary>Check if array is null or its length is 0.</summary>
		public static bool IsNullOrEmpty<T>( this T[] a ) {
			return a is null || a.Length <= 0;
		}
		//
		///<summary>Create an array of T and initialize all members.</summary>
		public static T[] Create<T>( int n ) where T : new() {
			var a = new T[ n ];
			for ( int i = 0; i < n; i++ ) {
				a[ i ] = new T();
			}
			return a;
		}
		///<summary>Returns the index of v in this array or -1, if not found.</summary>
		public static int IndexOf<T>( this T[] a, T v ) where T : struct {
			for ( int i = 0; i < a.Length; i++ ) {
				if ( a[ i ].Equals( v ) ) return i;
			}
			return -1;
		}
		///<summary>Test if the array contains any of the parameter values.</summary>
		public static int IndexOf<T>( this T[] a, params T[] args ) where T : struct {
			if ( a != null && args != null ) {
				int l1 = a.Length;
				int l2 = args.Length;
				if ( 0 < l1 && 0 < l2 ) {
					for ( int i = 0; i < l1; i++ ) {
						for ( int j = 0; j < l2; j++ ) {
							if ( a[ i ].Equals( args[ j ] ) ) return i;
						}
					}
				}
			}
			return -1;
		}
		///<summary>Return true if v is in this array.</summary>
		public static bool Contains<T>( this T[] a, T v ) where T : struct {
			if ( a is null ) return false;
			return -1 < IndexOf( a, v );
		}
		///<summary>Return true if any of the parameters is in this array.</summary>
		public static bool Contains<T>( this T[] a, params T[] args ) where T : struct {
			return -1 < IndexOf( a, args );
		}
		//
		///<summary>Call. ToString() on all contained objects and return the result.  The separator might be null.</summary>
		public static string GetString<T>( this T[] a, string separator = CS.SP ) {
			if ( a is null ) return null;
			var buffer = new StringBuilder(); string s;
			for ( int i = 0; i < a.Length; i++ ) {
				s = a[ i ].ToString();
				buffer.Append( s );
				if ( separator is not null ) buffer.Append( separator );
			}
			return buffer.ToString();
		}
		//
		//
		public static bool IsEqual<T>( this T[] a1, T[] a2 ) where T : IEquatable<T> {
			if ( a1 is null ) {
				if ( a2 is null ) return true;
				return false;
			}
			if ( a2 is null ) {
				return false;
			}
			if ( a1.Length != a2.Length ) return false;
			//
			for ( int i = 0; i < a1.Length; i++ ) {
				var e = a1[ i ].Equals( a2[ i ] );
				if ( !e ) return false;
			}
			return true;
		}
		//
		//
		public static int CompareTo<T>( this T[] a1, T[] a2 ) where T : IComparable<T> {
			if ( a1 is null ) {
				if ( a2 is null ) return 0;
				return -1;
			}
			if ( a2 is null ) {
				return 1;
			}
			if ( a1 == a2 ) {
				return 0;
			}
			var length = Math.Min( a1.Length, a2.Length );
			//
			for ( int i = 0; i < length; i++ ) {
				int d = a1[ i ].CompareTo( a2[ i ] );
				if ( d != 0 ) return d;
			}
			return a2.Length - a1.Length;
		}
		//
		//
		public static T Min<T>( this T[] a ) where T : IComparable<T> {
			if ( a is null ) return default( T );
			var l = a.Length;
			if ( 2 > l ) return default( T );
			T min = a[ 0 ];
			for ( int i = 1; i < l; i++ ) {
				if ( a[ i ].CompareTo( min ) < 0 ) min = a[ i ];
			}
			return min;
		}
		//
		//
		public static T Max<T>( this T[] a ) where T : IComparable<T> {
			if ( a is null ) return default( T );
			var l = a.Length;
			if ( 2 > l ) return default( T );
			T max = a[ 0 ];
			for ( int i = 1; i < l; i++ ) {
				if ( a[ i ].CompareTo( max ) > 0 ) max = a[ i ];
			}
			return max;
		}
	}
	//
	//
	//
	//
	//
	public static partial class CharArrayEx {
		///<summary>Returns the index of v in this array or -1, if not found.</summary>
		public static int IndexOf( this char[] a, char v ) {
			for ( int i = 0; i < a.Length; i++ ) {
				if ( v == a[ i ] ) return i;
			}
			return -1;
		}
	}
	//
	//
	//
	//
	//
	public static partial class StringArrayEx {
		//
		///<summary>Check if array length is greater than v and if string at position v is not empty.</summary>
		public static bool IsFilled( this string[] a, int v ) {
			return a is not null && a.Length > v && a[ v ].Length > 0;
		}
		//
		///<summary>Returns the index of v in this array or -1, if not found.</summary>
		public static int IndexOf( this string[] a, string v ) {
			if ( a is not null ) {
				for ( int i = 0; i < a.Length; i++ ) {
					if ( v == a[ i ] ) return i;
				}
			}
			return -1;
		}
		///<summary>Return true if v is in this array.</summary>
		public static bool Contains( this string[] a, string v ) {
			return -1 < IndexOf( a, v );
		}
		///<summary>Return true if any of v is in this array.</summary>
		public static bool Contains( this string[] a, params string[] v ) {
			if ( v.IsNullOrEmpty() ) return false;
			for ( int i = 0; i < v.Length; i++ ) {
				var p = IndexOf( a, v[ i ] );
				if ( -1 < p ) return true;
			}
			return false;
		}
		//
		///<summary>Returns the index of v in this array or -1, if not found.</summary>
		public static int IndexOf( this string[] a, string v, int len ) {
			if ( a is not null ) {
				v = v.Substring( 0, len );
				for ( int i = 0; i < a.Length; i++ ) {
					var b = a[ i ];
					if ( b is not null ) {
						b = b.Substring( 0, len );
						if ( v == b ) return i;
					}
				}
			}
			return -1;
		}
		//
		///<summary>Return true if v is in this array.</summary>
		public static bool Contains( this string[] a, string v, int len ) {
			return -1 < IndexOf( a, v, len );
		}
		//
		///<summary>Create an array and initialize all members.</summary>
		public static string[] Create( int n ) {
			var a = new string[ n ];
			for ( int i = 0; i < n; i++ ) {
				a[ i ] = CS.Empty;
			}
			return a;
		}
		//
		///<summary>Returns the index of v in this array or -1, if not found.</summary>
		public static int IndexOfIgnoreCase( this string[] a, string v ) {
			if ( a is not null ) {
				for ( int i = 0; i < a.Length; i++ ) {
					var c = v.CompareWithEmpty( a[ i ] );
					if ( 0 == c ) return i;
				}
			}
			return -1;
		}
		//
		///<summary>Return true if any of v is in a.</summary>
		public static bool ContainsIgnoreCase( this string[] a, params string[] v ) {
			if ( a is not null ) {
				if ( v.IsFilled() ) {
					for ( int i = 0; i < v.Length; i++ ) {
						var p = IndexOfIgnoreCase( a, v[ i ] );
						if ( -1 < p ) return true;
					}
				}
			}
			return false;
		}
	}
	//
	//
	//
	//
	//
	public static partial class DateTimeEx2 {
		//
		///<summary>Return a sortable date and time format with separators.</summary>
		public static string YMDHMS( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 1000 + '0' ),
				(char) ( dt.Year / 100 % 10 + '0' ),
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				CC.HY,
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				CC.HY,
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
				CC.SP,
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
				CC.CL,
				(char) ( dt.Second / 10 + '0' ),
				(char) ( dt.Second % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable date and time format with separators.</summary>
		public static string YMDHM( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 1000 + '0' ),
				(char) ( dt.Year / 100 % 10 + '0' ),
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				CC.HY,
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				CC.HY,
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
				CC.SP,
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable date format with separator.</summary>
		public static string YMD( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 1000 % 10 + '0' ),
				(char) ( dt.Year / 100 % 10 + '0' ),
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				CC.HY,
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				CC.HY,
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable date format without separator.</summary>
		public static string ymd( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 1000 % 10 + '0' ),
				(char) ( dt.Year / 100 % 10 + '0' ),
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format with separator.</summary>
		public static string HMS( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
				CC.CL,
				(char) ( dt.Second / 10 + '0' ),
				(char) ( dt.Second % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format without separator.</summary>
		public static string hms( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
				(char) ( dt.Second / 10 + '0' ),
				(char) ( dt.Second % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format with separators.</summary>
		public static string DHMS( this DateTime dt ) {
			var day = dt.Year < 2 ? dt.Day - 1 : dt.Day;
			var ca = new char[] {
				(char) ( day / 10 + '0' ),
				(char) ( day % 10 + '0' ),
				CC.HY,
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
				CC.CL,
				(char) ( dt.Second / 10 + '0' ),
				(char) ( dt.Second % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>DateTime format DD-HH-MM.</summary>
		public static string DHM( this DateTime dt ) {
			var day = dt.Year < 2 ? dt.Day - 1 : dt.Day;
			var ca = new char[] {
				(char) ( day / 10 + '0' ),
				(char) ( day % 10 + '0' ),
				CC.HY,
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format with separator from minutes.</summary>
		public static string HM( int minutes ) {
			var hours = minutes / 60;
			minutes = minutes % 60;
			var ca = new char[] {
				(char) ( hours / 10 + '0' ),
				(char) ( hours % 10 + '0' ),
				CC.CL,
				(char) ( minutes / 10 + '0' ),
				(char) ( minutes % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format with separator.</summary>
		public static string HM( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				CC.CL,
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable time format without separator.</summary>
		public static string hm( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a very compact, sortable date format without century and without separators.</summary>
		public static string ymd2( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a very compact, sortable date format without century and with only one separator.</summary>
		public static string ymd2hms( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
				CC.HY,
				(char) ( dt.Hour / 10 + '0' ),
				(char) ( dt.Hour % 10 + '0' ),
				(char) ( dt.Minute / 10 + '0' ),
				(char) ( dt.Minute % 10 + '0' ),
				(char) ( dt.Second / 10 + '0' ),
				(char) ( dt.Second % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a date format with separators.</summary>
		public static string DMY( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
				CC.DT,
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				CC.DT,
				(char) ( dt.Year / 1000 % 10 + '0' ),
				(char) ( dt.Year / 100 % 10 + '0' ),
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a date format without century, with separators.</summary>
		public static string DMY2( this DateTime dt ) {
			var ca = new char[] {
				(char) ( dt.Day / 10 + '0' ),
				(char) ( dt.Day % 10 + '0' ),
				CC.DT,
				(char) ( dt.Month / 10 + '0' ),
				(char) ( dt.Month % 10 + '0' ),
				CC.DT,
				(char) ( dt.Year / 10 % 10 + '0' ),
				(char) ( dt.Year % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a date format without century, with separators.</summary>
		public static string DMY2HM( this DateTime dt ) {
			var d = dt.Day;
			var m = dt.Month;
			var y = dt.Year;
			var h = dt.Hour;
			var n = dt.Minute;
			var ca = new char[] {
				(char) ( d / 10 + '0' ),
				(char) ( d % 10 + '0' ),
				CC.DT,
				(char) ( m / 10 + '0' ),
				(char) ( m % 10 + '0' ),
				CC.DT,
				(char) ( y / 10 % 10 + '0' ),
				(char) ( y % 10 + '0' ),
				CC.SP,
				(char) ( h / 10 + '0' ),
				(char) ( h % 10 + '0' ),
				CC.CL,
				(char) ( n / 10 + '0' ),
				(char) ( n % 10 + '0' ),
			};
			return new string( ca );
		}
		public static string DateShortAndTime( this DateTime dt ) {
			if ( dt.IsEmpty() ) return CS.Empty;
			return dt.DMY() + CS.SP + dt.HMS();
		}
		public static string DateAndTimeCompact( this DateTime dt ) {
			if ( dt.IsEmpty() ) return CS.Empty;
			return dt.DMY2() + CS.SP + dt.HM();
		}
		//
		//
		public static DateTime Max( params DateTime[] values ) {
			if ( Log.Deny( values.IsNullOrEmpty() ) ) return DateTime.MinValue;
			DateTime result = DateTime.MinValue;
			for ( int i = 0; i < values.Length; i++ ) {
				if ( result < values[ i ] ) result = values[ i ];
			}
			return result;
		}
		public static DateTime Min( params DateTime[] values ) {
			if ( Log.Deny( values.IsNullOrEmpty() ) ) return DateTime.MinValue;
			DateTime result = DateTime.MaxValue;
			for ( int i = 0; i < values.Length; i++ ) {
				if ( result > values[ i ] ) result = values[ i ];
			}
			return result;
		}
		//
		// Parsing
		//
		public static readonly char[] DateSeparators = new char[] { CC.HY, CC.MN, CC.DT, CC.CL, CC.SP, CC.FS, CC.T, CC.Z };
		//
		///<summary>Split a date or time string. Returns array of found integers or null.</summary>
		public static int[] SplitDate( this string value ) {
			if ( value.IsFilled() ) {
				var parts = value.Split( DateSeparators, StringSplitOptions.RemoveEmptyEntries );
				if ( !parts.IsNullOrEmpty() ) {
					// Convert all parts into integer
					var l = parts.Length;
					var values = new int[ l ];
					for ( int i = 0; i < l; i++ ) {
						values[ i ] = parts[ i ].ToInt();
					}
					return values;
				}
			}
			return null;
		}
		//
		///<summary>Quickly parse the standard format: 2018-11-27 09:24:11.</summary>
		public static DateTime ToDateTime( this string value ) {
			var values = SplitDate( value );
			if ( values is not null ) {
				Array.Resize( ref values, 6 );
				var result = new DateTime( values[ 0 ], values[ 1 ], values[ 2 ]
					, values[ 3 ], values[ 4 ], values[ 5 ]
					, DateTimeKind.Utc );
				return result;
			}
			return DateTime.MinValue;
		}
		//
		///<summary>Quickly parse the german format: 27.11.2018 09:24:11.</summary>
		public static DateTime GermanDateTime( this string value ) {
			var values = SplitDate( value );
			if ( values.IsFilled() ) {
				Array.Resize( ref values, 6 );
				var year = values[ 2 ];
				if ( year < 1600 ) year += 2000;
				var result = new DateTime( year, values[ 1 ], values[ 0 ]
					, values[ 3 ], values[ 4 ], values[ 5 ]
					, DateTimeKind.Utc );
				return result;
			}
			return DateTime.MinValue;
		}
		//
		//
		///<summary>Convert DateTime to UNIX timestamp. For JavaScript Date(), msec must be true.</summary>
		// TASK: This did not produce the right results when working with ClickHouse. Check if these methods are still being used.
		[Obsolete]
		public static long ToUnixTimestamp( this DateTime value, bool msec ) {
			// SqlDataReader.GetDateTime() creates DateTimeKind.Unspecified
			Log.Assert( DateTimeKind.Utc == value.Kind || DateTimeKind.Unspecified == value.Kind );
			var date = new DateTime( 1970, 1, 1, 0, 0, 0, value.Kind );
			var seconds = (long) ( ( value - date ).TotalSeconds );
			if ( msec ) seconds *= 1000;
			return seconds;
		}
		//
		//
		///<summary>Convert UNIX timestamp to DateTime, only UTC.  For JavaScript Date(), msec must be true.</summary>
		///<remarks>UNIX timestamp is only defined for UTC!</remarks>
		// TASK: ToUnixTimestamp did not produce the right results when working with ClickHouse. Check if these methods are still being used.
		[Obsolete]
		public static DateTime ToDateTime( long delta, bool msec ) {
			var dateTime = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
			if ( msec ) delta /= 1000;
			dateTime = dateTime.AddSeconds( delta );
			return dateTime;
		}
		//
		//
		///<summary>Convert UTC DateTime to UNIX timestamp.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static long ToUnixTS( this DateTime dateTime ) {
			Log.Assert( DateTimeKind.Utc == dateTime.Kind );
			return ( (DateTimeOffset) dateTime ).ToUnixTimeSeconds();
		}
		//
		//
		private static void _Compare( ref int result, int flags, TimeUnit tu, int a, int b ) {
			var which = (int) tu;
			if ( 0 != ( flags & which ) ) {
				if ( a != b ) result |= which;
			}
		}
		//
		///<summary>The returned TimeUnit will have one flag set for every different field.
		/// It will NOT indicate which date is bigger or smaller.</summary>
		public static TimeUnit Compare( this DateTime a, DateTime b, TimeUnit unit ) {
			int result = 0;
			var flags = (int) unit;
			//ompare( ref result, flags, TimeUnit.Nanosecond, a.Nano, b.Nanosecond );
			_Compare( ref result, flags, TimeUnit.Millisecond, a.Millisecond, b.Millisecond );
			_Compare( ref result, flags, TimeUnit.Second, a.Second, b.Second );
			_Compare( ref result, flags, TimeUnit.Minute, a.Minute, b.Minute );
			_Compare( ref result, flags, TimeUnit.Hour, a.Hour, b.Hour );
			_Compare( ref result, flags, TimeUnit.Day, a.Day, b.Day );
			_Compare( ref result, flags, TimeUnit.Month, a.Month, b.Month );
			_Compare( ref result, flags, TimeUnit.Year, a.Year, b.Year );
			return (TimeUnit) result;
		}
		//
		///<summary>Compare only the date.</summary>
		public static int CompareDate( this DateTime a, DateTime b ) {
			// Compare year
			int result = b.Year - a.Year;
			// If identical, compare month
			if ( 0 == result ) {
				result = b.Month - a.Month;
			}
			// If identical, compare day
			if ( 0 == result ) {
				result = b.Day - a.Day;
			}
			return result;
		}
		//
	}
	//
	//
	[Serializable()]
	public enum TimeUnit : byte {
		Unknown = 0x00, // 
		Nanosecond = 0x01, // ns, not available in DateTime
		Millisecond = 0x02, // ms
		Second = 0x04, // sec
		Minute = 0x08, // min
		Hour = 0x10, // h
		Day = 0x20, // d
		Month = 0x40, // M
		Year = 0x80, // Y
		AllDate = TimeUnit.Year | TimeUnit.Month | TimeUnit.Day,
		AllTime = TimeUnit.Hour | TimeUnit.Minute | TimeUnit.Second,
		UpToNanosecond = TimeUnit.Nanosecond | TimeUnit.Millisecond | TimeUnit.Second | TimeUnit.Minute | TimeUnit.Hour | TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToMillisecond = TimeUnit.Millisecond | TimeUnit.Second | TimeUnit.Minute | TimeUnit.Hour | TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToSecond = TimeUnit.Second | TimeUnit.Minute | TimeUnit.Hour | TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToMinute = TimeUnit.Minute | TimeUnit.Hour | TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToHour = TimeUnit.Hour | TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToDay = TimeUnit.Day | TimeUnit.Month | TimeUnit.Year,
		UpToMonth = TimeUnit.Month | TimeUnit.Year,
	};
	//
	//
	//
	//
	//
	public static partial class StringEx {
		///<summary>Check if the string is null or empty.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsEmpty( this string v ) {
			return String.IsNullOrEmpty( v );
		}
		///<summary>Check if the string is null or empty.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNullOrEmpty( this string v ) {
			return String.IsNullOrEmpty( v );
		}
		///<summary>Get string length, even if it is null.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static int GetLength( this string v ) {
			return v is null ? 0 : v.Length;
		}
		///<summary>Check if the string is null or empty, or contains only whitespace.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNullOrWhiteSpace( this string v ) {
			return String.IsNullOrWhiteSpace( v );
		}
		///<summary>Check if the string is NOT ( null or empty or only whitespace ).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNotBlank( this string v ) {
			return !String.IsNullOrWhiteSpace( v );
		}
		///<summary>Check if the string is NOT null or empty.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsFilled( this string v ) {
			return !String.IsNullOrEmpty( v );
		}
		///<summary>Check if the string is NOT null or empty after trimming the given character.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsFilled( this string v, char c ) {
			if ( String.IsNullOrEmpty( v ) ) return false;
			return v.Trim( c ).Length > 0;
		}
		///<summary>Check if the string is NOT null or empty after trimming the given characters.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsFilled( this string v, char[] ac ) {
			if ( String.IsNullOrEmpty( v ) ) return false;
			return v.Trim( ac ).Length > 0;
		}
		///<summary>If the trimmed string is null or empty, return null. Otherwise return the trimmed string.</summary>
		public static string Trimmed( this string v ) {
			if ( v.IsNullOrEmpty() ) return null;
			v = v.Trim();
			if ( v.IsNullOrEmpty() ) return null;
			return v;
		}
		///<summary>Test if a string is null or empty. Allow testing null strings.</summary>
		public static bool IsNullOrTrimmedEmpty( this string v ) {
			if ( v.IsNullOrEmpty() ) return true;
			v = v.Trim();
			if ( v.IsNullOrEmpty() ) return true;
			return false;
		}
		///<summary>Join all filled strings with a separator.</summary>
		public static string Concat( string separator, params string[] values ) {
			if ( values.IsEmpty() ) return null;
			var sb = new StringBuilder();
			var I = values.Length;
			for ( int i = 0; i < I; i++ ) {
				sb.AppendIf( values[ i ], separator );
			}
			return sb.ToString();
		}
		//
		//
		public static bool RxIsMatch( this string v, Regex rx ) {
			return rx.IsMatch( v );
		}
		public static bool RxIsMatch( this string v, string pattern ) {
			return v.RxIsMatch( new Regex( pattern ) );
		}
		//
		//
		public static string RxReplace( this string v, Regex pattern, string replaceStr, int count, int startPos ) {
			if ( v.IsNullOrEmpty() ) return v;
			return pattern.Replace( v, replaceStr, count, startPos );
		}
		public static string RxReplace( this string v, Regex pattern, string replaceStr ) {
			return RxReplace( v, pattern, replaceStr, -1, 0 );
		}
		public static string RxReplace( this string v, Regex pattern, string replaceStr, int count ) {
			return RxReplace( v, pattern, replaceStr, count, 0 );
		}
		public static string RxReplace( this string v, string pattern, string replaceStr, int count, int startPos ) {
			if ( v.IsNullOrEmpty() ) return v;
			Regex rx = new Regex( pattern );
			if ( replaceStr is null ) { replaceStr = CS.Empty; }
			string newString = rx.Replace( v, replaceStr, count, startPos );
			return newString;
		}
		public static string RxReplace( this string v, string pattern, string replaceStr ) {
			return RxReplace( v, pattern, replaceStr, -1, 0 );
		}
		public static string RxReplace( this string v, string pattern, string replaceStr, int count ) {
			return RxReplace( v, pattern, replaceStr, count, 0 );
		}
		// return true, if the character should be replaced.
		public delegate bool ReplaceCheckChar( Char c );
		//
		///<summary>Replace all matching characters. If replaceWith is 0, remove them.</summary>
		public static string Replace( this string v, char replaceWith, ReplaceCheckChar cc ) {
			if ( v.IsNullOrEmpty() ) return v;
			var len = v.Length;
			StringBuilder result = new StringBuilder( len );
			for ( int i = 0; i < len; i++ ) {
				char c = v[ i ];
				if ( cc( c ) ) {
					if ( replaceWith > 0 ) {
						result.Append( replaceWith );
					}
				} else {
					result.Append( c );
				}
			}
			return result.ToString();
		}
		//
		public static string ReplaceNonPrintable( this string v, char replaceWith ) {
			if ( v.IsNullOrEmpty() ) return v;
			var len = v.Length;
			StringBuilder result = new StringBuilder( len );
			for ( int i = 0; i < len; i++ ) {
				char c = v[ i ];
				byte b = (byte) c;
				if ( b < 32 )
					result.Append( replaceWith );
				else
					result.Append( c );
			}
			return result.ToString();
		}
		//
		///<summary>Replace all 'from' matches, with 'into' values, using default comparison.</summary>
		public static string Replace( this string v, string[] from, string[] into ) {
			if ( v.IsNullOrEmpty() ) return v;
			if ( Log.Assert( from is not null && into is not null && from.Length == into.Length ) ) {
				for ( int i = 0; i < from.Length; i++ ) {
					v = v.Replace( from[ i ], into[ i ] );
				}
			}
			return v;
		}
		//
		//
#if CS_NET_CORE
		///<summary>Replace all 'from' matches, with 'into' values, using StringComparison.OrdinalIgnoreCase.</summary>
		public static string ReplaceOIC( this string v, string[] from, string[] into ) {
			if ( v.IsNullOrEmpty() ) return v;
			if ( Log.Assert( from is not null && into is not null && from.Length == into.Length ) ) {
				for ( int i = 0; i < from.Length; i++ ) {
					v = v.Replace( from[ i ], into[ i ], StringComparison.OrdinalIgnoreCase );
				}
			}
			return v;
		}
		///<summary>Replace all keys by their values, using StringComparison.OrdinalIgnoreCase.</summary>
		public static string ReplaceOIC( this string v, Dictionary<string, string> sd ) {
			if ( v.IsNullOrEmpty() || sd is null || 0 == sd.Count ) return v;
			foreach ( var kv in sd ) {
				v = v.Replace( kv.Key, kv.Value, StringComparison.OrdinalIgnoreCase );
			}
			return v;
		}
#endif
		//
		public static string Replace( this string v, string[,] table ) {
			if ( v.IsNullOrEmpty() ) return v;
			if ( Log.Assert( table is not null && table.Length > 1 ) ) {
				for ( int i = 0, n = table.Length / 2; i < n; i++ ) {
					v = v.Replace( table[ i, 0 ], table[ i, 1 ] );
				}
			}
			return v;
		}
		///<summary>Remove characters from string.</summary>
		public static string RemoveCharacters( this string v, params char[] unwanted ) {
			if ( v.IsNullOrEmpty() ) return v;
			if ( unwanted is null ) return v;
			var sa = v.Split( unwanted );
			return string.Join( string.Empty, sa );
		}
		//
		///<summary>Replace some inner part of a string.</summary>
		public static string Splice( this string v, int p1, string insert, int p2 ) {
			return v.Substring( 0, p1 ) + insert + v.Substring( p2 );
		}
		///<summary>Check if the string contains any of the strings in the array. Ignore Case. Every kind of empty string returns false.</summary>
		public static bool ContainsIgnoreCase( this string v, string check ) {
			if ( v is null || check is null || 0 == v.Length || 0 == check.Length ) return false;
			string t = check.ToLowerInvariant();
			v = v.ToLowerInvariant();
			return v.Contains( t );
		}
		///<summary>Check if the string contains any of the strings in the array. Ignore Case.</summary>
		public static bool ContainsIgnoreCase( this string v, params string[] check ) {
			if ( v is null || check is null || 0 == v.Length || 0 == check.Length ) return false;
			v = v.ToLowerInvariant(); string t;
			for ( int i = 0; i < check.Length; i++ ) {
				t = check[ i ].ToLowerInvariant();
				if ( v.Contains( t ) ) return true;
			}
			return false;
		}
		///<summary>Check if the string starts with any of the strings in the array.</summary>
		public static bool StartsWith( this string v, params string[] check ) {
			if ( v is null || check is null || 0 == v.Length || 0 == check.Length ) return false;
			for ( int i = 0; i < check.Length; i++ ) {
				if ( v.StartsWith( check[ i ] ) ) return true;
			}
			return false;
		}
		///<summary>Convert string to int, return 0 on error.</summary>
		public static bool ToBool( this string v ) {
			return BoolEx.GetCharBool( v );
		}
		///<summary>Convert string to int, return 0 on error.</summary>
		public static int ToInt( this string v ) {
			if ( v is null || 0 == v.Length ) return 0;
			if ( int.TryParse( v, out var result ) ) return result;
			return 0;
		}
		///<summary>Convert string to long, return 0 on error.</summary>
		public static long ToLong( this string v ) {
			if ( v is null || 0 == v.Length ) return 0;
			if ( long.TryParse( v, out var result ) ) return result;
			return 0;
		}
		///<summary>Convert string to double, return NaN on error.</summary>
		public static double ToDouble( this string v ) {
			if ( v is null || 0 == v.Length ) return Double.NaN;
			if ( double.TryParse( v, out var result ) ) return result;
			return 0.0;
		}
		///<summary>Convert string to decimal, return NaN on error.</summary>
		public static decimal ToDecimal( this string v ) {
			if ( v is null || 0 == v.Length ) return 0m;
			if ( decimal.TryParse( v, out var result ) ) return result;
			return 0m;
		}
		//
		///<summary>Formatting for JSON.</summary>
		public static string ToJson( this string v ) {
			if ( v is null ) return CS.null_;
			if ( 0 == v.Length ) return CS.EmptyQuotes2;
			return CS.Q2 + JSON.Escape( v ) + CS.Q2;
		}
		//
		///<summary>Enclose a string in single quotes or null, if it is not null.</summary>
		public static string Quote1( this string v ) {
			if ( v is null ) return null;
			return CS.Q1 + v + CS.Q1;
		}
		///<summary>Enclose a string in double quotes, if it is not null.</summary>
		public static string Quote2( this string v ) {
			if ( v is null ) return null;
			return CS.Q2 + v + CS.Q2;
		}
		///<summary>Enclose a string in single quotes or empty single quotes, if it is not null.</summary>
		public static string Quotes1( this string v ) {
			if ( v is null ) return CS.EmptyQuotes1;
			return CS.Q1 + v + CS.Q1;
		}
		///<summary>Enclose a string in double quotes or empty double quotes, if it is not null.</summary>
		public static string Quotes2( this string v ) {
			if ( v is null ) return CS.EmptyQuotes2;
			return CS.Q2 + v + CS.Q2;
		}
		///<summary>Enclose a string in brackets.</summary>
		public static string Bracket( this string v ) {
			if ( v is null ) return null;
			return CS.B1 + v + CS.B2;
		}
		///<summary>Enclose a string in braces.</summary>
		public static string Braced( this string v ) {
			if ( v is null ) return null;
			return CS.C1 + v + CS.C2;
		}
		///<summary>Enclose a string in variables-braces, if it is not null or empty.</summary>
		public static string BracedVar( this string v ) {
			if ( v is null ) return null;
			if ( 0 == v.Length ) return v;
			return CS.C1 + CS.DL + v + CS.C2;
		}
		///<summary>Enclose in-place all strings in array in variables-braces.</summary>
		public static string[] BracedVar( this string[] v ) {
			if ( v is null ) return null;
			var I = v.Length;
			for ( int i = 0; i < I; i++ ) {
				v[ i ] = v[ i ].BracedVar();
			}
			return v;
		}
		//
		public static readonly Regex rxIntegralNumber = new Regex( "^[-+]?[0-9]+$" );
		public static readonly Regex rxDecimalNumber = new Regex( "^[-+]?[0-9]+\\.?[0-9]*([eE][-+]?[0-9]+)?$" );
		//
		///<summary>Check if the string is a plausible integer number.</summary>
		public static bool IsIntegral( this string v ) {
			return v.IsFilled() && rxIntegralNumber.IsMatch( v );
		}
		//
		///<summary>Check if the string is a plausible decimal number.</summary>
		public static bool IsDecimal( this string v ) {
			return v.IsFilled() && rxDecimalNumber.IsMatch( v );
		}
		///<summary>Get a substring. If length is < 0, the remainder of the string is returned.</summary>
		public static string Mid( this string value, int offset, int length = -1 ) {
			if ( value is null ) return null;
			if ( 0 == length ) return CS.Empty;
			if ( offset < 0 ) offset = 0;
			var l = value.Length;
			if ( 0 == l ) return value;
			if ( offset > l ) return null;
			if ( length < 0 || offset + length > l ) length = l - offset;
			if ( length < 0 ) return null;
			return value.Substring( offset, length );
		}
		///<summary>Get a save substring. If length is < 0, the remainder of the string is returned. Never returns NULL.</summary>
		public static string SaveSubStr( this string value, int offset, int length = -1 ) {
			if ( value is null || 0 == length ) return CS.Empty;
			if ( offset < 0 ) offset = 0;
			var l = value.Length;
			if ( 0 == l ) return value;
			if ( offset > l ) return CS.Empty;
			if ( length < 0 || offset + length > l ) length = l - offset;
			if ( length < 0 ) return CS.Empty;
			return value.Substring( offset, length );
		}
		///<summary>Get substring of specified number of characters on the right.</summary>
		public static string Right( this string value, int length ) {
			if ( value is null ) return null;
			var l = value.Length;
			if ( 0 == l || length > l ) return value;
			return value.Substring( l - length );
		}
		///<summary>Get substring of specified number of characters on the left.</summary>
		public static string Left( this string value, int length ) {
			if ( value is null ) return null;
			var l = value.Length;
			if ( 0 == l || length > l ) return value;
			return value.Substring( 0, length );
		}
		///<summary>Like Left, but append 3 dots if the string was truncated.</summary>
		public static string Ellipsis( this string value, int length ) {
			if ( value is null ) return null;
			var l = value.Length;
			if ( 0 == l || length > l ) return value;
			value = value.Substring( 0, length ) + CS.Dt3;
			return value;
		}
		///<summary>Replace Caret into Quote1. Used often to prevent quoted quotes.</summary>
		public static string CaretToQuote1( this string v ) {
			if ( v is null ) return null;
			return v.Replace( '^', CC.Q1 );
		}
		///<summary>Replace Caret into Quote2. Used often to prevent quoted quotes.</summary>
		public static string CaretToQuote2( this string v ) {
			if ( v is null ) return null;
			return v.Replace( '^', CC.Q2 );
		}
		///<summary>Make 1st character lowercase.</summary>
		public static string DromedaryCase( this string v ) {
			if ( v is null ) return null;
			if ( 0 == v.Length ) return v;
			if ( 1 == v.Length ) return v.ToLower();
			var s1 = v.Substring( 0, 1 ).ToLower();
			var s2 = v.Substring( 1 );
			return s1 + s2;
		}
		///<summary>Make 1st character uppercase.</summary>
		public static string CamelCase( this string v ) {
			if ( v is null ) return null;
			if ( 0 == v.Length ) return v;
			if ( 1 == v.Length ) return v.ToUpper();
			var s1 = v.Substring( 0, 1 ).ToUpper();
			var s2 = v.Substring( 1 );
			return s1 + s2;
		}
		///<summary>JSON escape the string</summary>
		public static string Json( this string v ) {
			if ( v is null ) return null;
			v = JSON.Escape( v );
			return v;
		}
		//
		///<summary>URI escape the string</summary>
		public static string URI( this string v ) {
			if ( v is null ) return null;
			return Uri.EscapeDataString( v );
		}
		//
		///<summary>Test if the string contains the character.</summary>
		public static bool Contains( this string v, char c ) {
			if ( v is null ) return false;
			return -1 < v.IndexOf( c );
		}
		//
		///<summary>Test if the string contains any of the strings in the array.</summary>
		public static bool Contains( this string v, params string[] sa ) {
			if ( v is null || sa is null || 0 == sa.Length ) return false;
			foreach ( var s in sa ) {
				if ( v.Contains( s ) ) return true;
			}
			return false;
		}
		//
		///<summary>Test if the string contains all of the strings in the array.</summary>
		public static bool ContainsAll( this string v, params string[] sa ) {
			if ( v is null || sa is null || 0 == sa.Length ) return false;
			int found = 0;
			foreach ( var s in sa ) {
				if ( v.Contains( s ) ) found++;
			}
			return sa.Length == found;
		}
		//
		///<summary>Test if the string contains characters of another string.</summary>
		public static bool ContainsChar( this string v, string chars ) {
			for ( int i = 0; i < v.Length; i++ ) {
				if ( chars.Contains( v[ i ] ) ) return true;
			}
			return false;
		}
		//
		///<summary>Count number of specific character within string.</summary>
		public static int Count( this string v, char c ) {
			if ( v is null ) return 0;
			int count = 0;
			var I = v.Length;
			for ( int i = 0; i < I; i++ ) {
				if ( v[ i ] == c ) count++;
			}
			return count;
		}
		//
		///<summary>Compare strings, any may be null or empty. Empty is considered equal to null.</summary>
		public static int CompareWithEmpty( this string x, string y, StringComparison comparison = StringComparison.OrdinalIgnoreCase ) {
			if ( x is null || 0 == x.Length ) {
				if ( y is null || 0 == y.Length ) {
					return 0;
				} else {
					return -1;
				}
			}
			if ( y is null || 0 == y.Length ) {
				return 1;
			}
			return String.Compare( x, y, comparison );
		}
		//
		///<summary>Compare strings, any may be null.</summary>
		public static int CompareWith( this string x, string y, StringComparison comparison = StringComparison.OrdinalIgnoreCase ) {
			if ( x is null )
				if ( y is null )
					return 0;
				else
					return -1;
			if ( y is null )
				return 1;
			return String.Compare( x, y, comparison );
		}
		//
		///<summary>Check if strings are equal, any may be null.</summary>
		public static bool EqualTo( this string x, string y, StringComparison comparison = StringComparison.OrdinalIgnoreCase ) {
			return 0 == CompareWith( x, y, comparison );
		}
		//
		///<summary>Append one to another. Any of the strings may be empty or null.</summary>
		public static string Append( this string first, string second ) {
			if ( first is null ) return second;
			if ( second is null ) return first;
			return first + second;
		}
		//
		///<summary>Append one to another. Any of the strings may be empty or null, in this case the separator is not used.</summary>
		public static string Append( this string first, string second, string separator ) {
			if ( first.IsNullOrEmpty() ) return second;
			if ( second.IsNullOrEmpty() ) return first;
			return first + separator + second;
		}
		//
		///<summary>Append parameters to an URL. If the URL already contains a question mark, use an ampersand, otherwise question mark, to append the string.</summary>
		public static string AppendUrlParameters( this string url, string args ) {
			if ( url is null ) return null;
			if ( args is null || 0 == args.Length ) return url;
			bool q = url.Contains( CS.QM );
			url = url + ( q ? CS.AM : CS.QM ) + args;
			return url;
		}
		//
		///<summary>Build a new string from N replicas of the original string.</summary>
		public static string Replicate( this string x, int n ) {
			if ( x is null ) return null;
			if ( 0 == x.Length ) return x;
			if ( 0 == n ) return null;
			if ( 1 == n ) return x;
			int len = x.Length;
			var sb = new StringBuilder( len * n );
			for ( int i = 0; i < n; i++ ) {
				sb.Append( x );
			}
			return sb.ToString();
		}
		//
		///<summary>Ensure trailing forward slash</summary>
		public static string TrailingSlash( this string value ) {
			if ( value.EndsWith( CS.FS ) ) return value;
			return value + CS.FS;
		}
		//
		///<summary>Ensure trailing backslash</summary>
		public static string TrailingBackslash( this string value ) {
			if ( value.EndsWith( CS.BS ) ) return value;
			return value + CS.BS;
		}
		//
		///<summary>Remove all invalid XML characters.</summary>
		public static string FixXmlChars( this string value ) {
			if ( value is null ) return null;
			if ( CS.Empty == value ) return value;
			var sb = new StringBuilder( value.Length );
			for ( int i = 0; i < value.Length; i++ ) {
				var c = value[ i ];
				if ( c.IsXml() ) sb.Append( c ); // otherwise drop it
			}
			return sb.ToString();
		}
		//
		///<summary>Trim the string. If it is empty or NULL return null.</summary>
		public static string GetNULLTrimmed( this string value ) {
			if ( value.IsNullOrEmpty() ) return null;
			value = value.Trim();
			if ( 0 == value.Length ) return null;
			if ( 0 == String.Compare( value, CS.NULL, true ) ) return null;
			return value;
		}
		//
		//
		public static char[] PunctuationTrimChars = new char[] { CC.CM, CC.SP, CC.SC, CC.HY, CC.FS, CC.DT };
		//
		///<summary>Trim all general punctuation from a string. If it is empty, NULL or "0" return null.</summary>
		public static string GetPunctuationTrimmed( this string value ) {
			if ( value.IsNullOrEmpty() ) return null;
			value = value.Trim( PunctuationTrimChars );
			if ( 0 == value.Length ) return null;
			if ( value == CS.NULL || CS.N0 == value ) return null;
			return value;
		}
		//
		//
		// See https://stackoverflow.com/questions/201323/how-can-i-validate-an-email-address-using-a-regular-expression
		// public static Regex EmailCheckRx = new Regex( "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])" );
		//
		///<summary>Check email address for validity.</summary>
		public static bool CheckEmail( this string value ) {
			if ( !String.IsNullOrEmpty( value ) ) {
				try {
					var _ = new System.Net.Mail.MailAddress( value );
					return true;
				} catch ( Exception ex ) {
					Log.SmallException( ex );
				}
			}
			return false;
		}
		//
		//
		public static char[] EmailTrimChars = new char[] { CC.SP, CC.CR, CC.LF, CC.DT, CC.CM, CC.SC, CC.AT, CC.LT, CC.GT };
		//
		///<summary>Trim invalid characters from email address and check the result.</summary>
		public static string TrimEmail( string value ) {
			// Prevent empty
			if ( value.IsFilled() ) {
				value = value.Trim( EmailTrimChars );
				// Prevent empty
				if ( value.IsFilled() ) {
					try {
						// Check the address
						var address = new System.Net.Mail.MailAddress( value );
						if ( address is not null ) {
							// Get result
							value = address.Address;
							// Prevent empty
							if ( value.IsFilled() ) {
								// OK
								return value;
							}
						}
					} catch ( Exception ex ) {
						Log.ExceptionV( value, ex );
					}
				}
			}
			return null;
		}
		//
		//
		//
		//
		///<summary>Converted to lowercase.  Never return null.</summary>
		public static string Lower( this string v ) {
			if ( v.IsNullOrEmpty() ) return CS.Empty;
			return v.ToLowerInvariant();
		}
		//
		//
		private static readonly string[] _XmlFrom = new string[] { "<", ">", "\"", "'", "&" };
		private static readonly string[] _XmlInto = new string[] { "&lt;", "&gt;", "&quot;", "&apos;", "&amp;" };
		//
		///<summary>XML-escape the string.</summary>
		public static string ToXML( this string str ) {
			if ( str.IsFilled() ) {
				str = str.Replace( _XmlFrom, _XmlInto );
			}
			return str;
		}
		//
		// https://www.w3.org/TR/xml/#charsets
		static readonly Regex _RxXmlCleanSpecial = new Regex( "[\x0000-\x0008\x000B\x000C\x000E-\x001F\xFFFE\xFFFF]", RegexOptions.Compiled | RegexOptions.CultureInvariant );
		//
		///<summary>Replace forbidden XML characters by spaces. See: https://www.w3.org/TR/xml/#charsets</summary>
		public static string XmlCleanSpecial( this string value ) {
			if ( value is null || 0 == value.Length ) return value;
			return _RxXmlCleanSpecial.Replace( value, " " );
		}
		//
		///<summary>Replace all CR/LF by Space.</summary>
		public static string SingleLine( this string value ) {
			if ( value is null ) return null;
			if ( 0 == value.Length ) return value;
			value = value.Replace( CS.CR, CS.SP );
			value = value.Replace( CS.LF, CS.SP );
			return value;
		}
		//
		/////<summary>Convert string to byte array.</summary>
		/////<remarks>heap allocation, use only when you cannot operate on spans</remarks>
		//public static byte[] ToByteArray( this string s ) => s.ToByteSpan().ToArray();
		////
		/////<summary>Convert string to byte span.</summary>
		//public static ReadOnlySpan<byte> ToByteSpan( this string s ) => MemoryMarshal.Cast<char, byte>( s );
	}
	//
	//
	//
	//
	//
	public static partial class StringBuilderEx {
		//
		///<summary>Check if the string builder is NOT null or empty.</summary>
		public static bool IsFilled( this StringBuilder v ) {
			return v is not null && v.Length > 0;
		}
		//
		///<summary>Write text into a fixed-width column.</summary>
		public static StringBuilder PutFixed( this StringBuilder sb, object o, int width ) {
			var text = o is String ? (string) o : o.ToString();
			if ( text is null ) text = CS.Empty;
			var w = Math.Abs( width );
			text = text.Left( w - 1 );
			text = width < 0 ? text.PadLeft( w ) : text.PadRight( w );
			if ( text.Length > w ) text = text.Left( w );
			sb.Append( text );
			return sb;
		}
		//
		///<summary>Append text, if not null or empty. If separator is not null and string builder contains content, then the separator is inserted before the text.</summary>
		public static StringBuilder AppendIf( this StringBuilder sb, string text, string separator = null ) {
			if ( text.IsFilled() ) {
				if ( separator is not null ) {
					if ( sb.Length > 0 ) {
						sb.Append( separator );
					}
				}
				sb.Append( text );
			}
			return sb;
		}
		//
		///<summary>Append text, if expression is true and text is not null or empty. If separator is not null and string builder contains content, then the separator is inserted before the text.</summary>
		public static StringBuilder AppendIf( this StringBuilder sb, bool expression, string text, string separator = null ) {
			if ( expression ) {
				AppendIf( sb, text, separator );
			}
			return sb;
		}
		///<summary>Like AppendIf, but remove all punctuation from the value before appending it.</summary>
		public static StringBuilder AppendSeparator( this StringBuilder sb, string value, string separator = CS.CmSp ) {
			if ( value is null ) return sb;
			value = value.GetPunctuationTrimmed();
			return sb.AppendIf( value, separator );
		}
		///<summary>Append name and value.</summary>
		public static StringBuilder AppendNameValue( this StringBuilder sb, string name, object value ) {
			if ( Log.Deny( name.IsNullOrEmpty() ) ) return sb;
			sb.Append( ' ' ).Append( name ).Append( ':' ).Append( value is null ? CS.Empty : value.ToString() );
			return sb;
		}
		//
		// Adapted from StackOverflow question 24769701:
		///<summary>Trim any of the given characters from the end of the string.</summary>
		public static StringBuilder TrimEnd( this StringBuilder sb, params char[] chars ) {
			// Check incoming arguments
			if ( sb is null || sb.Length == 0 ) return sb;
			// If characters are missing, use the default.
			if ( chars is null || chars.Length == 0 ) chars = CA.SpTbCrLf;
			// Start from the end of a string builder
			int i = sb.Length - 1;
			for ( ; i >= 0; i-- ) {
				// Is the current character allowed?
				if ( -1 == chars.IndexOf( sb[ i ] ) ) {
					break;
				}
			}
			// Fix the length
			if ( i < sb.Length - 1 ) {
				sb.Length = i + 1;
			}
			return sb;
		}
	}
	//
	//
	//
	//
	//
	public static partial class RandomEx {
		public static bool Bool( this Random rnd ) {
			var v = rnd.Next( 0, 2 );
			return 1 == v;
		}
	}
	//
	//
	//
	//
	//
	public static partial class ObjectEx {
		//
		//
		public static bool IsEqual( this object o1, object o2 ) {
			if ( o1 is null ) {
				if ( o2 is null ) {
					return true;
				}
				return false;
			}
			return o1.Equals( o2 );
		}
		//
		//
		///<summary>Format an object</summary>
		public static string DisplayFormat( this object o, string format ) {
			if ( o is null ) return CS.Empty;
			// Check against MinValue
			var min = false;
			var t = o.GetType();
			var tc = Type.GetTypeCode( t );
			switch ( tc ) {
				case TypeCode.Boolean: break;
				case TypeCode.Byte: break;
				case TypeCode.Char: break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: min = ( (System.DateTime) o ) == System.DateTime.MinValue; break;
				case TypeCode.Decimal: break;
				case TypeCode.Double: break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: break;
				case TypeCode.Int32: break;
				case TypeCode.Int64: break;
				case TypeCode.Object: break;
				case TypeCode.SByte: break;
				case TypeCode.Single: break;
				case TypeCode.String: break;
				case TypeCode.UInt16: break;
				case TypeCode.UInt32: break;
				case TypeCode.UInt64: break;
			}
			// Special formatting rules
			switch ( tc ) {
				case TypeCode.DateTime: if ( format.IsNullOrEmpty() ) format = "yyyy-MM-dd HH:mm:ss"; break;
			}
			if ( min ) return CS.Empty;
			// Finally try to format
			try {
				if ( format.IsFilled() ) {
					// Handle special format (=COLOR, =BYTES, =RATING)
					if ( CC.EQ == format[ 0 ] ) format = null;
					// Do standard .NET formatting
					switch ( tc ) {
						case TypeCode.Byte: return ( (System.Byte) o ).ToString( format );
						case TypeCode.DateTime: return ( (System.DateTime) o ).ToString( format );
						case TypeCode.Decimal: return ( (System.Decimal) o ).ToString( format );
						case TypeCode.Double: return ( (System.Double) o ).ToString( format );
						case TypeCode.Int16: return ( (System.Int16) o ).ToString( format );
						case TypeCode.Int32: return ( (System.Int32) o ).ToString( format );
						case TypeCode.Int64: return ( (System.Int64) o ).ToString( format );
						case TypeCode.SByte: return ( (System.SByte) o ).ToString( format );
						case TypeCode.Single: return ( (System.Single) o ).ToString( format );
						case TypeCode.UInt16: return ( (System.UInt16) o ).ToString( format );
						case TypeCode.UInt32: return ( (System.UInt32) o ).ToString( format );
						case TypeCode.UInt64: return ( (System.UInt64) o ).ToString( format );
						default: return o.ToString();
					}
				} else { // Without format string use plain formatting
					return o.ToString();
				}
			} catch ( Exception ex ) {
				Log.Exception( "DisplayFormat", ex );
			}
			return CS.Empty;
		}
		//
		///<summary>Get type code of object.</summary>
		public static TypeCode ToTypeCode( this object o ) {
			if ( o is null ) return TypeCode.Empty;
			Type t = o.GetType();
			if ( Log.Deny( t is null ) ) return TypeCode.Empty;
			TypeCode tc = Type.GetTypeCode( t );
			return tc;
		}
		//
		//
		///<summary>Check is the type code of one object is the same as that of another object.</summary>
		public static bool CheckTypeCode( this object o, TypeCode tcc ) {
			if ( o is null ) return false;
			TypeCode tc = ToTypeCode( o );
			return tc == tcc;
		}
		//
		//
		///<summary>Convert any object to int, return 0 on error.</summary>
		public static int ToInt( this object v ) {
			if ( v is null ) return 0;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			int r = 0;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v ? 1 : 0; break;
				case TypeCode.Byte: r = Convert.ToInt32( (byte) v ); break;
				case TypeCode.Char: r = Convert.ToInt32( (char) v ); break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: r = (int) (decimal) v; break;
				case TypeCode.Double: r = (int) (double) v; break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = (int) ( (Int16) v ); break;
				case TypeCode.Int32: r = (int) ( (Int32) v ); break;
				case TypeCode.Int64: r = (int) ( (Int64) v ); break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = Convert.ToInt32( (sbyte) v ); break;
				case TypeCode.Single: r = (int) (Single) v; break;
				case TypeCode.String: r = ( (string) v ).ToInt(); break;
				case TypeCode.UInt16: r = (int) ( (Int16) v ); break;
				case TypeCode.UInt32: r = (int) ( (Int32) v ); break;
				case TypeCode.UInt64: r = (int) ( (Int64) v ); break;
			}
			return r;
		}
		///<summary>Convert any object to long, return 0 on error.</summary>
		public static long ToLong( this object v ) {
			if ( v is null ) return 0;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			long r = 0;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v ? 1 : 0; break;
				case TypeCode.Byte: r = Convert.ToInt32( (byte) v ); break;
				case TypeCode.Char: r = Convert.ToInt32( (char) v ); break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: r = (long) (decimal) v; break;
				case TypeCode.Double: r = (long) (double) v; break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = (long) ( (Int16) v ); break;
				case TypeCode.Int32: r = (long) ( (Int32) v ); break;
				case TypeCode.Int64: r = (long) ( (Int64) v ); break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = Convert.ToInt32( (sbyte) v ); break;
				case TypeCode.Single: r = (long) (Single) v; break;
				case TypeCode.String: r = ( (string) v ).ToLong(); break;
				case TypeCode.UInt16: r = (long) ( (Int16) v ); break;
				case TypeCode.UInt32: r = (long) ( (Int32) v ); break;
				case TypeCode.UInt64: r = (long) ( (Int64) v ); break;
			}
			return r;
		}
		//
#if NET7_0_OR_GREATER
		//
		///<summary>Convert any object to UInt16, return fallback on error.</summary>
		public static UInt16 ToUInt16( this object o, UInt16 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (ushort) ( (bool) o ? 1 : 0 );
				case TypeCode.Byte: return Convert.ToUInt16( (byte) o );
				case TypeCode.Char: return Convert.ToUInt16( (char) o );
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: return (UInt16) (decimal) o;
				case TypeCode.Double: return (UInt16) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (UInt16) ( (Int16) o );
				case TypeCode.Int32: return (UInt16) ( (Int32) o );
				case TypeCode.Int64: return (UInt16) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToUInt16( (sbyte) o );
				case TypeCode.Single: return (UInt16) (Single) o;
				case TypeCode.String: return UInt16.Parse( (string) o );
				case TypeCode.UInt16: return (UInt16) ( (UInt16) o );
				case TypeCode.UInt32: return (UInt16) ( (UInt32) o );
				case TypeCode.UInt64: return (UInt16) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is UInt128 ) {
						return (UInt16) ( ( (UInt128) o ) & UInt128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (UInt16) ( ( (BigInteger) o ) & UInt128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		///<summary>Convert any object to UInt32, return fallback on error.</summary>
		public static UInt32 ToUInt32( this object o, UInt32 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1u : 0;
				case TypeCode.Byte: return Convert.ToUInt32( (byte) o );
				case TypeCode.Char: return Convert.ToUInt32( (char) o );
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: return (UInt32) (decimal) o;
				case TypeCode.Double: return (UInt32) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (UInt32) ( (Int16) o );
				case TypeCode.Int32: return (UInt32) ( (Int32) o );
				case TypeCode.Int64: return (UInt32) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToUInt32( (sbyte) o );
				case TypeCode.Single: return (UInt32) (Single) o;
				case TypeCode.String: return UInt32.Parse( (string) o );
				case TypeCode.UInt16: return (UInt32) ( (UInt16) o );
				case TypeCode.UInt32: return (UInt32) ( (UInt32) o );
				case TypeCode.UInt64: return (UInt32) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is UInt128 ) {
						return (UInt32) ( ( (UInt128) o ) & UInt128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (UInt32) ( ( (BigInteger) o ) & UInt128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		//
		///<summary>Convert any object to UInt64, return fallback on error.</summary>
		public static UInt64 ToUInt64( this object o, UInt64 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1u : 0;
				case TypeCode.Byte: return Convert.ToUInt64( (byte) o );
				case TypeCode.Char: return Convert.ToUInt64( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (UInt64) (decimal) o;
				case TypeCode.Double: return (UInt64) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (UInt64) ( (Int16) o );
				case TypeCode.Int32: return (UInt64) ( (Int32) o );
				case TypeCode.Int64: return (UInt64) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToUInt64( (sbyte) o );
				case TypeCode.Single: return (UInt64) (Single) o;
				case TypeCode.String: return UInt64.Parse( (string) o );
				case TypeCode.UInt16: return (UInt64) ( (UInt16) o );
				case TypeCode.UInt32: return (UInt64) ( (UInt32) o );
				case TypeCode.UInt64: return (UInt64) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is UInt128 ) {
						return (UInt64) ( ( (UInt128) o ) & UInt128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (UInt64) ( ( (BigInteger) o ) & UInt128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		//
		///<summary>Convert any object to UInt128, return fallback on error.</summary>
		public static UInt128 ToUInt128( this object o, UInt128 fallback ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1u : 0;
				case TypeCode.Byte: return Convert.ToUInt32( (byte) o );
				case TypeCode.Char: return Convert.ToUInt32( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (UInt128) (decimal) o;
				case TypeCode.Double: return (UInt128) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (UInt128) ( (Int16) o );
				case TypeCode.Int32: return (UInt128) ( (Int32) o );
				case TypeCode.Int64: return (UInt128) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToUInt32( (sbyte) o );
				case TypeCode.Single: return (UInt128) (Single) o;
				case TypeCode.String: return UInt128.Parse( (string) o );
				case TypeCode.UInt16: return (UInt128) ( (UInt16) o );
				case TypeCode.UInt32: return (UInt128) ( (UInt32) o );
				case TypeCode.UInt64: return (UInt128) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is UInt128 ) {
						return (UInt128) ( ( (UInt128) o ) & UInt128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (UInt128) ( ( (BigInteger) o ) & UInt128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		///<summary>Convert any object to Int16, return fallback on error.</summary>
		public static Int16 ToInt16( this object o, Int16 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (short) ( (bool) o ? 1 : 0 );
				case TypeCode.Byte: return Convert.ToInt16( (byte) o );
				case TypeCode.Char: return Convert.ToInt16( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (Int16) (decimal) o;
				case TypeCode.Double: return (Int16) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (Int16) ( (Int16) o );
				case TypeCode.Int32: return (Int16) ( (Int32) o );
				case TypeCode.Int64: return (Int16) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToInt16( (sbyte) o );
				case TypeCode.Single: return (Int16) (Single) o;
				case TypeCode.String: return Int16.Parse( (string) o );
				case TypeCode.UInt16: return (Int16) ( (UInt16) o );
				case TypeCode.UInt32: return (Int16) ( (UInt32) o );
				case TypeCode.UInt64: return (Int16) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is Int128 ) {
						return (Int16) ( ( (Int128) o ) & Int128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (Int16) ( ( (BigInteger) o ) & Int128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		///<summary>Convert any object to Int32, return fallback on error.</summary>
		public static Int32 ToInt32( this object o, Int32 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1 : 0;
				case TypeCode.Byte: return Convert.ToInt32( (byte) o );
				case TypeCode.Char: return Convert.ToInt32( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (Int32) (decimal) o;
				case TypeCode.Double: return (Int32) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (Int32) ( (Int16) o );
				case TypeCode.Int32: return (Int32) ( (Int32) o );
				case TypeCode.Int64: return (Int32) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToInt32( (sbyte) o );
				case TypeCode.Single: return (Int32) (Single) o;
				case TypeCode.String: return Int32.Parse( (string) o );
				case TypeCode.UInt16: return (Int32) ( (UInt16) o );
				case TypeCode.UInt32: return (Int32) ( (UInt32) o );
				case TypeCode.UInt64: return (Int32) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is Int128 ) {
						return (Int32) ( ( (Int128) o ) & Int128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (Int32) ( ( (BigInteger) o ) & Int128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		//
		///<summary>Convert any object to Int64, return fallback on error.</summary>
		public static Int64 ToInt64( this object o, Int64 fallback = 0 ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1u : 0;
				case TypeCode.Byte: return Convert.ToInt64( (byte) o );
				case TypeCode.Char: return Convert.ToInt64( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (Int64) (decimal) o;
				case TypeCode.Double: return (Int64) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (Int64) ( (Int16) o );
				case TypeCode.Int32: return (Int64) ( (Int32) o );
				case TypeCode.Int64: return (Int64) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToInt64( (sbyte) o );
				case TypeCode.Single: return (Int64) (Single) o;
				case TypeCode.String: return Int64.Parse( (string) o );
				case TypeCode.UInt16: return (Int64) ( (UInt16) o );
				case TypeCode.UInt32: return (Int64) ( (UInt32) o );
				case TypeCode.UInt64: return (Int64) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is Int128 ) {
						return (Int64) ( ( (Int128) o ) & Int128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (Int64) ( ( (BigInteger) o ) & Int128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
		//
		///<summary>Convert any object to Int128, return fallback on error.</summary>
		public static Int128 ToInt128( this object o, Int128 fallback ) {
			if ( o is null ) return fallback;
			// Intentionally NOT match using pattern matching on type, because slower.
			var t = o.GetType();
			var c = Type.GetTypeCode( t );
			switch ( c ) {
				case TypeCode.Boolean: return (bool) o ? 1u : 0;
				case TypeCode.Byte: return Convert.ToInt32( (byte) o );
				case TypeCode.Char: return Convert.ToInt32( (char) o );
				case TypeCode.DBNull:
				case TypeCode.DateTime:
				case TypeCode.Decimal: return (Int128) (decimal) o;
				case TypeCode.Double: return (Int128) (double) o;
				case TypeCode.Empty:
				case TypeCode.Int16: return (Int128) ( (Int16) o );
				case TypeCode.Int32: return (Int128) ( (Int32) o );
				case TypeCode.Int64: return (Int128) ( (Int64) o );
				case TypeCode.SByte: return Convert.ToInt32( (sbyte) o );
				case TypeCode.Single: return (Int128) (Single) o;
				case TypeCode.String: return Int128.Parse( (string) o );
				case TypeCode.UInt16: return (Int128) ( (UInt16) o );
				case TypeCode.UInt32: return (Int128) ( (UInt32) o );
				case TypeCode.UInt64: return (Int128) ( (UInt64) o );
				case TypeCode.Object:
					if ( o is Int128 ) {
						return (Int128) ( ( (Int128) o ) & Int128.MaxValue );
					}
					if ( o is BigInteger big ) {
						return (Int128) ( ( (BigInteger) o ) & Int128.MaxValue );
					}
					break;
			}
			return fallback;
		}
		//
#endif
		//
		///<summary>Convert any object to float, return 0 on error.</summary>
		public static float ToFloat( this object v ) {
			if ( v is null ) return 0;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			float r = 0;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v ? 1 : 0; break;
				case TypeCode.Byte: r = (float) Convert.ToDouble( (byte) v ); break;
				case TypeCode.Char: r = (float) Convert.ToDouble( (char) v ); break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: r = (float) ( (decimal) v ); break;
				case TypeCode.Double: r = (float) ( (double) v ); break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = (float) ( (Int16) v ); break;
				case TypeCode.Int32: r = (float) ( (Int32) v ); break;
				case TypeCode.Int64: r = (float) ( (Int64) v ); break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = (float) Convert.ToDouble( (sbyte) v ); break;
				case TypeCode.Single: r = (float) ( (Single) v ); break;
				case TypeCode.String: r = (float) ( (string) v ).ToDouble(); break;
				case TypeCode.UInt16: r = (float) ( (UInt16) v ); break;
				case TypeCode.UInt32: r = (float) ( (UInt32) v ); break;
				case TypeCode.UInt64: r = (float) ( (UInt64) v ); break;
			}
			return r;
		}
		//
		///<summary>Convert any object to double, return 0 on error.</summary>
		public static double ToDouble( this object v ) {
			if ( v is null ) return 0;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			double r = 0;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v ? 1 : 0; break;
				case TypeCode.Byte: r = Convert.ToDouble( (byte) v ); break;
				case TypeCode.Char: r = Convert.ToDouble( (char) v ); break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: r = (double) ( (decimal) v ); break;
				case TypeCode.Double: r = ( (double) v ); break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = (double) ( (Int16) v ); break;
				case TypeCode.Int32: r = (double) ( (Int32) v ); break;
				case TypeCode.Int64: r = (double) ( (Int64) v ); break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = Convert.ToDouble( (sbyte) v ); break;
				case TypeCode.Single: r = (double) ( (Single) v ); break;
				case TypeCode.String: r = ( (string) v ).ToDouble(); break;
				case TypeCode.UInt16: r = (double) ( (UInt16) v ); break;
				case TypeCode.UInt32: r = (double) ( (UInt32) v ); break;
				case TypeCode.UInt64: r = (double) ( (UInt64) v ); break;
			}
			return r;
		}
		//
		///<summary>Convert any object to decimal, return 0 on error.</summary>
		public static decimal ToDecimal( this object v ) {
			if ( v is null ) return 0;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			decimal r = 0;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v ? 1 : 0; break;
				case TypeCode.Byte: r = Convert.ToDecimal( (byte) v ); break;
				case TypeCode.Char: r = Convert.ToDecimal( (char) v ); break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Double: r = (decimal) ( (double) v ); break;
				case TypeCode.Decimal: r = ( (decimal) v ); break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = (decimal) ( (Int16) v ); break;
				case TypeCode.Int32: r = (decimal) ( (Int32) v ); break;
				case TypeCode.Int64: r = (decimal) ( (Int64) v ); break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = Convert.ToDecimal( (sbyte) v ); break;
				case TypeCode.Single: r = (decimal) ( (Single) v ); break;
				case TypeCode.String: r = ( (string) v ).ToDecimal(); break;
				case TypeCode.UInt16: r = (decimal) ( (UInt16) v ); break;
				case TypeCode.UInt32: r = (decimal) ( (UInt32) v ); break;
				case TypeCode.UInt64: r = (decimal) ( (UInt64) v ); break;
			}
			return r;
		}
		//
		///<summary>Convert any object to bool, return false on error.</summary>
		public static bool ToBool( this object v ) {
			if ( v is null ) return false;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			bool r = false;
			switch ( c ) {
				case TypeCode.Boolean: r = (bool) v; break;
				case TypeCode.Byte: r = 0 < (byte) v; break;
				case TypeCode.Char: r = '1' == (char) v; break;
				case TypeCode.DBNull: break;
				case TypeCode.DateTime: break;
				case TypeCode.Decimal: r = 0.1m < (decimal) v; break;
				case TypeCode.Double: r = 0.1 < (double) v; break;
				case TypeCode.Empty: break;
				case TypeCode.Int16: r = 0 != (Int16) v; break;
				case TypeCode.Int32: r = 0 != (Int32) v; break;
				case TypeCode.Int64: r = 0 != (Int64) v; break;
				case TypeCode.Object: break;
				case TypeCode.SByte: r = 0 < (sbyte) v; break;
				case TypeCode.Single: r = 0.1 < (Single) v; break;
				case TypeCode.String:
					var s = v as string;
					if ( CS.N1 == s || CS.true_ == s.ToLowerInvariant() ) r = true;
					break;
				case TypeCode.UInt16: r = 0 != (UInt16) v; break;
				case TypeCode.UInt32: r = 0 != (UInt32) v; break;
				case TypeCode.UInt64: r = 0 != (UInt64) v; break;
			}
			return r;
		}
		//
		///<summary>Convert any object to DateTime, return DateTime.MinValue on error.</summary>
		public static DateTime ToDateTime( this object v ) {
			if ( v is null ) return DateTime.MinValue;
			var t = v.GetType();
			var c = Type.GetTypeCode( t );
			var r = DateTime.MinValue;
			switch ( c ) {
				case TypeCode.DateTime: r = (DateTime) v; break;
				case TypeCode.Int16: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
				case TypeCode.Int32: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
				case TypeCode.Int64: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
				case TypeCode.String: r = DateTimeEx2.ToDateTime( (string) v ); break;
				case TypeCode.UInt16: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
				case TypeCode.UInt32: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
				case TypeCode.UInt64: r = new DateTime( (long) v, DateTimeKind.Utc ); break;
			}
			return r;
		}
		//
		///<summary>Convert object into string. If it is null, return empty string. Never return null.</summary>
		public static string StringValue( this object o ) {
			if ( o is null ) return CS.Empty;
			if ( o is string ) return o as string;
			return o.ToString();
		}
		//
		//
	}
	//
	//
	//
	//
	public static class BoolEx {
		//
		public static bool GetCharBool( char c ) {
			c = Char.ToLowerInvariant( c );
			// Supports: yes, true, ja, checked, on, 1
			return 'y' == c || 't' == c || 'j' == c || 'c' == c || 'o' == c || '1' == c;
		}
		//
		public static bool GetCharBool( string s ) {
			if ( s.IsNullOrEmpty() ) return false;
			s = s.Trim();
			if ( 0 == s.Length ) return false;
			return GetCharBool( s[ 0 ] );
		}
		//
		///<summary>Boolean Value.</summary>
		public static string ToJson( this bool v ) {
			return v ? CS.true_ : CS.false_;
		}
		//
		public static bool GetCharBool( this IDataReader rdr, int pos ) {
			var o = rdr.GetValue( pos );
			var t = o.GetType();
			var tc = Type.GetTypeCode( t );
			switch ( tc ) {
				case TypeCode.Boolean: return (bool) o;
				case TypeCode.Byte: return 0 != (byte) o;
				case TypeCode.Char: return GetCharBool( (char) o );
				case TypeCode.DBNull: return false;
				case TypeCode.DateTime: return false;
				case TypeCode.Decimal: return 0 != (Decimal) o;
				case TypeCode.Double: return 0 != (Double) o;
				case TypeCode.Empty: return false;
				case TypeCode.Int16: return 0 != (Int16) o;
				case TypeCode.Int32: return 0 != (Int32) o;
				case TypeCode.Int64: return 0 != (Int64) o;
				case TypeCode.Object: return false;
				case TypeCode.SByte: return 0 != (SByte) o;
				case TypeCode.Single: return 0 != (Single) o;
				case TypeCode.String: return GetCharBool( (string) o );
				case TypeCode.UInt16: return 0 != (UInt16) o;
				case TypeCode.UInt32: return 0 != (UInt32) o;
				case TypeCode.UInt64: return 0 != (UInt64) o;
				default: break;
			}
			return false;
		}
		//
		//
		///<summary>Compare two Boolean values.</summary>
		public static int Compare( bool a, bool b ) {
			if ( a == b ) return 0;
			if ( a ) return 1;
			return -1;
		}
		//
		//
		///<summary>Shorter notation for "value = value && expression", but expression is always evaluated.</summary>
		public static bool Check( ref this bool value, bool expression ) {
			if ( value ) {
				if ( !expression ) {
					value = false;
				}
			}
			return value;
		}
		//
		//
		///<summary>Shorter notation for "if( condition ) value = value && expression", but expression is always evaluated.</summary>
		public static bool Check( ref this bool value, bool condition, bool expression ) {
			if ( condition ) {
				if ( value ) {
					if ( !expression ) {
						value = false;
					}
				}
			}
			return value;
		}
		//
		//
		public static bool Any( ref this bool value, bool expression ) {
			if ( !value ) {
				if ( expression ) {
					value = true;
				}
			}
			return value;
		}
		//
		//
		public static bool Any( ref this bool value, bool condition, bool expression ) {
			if ( condition ) {
				if ( !value ) {
					if ( expression ) {
						value = true;
					}
				}
			}
			return value;
		}
	}
	//
	//
	//
	//
	//
	public static partial class IntegerEx {
		//
		///<summary>Format a number with leading zeros.</summary>
		public static string LeadingZero( this long value, int digits ) {
			var negative = value < 0;
			if ( negative ) value = Math.Abs( value );
			var ca = new char[ digits ];
			for ( int i = 0; i < digits; i++ ) ca[ i ] = CC.N0;
			for ( int i = 0; i < digits; i++ ) {
				var r = value % 10;
				ca[ digits - i - 1 ] = (char) ( r + '0' );
				value = value / 10;
			}
			if ( negative ) ca[ 0 ] = CC.HY;
			return new string( ca );
		}
		//
		///<summary>Format a number with leading zeros.</summary>
		public static string LeadingZero( this int value, int digits ) {
			return ( (long) value ).LeadingZero( digits );
		}
	}
	//
	//
	//
	//
	//
	//
	public static class Bit {
		//
		// GET
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Test if the bit is set.</summary>
		public static bool Get( ulong value, int pos ) { return 0 != ( value & ( 1ul << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Test if the bit is set.</summary>
		public static bool Get( uint value, int pos ) { return 0 != ( value & ( 1 << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Test if the bit is set.</summary>
		public static bool Get( ushort value, int pos ) { return 0 != ( value & ( 1 << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Test if the bit is set.</summary>
		public static bool Get( byte value, int pos ) { return 0 != ( value & ( 1 << pos ) ); }
		//
		// SET
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Set the bit.</summary>
		public static ulong Set( ulong value, int pos, bool bit ) { return ( value | ( 1ul << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Set the bit.</summary>
		public static uint Set( uint value, int pos, bool bit ) { return ( value | ( 1u << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Set the bit.</summary>
		public static ushort Set( ushort value, int pos, bool bit ) { return (ushort) ( value | ( 1u << pos ) ); }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Set the bit.</summary>
		public static byte Set( byte value, int pos, bool bit ) { return (byte) ( value | (byte) ( 1 << pos ) ); }
		//
		// CLEAR
		//
		///<summary>Clear the bit.</summary>
		public static ulong Clear( ulong value, int pos ) { return value & ~( 1u << pos ); }
		///<summary>Clear the bit.</summary>
		public static uint Clear( uint value, int pos ) { return value & ~( 1u << pos ); }
		///<summary>Clear the bit.</summary>
		public static ushort Clear( ushort value, int pos ) { return (ushort) ( (uint) value & (uint) ~( 1u << pos ) ); }
		///<summary>Clear the bit.</summary>
		public static byte Clear( byte value, int pos ) { return (byte) ( (uint) value & (uint) ~( 1u << pos ) ); }
		//
		// ANY
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if any set bit is matching.</summary>
		public static bool Any( ulong a, ulong b ) { return 0 != ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if any set bit is matching.</summary>
		public static bool Any( uint a, uint b ) { return 0 != ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if any set bit is matching.</summary>
		public static bool Any( ushort a, ushort b ) { return 0 != ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if any set bit is matching.</summary>
		public static bool Any( byte a, byte b ) { return 0 != ( a & b ); }
		//
		// ALL
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if all set bits of b are found in a.</summary>
		public static bool All( ulong a, ulong b ) { return b == ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if all set bits of b are found in a.</summary>
		public static bool All( uint a, uint b ) { return b == ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if all set bits of b are found in a.</summary>
		public static bool All( ushort a, ushort b ) { return b == ( a & b ); }
		// 
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Check if all set bits of b are found in a.</summary>
		public static bool All( byte a, byte b ) { return b == ( a & b ); }
		//
		// UTILITIES
		// 
		///<summary>Count the number of on or off bits in a.</summary>
		public static int Count( ulong a, bool on ) {
			int count = 0;
			for ( var i = 0; i < 64; i++ ) {
				if ( on == Bit.Get( a, i ) ) count++;
			}
			return count;
		}
		//
		///<summary>Convert the bits into an array. </summary>
		public static ushort[] ToArray( ulong value, Tri which ) {
			int count = 0;
			switch ( which ) {
				case Tri.On: count = Count( value, true ); break;
				case Tri.Off: count = Count( value, false ); break;
				case Tri.All: count = 64; break;
			}
			const ushort one = 1; const ushort zero = 0;
			int w = 0; bool b; ushort v = 0;
			var ai = new ushort[ count ];
			for ( int i = 0; i < 64; i++ ) {
				b = Get( value, i );
				v = (ushort) ( one << i );
				switch ( which ) {
					case Tri.On: if ( b ) ai[ w++ ] = v; break;
					case Tri.Off: if ( !b ) ai[ w++ ] = v; break;
					case Tri.All: ai[ w++ ] = b ? v : zero; break;
				}
			}
			return ai;
		}
	}
	//
	//
	public static partial class DoubleEx {
		//
		///<summary>Test if this number is NaN.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNaN( this System.Single v ) {
			return System.Single.IsNaN( v );
		}
		//
		///<summary>Test if this number is NaN.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNaN( this System.Double v ) {
			return System.Double.IsNaN( v );
		}
		//
		///<summary>Test if this number is Infinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsInfinity( this System.Single v ) {
			return System.Single.IsInfinity( v );
		}
		//
		///<summary>Test if this number is Infinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsInfinity( this System.Double v ) {
			return System.Double.IsInfinity( v );
		}
		//
		///<summary>Test if this number is PositiveInfinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsPositiveInfinity( this System.Single v ) {
			return v.IsNear( Single.PositiveInfinity );
		}
		//
		///<summary>Test if this number is PositiveInfinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsPositiveInfinity( this System.Double v ) {
			return v.IsNear( Double.PositiveInfinity );
		}
		//
		///<summary>Test if this number is NegativeInfinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNegativeInfinity( this System.Single v ) {
			return v.IsNear( Single.NegativeInfinity );
		}
		//
		///<summary>Test if this number is NegativeInfinity.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNegativeInfinity( this System.Double v ) {
			return v.IsNear( Double.NegativeInfinity );
		}
		//
		///<summary>Test if this is a valid number.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNumber( this System.Single v ) {
			return !System.Single.IsInfinity( v ) && !System.Single.IsNaN( v );
		}
		//
		///<summary>Test if this is a valid number.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNumber( this System.Double v ) {
			return !System.Double.IsInfinity( v ) && !System.Double.IsNaN( v );
		}
		///<summary>Test if this first value is approximately equal to the second value.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNear( this double me, double other, double delta = 0.0001 ) {
			return Math.Abs( me - other ) < delta;
		}
		///<summary>Test if this first value is approximately equal to the second value.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsNear( this float me, float other, float delta = (float) 0.0001 ) {
			return Math.Abs( me - other ) < delta;
		}
		///<summary>Test if this first value is approximately equal to zero.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsZero( this double me, double delta = 0.0001 ) {
			return Math.Abs( me ) < delta;
		}
		///<summary>Test if this first value is approximately equal to zero.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static bool IsZero( this float me, float delta = (float) 0.0001 ) {
			return Math.Abs( me ) < delta;
		}
		///<summary>Multiply combination of double and decimal.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static decimal Multiply( this double me, decimal value ) {
			return (decimal) ( (decimal) me * value );
		}
		///<summary>Multiply combination of decimal and double.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static decimal Multiply( this decimal me, double value ) {
			return (decimal) ( me * (decimal) value );
		}
		///<summary>Convert the number into an integer after multiplying with 10000.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static long Decimals4( this double value ) {
			return (long) Math.Round( value * 10000.0000, 0 );
		}
		///<summary>Convert the number into an integer after multiplying with 10000.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static long Decimals4( this decimal value ) {
			return (long) Math.Round( value * 10000.0000M, 0 );
		}
		///<summary>Convert the number into an integer after multiplying with 100.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static long Decimals2( this double value ) {
			return (long) Math.Round( value * 100.0000, 0 );
		}
		///<summary>Convert the number into an integer after multiplying with 100.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static long Decimals2( this decimal value ) {
			return (long) Math.Round( value * 100.0000M, 0 );
		}
		///<summary>Convert from integer and then divide by10000.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static double DoubleFrom4( this long value ) {
			return ( (double) value ) / 10000.0000;
		}
		///<summary>Convert from integer and then divide by10000.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static decimal DecimalFrom4( this long value ) {
			return ( (decimal) value ) / 10000.0000M;
		}
		///<summary>Convert from integer and then divide by100.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static double DoubleFrom2( this long value ) {
			return ( (double) value ) / 100.0000;
		}
		///<summary>Convert from integer and then divide by100.</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static decimal DecimalFrom2( this long value ) {
			return ( (decimal) value ) / 100.0000M;
		}
	}
	//
	//
	public static partial class DecimalEx {
		//
		///<summary>Test if this first value is approximately equal to the second value.</summary>
		public static bool IsNear( this Decimal me, Decimal other, Decimal delta = 0.0001m ) {
			return Math.Abs( me - other ) < delta;
		}
		///<summary>Test if this first value is approximately equal to zero.</summary>
		public static bool IsZero( this Decimal me, Decimal delta = 0.0001m ) {
			return Math.Abs( me ) < delta;
		}
	}
	//
	//
	public static partial class ComparableEx {
		//
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this short x, short a, short b ) {
			return x >= a && x <= b;
		}
		//
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this ushort x, ushort a, ushort b ) {
			return x >= a && x <= b;
		}
		//
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this int x, int a, int b ) {
			return x >= a && x <= b;
		}
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this long x, long a, long b ) {
			return x >= a && x <= b;
		}
		//
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this ulong x, ulong a, ulong b ) {
			return x >= a && x <= b;
		}
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this double x, double a, double b ) {
			return x >= a && x <= b;
		}
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool Between( this decimal x, decimal a, decimal b ) {
			return x >= a && x <= b;
		}
		//
		///<summary>Test if x is between (including) a and b.</summary>
		public static bool IsBetween<T>( this T x, T a, T b ) where T : IComparable, IComparable<T> {
			return x.CompareTo( a ) >= 0 && x.CompareTo( b ) <= 0;
		}
		//
		///<summary>Limit x to values between (including) a and b.</summary>
		public static short Limit( this short x, short a, short b ) {
			if ( x < a ) return a;
			if ( x > b ) return b;
			return x;
		}
		//
		///<summary>Limit x to values between (including) a and b.</summary>
		public static int Limit( this int x, int a, int b ) {
			if ( x < a ) return a;
			if ( x > b ) return b;
			return x;
		}
		///<summary>Limit x to values between (including) a and b.</summary>
		public static long Limit( this long x, long a, long b ) {
			if ( x < a ) return a;
			if ( x > b ) return b;
			return x;
		}
		///<summary>Limit x to values between (including) a and b.</summary>
		public static double Limit( this double x, double a, double b ) {
			if ( x < a ) return a;
			if ( x > b ) return b;
			return x;
		}
		///<summary>Limit x to values between (including) a and b.</summary>
		public static decimal Limit( this decimal x, decimal a, decimal b ) {
			if ( x < a ) return a;
			if ( x > b ) return b;
			return x;
		}
		//
		///<summary>Limit x to values between (including) a and b.</summary>
		public static T Limit<T>( this T x, T a, T b ) where T : IComparable, IComparable<T> {
			var r = x.CompareTo( a );
			if ( r < 0 ) return a;
			r = x.CompareTo( b );
			if ( r > 0 ) return b;
			return x;
		}
	}
	//
	//
	public static partial class CharEx {
		//
		//
		///<summary>Check if the character is valid in XML 1.0</summary>
		// http://en.wikipedia.org/wiki/List_of_Unicode_characters
		// http://en.wikipedia.org/wiki/Valid_Characters_in_XML#XML_1.1
		// http://seattlesoftware.wordpress.com/2008/09/11/hexadecimal-value-0-is-an-invalid-character/
		public static bool IsXml( this char ch ) {
			return ( ch >= 0x20 && ch < 0x7F ) // normal ANSI characters
				|| ch == 0x9 // '\t'
				|| ch == 0xA // '\n'
				|| ch == 0xD // '\r'
				|| ch == 0x85 // next line
				|| ( ch >= 0xA0 && ch <= 0xD7FF ) // extended characters
				|| ( ch >= 0xE000 && ch <= 0xFFFD );
		}
	}
	//
	//
	//
	public static class DictionaryEx {
		//
		// GET
		//
		public static T TryGet<K, T>( this Dictionary<K, T> dict, K key ) {
			if ( dict.TryGetValue( key, out var t ) ) {
				return t;
			}
			return default;
		}
		//
		public static T TryGet<K, T>( this SortedDictionary<K, T> dict, K key ) {
			if ( dict.TryGetValue( key, out var t ) ) {
				return t;
			}
			return default;
		}
		//
		// ADD
		//
		public static void TryAdd<K, T>( this Dictionary<K, T> dict, K key, T value ) {
			if ( !dict.ContainsKey( key ) ) {
				dict.Add( key, value );
			}
		}
		//
		public static void TryAdd<K, T>( this SortedDictionary<K, T> dict, K key, T value ) {
			if ( !dict.ContainsKey( key ) ) {
				dict.Add( key, value );
			}
		}
		//
		// ADD FROM DICTIONARY
		//
		public static void AddRange<K, T>( this Dictionary<K, T> dict, Dictionary<K, T> other, bool replace = false ) {
			foreach ( var kv in other ) {
				if ( !dict.ContainsKey( kv.Key ) ) {
					dict.Add( kv.Key, kv.Value );
				} else if ( replace ) {
					dict[ kv.Key ] = kv.Value;
				}
			}
		}
		//
		public static void AddRange<K, T>( this SortedDictionary<K, T> dict, SortedDictionary<K, T> other, bool replace = false ) {
			foreach ( var kv in other ) {
				if ( !dict.ContainsKey( kv.Key ) ) {
					dict.Add( kv.Key, kv.Value );
				} else if ( replace ) {
					dict[ kv.Key ] = kv.Value;
				}
			}
		}
	}
	//
	//
	public static class HashSetEx {
		//
		public static bool Set<T>( this HashSet<T> hash, T key ) {
			if ( hash.Contains( key ) ) return true;
			hash.Add( key ); return false;
		}
		//
		public static int Add<T>( this HashSet<T> hash, T[] a ) {
			if ( a is null ) return 0;
			var count = 0;
			for ( int i = 0; i < a.Length; i++ ) {
				var found = Set( hash, a[ i ] );
				if ( !found ) count++;
			}
			return count;
		}
	}
	//
	//
	public static class CollectionEx {
		//
		public static bool Set<T>( this ICollection<T> collection, T key ) where T : class {
			if ( !collection.Contains( key ) ) {
				collection.Add( key );
				return true;
			}
			return false;
		}
		//
		public static string Join<T>( this ICollection<T> collection, string separator ) where T : class {
			var i = 0;
			var count = collection.Count;
			var sa = new string[ count ];
			foreach ( T item in collection ) {
				if ( item is not null ) {
					sa[ i++ ] = item.ToString();
				}
			}
			return String.Join( separator, sa, 0, i );
		}
		//
		// TASK: Reverse
		public static string Join<T>( this Stack<T> stack, string separator ) where T : class {
			var a = stack.ToArray();
			Array.Reverse( a );
			return a.Join( separator );
		}
	}
	//
	//
	///<summary>Implements table-based string replacement with regular expressions.</summary>
	public class ReplaceAll {
		//
		//
		public class Item : IComparable<Item> {
			public string From;
			public string Into;
			public Regex Pattern;
			//
			public int CompareTo( Item other ) {
				return this.From.CompareTo( other.From );
			}
			public override string ToString() {
				var sb = new StringBuilder();
				sb.Append( CS.C1 ).Append( From ).Append( CS.CmSp ).Append( Into ).Append( CS.C2 );
				return sb.ToString();
			}
		}
		//
		//
		protected bool _inline;
		protected bool _complete;
		protected bool _ignoreCase;
		protected Item[] _items;
		//
		//
		private void MakeItems( string[,] patterns ) {
			var length = patterns.Length / 2;
			_items = new Item[ length ];
			for ( int i = 0; i < length; i++ ) {
				_items[ i ] = new Item {
					From = patterns[ i, 0 ],
					Into = patterns[ i, 1 ],
					Pattern = null
				};
			}
		}
		//
		//
		private void Sort() {
			Array.Sort( _items );
			Array.Reverse( _items );
		}
		//
		//
		///<summary>Create the internal regular expression replacement table.</summary>
		public ReplaceAll( RegexOptions options, string[] from, string[] into ) {
			if ( from is null || into is null || 0 == from.Length || 0 == into.Length || from.Length != into.Length ) throw new ArgumentException();
			_complete = false;
			_items = new Item[ from.Length ];
			for ( int i = 0; i < from.Length; i++ ) {
				var rx = new Regex( from[ i ], options );
				_items[ i ].From = from[ i ];
				_items[ i ].Into = into[ i ];
				_items[ i ].Pattern = rx;
			}
			Sort();
		}
		//
		//
		///<summary>Create the internal regular expression replacement table.</summary>
		public ReplaceAll( RegexOptions options, string[,] patterns ) {
			if ( patterns is null || 0 == patterns.Length ) throw new ArgumentException();
			_complete = false;
			MakeItems( patterns );
			for ( int i = 0; i < _items.Length; i++ ) {
				var rx = new Regex( patterns[ i, 0 ], options );
				_items[ i ].Pattern = rx;
			}
			Sort();
		}
		//
		//
		///<summary>Create the internal string replacement table.</summary>
		public ReplaceAll( string[,] patterns, bool ignoreCase, bool inline ) {
			if ( patterns is null || 0 == patterns.Length ) throw new ArgumentException();
			_complete = true;
			_inline = inline;
			_ignoreCase = ignoreCase;
			MakeItems( patterns );
			Sort();
		}
		//
		///<summary>Apply one regular expression replacements on the input string.</summary>
		public bool RunOne( ref string input, int row ) {
			// Get the item according to the row. If row is out of bounds, this might throw.
			var item = _items[ row ];
			if ( _complete ) {
				if ( _inline ) {
					input = input.Replace( item.From, item.Into );
					return false;
				} else {
					// String comparison
					var difference = String.Compare( item.From, input, _ignoreCase );
					if ( 0 == difference ) {
						input = item.Into;
						return true;
					}
				}
			} else {
				// Regular expressions: Does this match?
				var match = item.Pattern.Match( input );
				if ( match.Success ) {
					// Loop over all matches
					var start = 0;
					var sb = new StringBuilder();
					for ( int i = 0; i < match.Captures.Count; i++ ) {
						// Index and length of this match
						var index = match.Captures[ i ].Index;
						var length = match.Captures[ i ].Length;
						// Append the string outside of the match.
						sb.Append( input.Substring( start, index ) );
						// Append the replacement for the match.
						sb.Append( item.Into );
						// Move the start pointer. The start pointer is relative to the original input string.
						start = index + length;
					}
					// Append the remaining string.
					sb.Append( input.Substring( start ) );
					input = sb.ToString();
					return true;
				}
			}
			return false;
		}
		//
		///<summary>Apply all regular expression replacements on the input string.</summary>
		public bool Run( ref string input ) {
			if ( input.IsNullOrEmpty() ) return false;
			// Loop over all patterns
			for ( int i = 0; i < _items.Length; i++ ) {
				// Try every replacement on the input string
				if ( RunOne( ref input, i ) ) return true;
			}
			return false;
		}
		//
	}
	//
	//
	//
	//
	///<summary>Extensions for enumerable classes.</summary>
	public static class EnumerableEx {
		//
		///<summary>Check if the object is not null and its length is greater than 0.</summary>
		public static bool IsFilled<T>( this ICollection<T> list ) {
			return list is not null && list.Count > 0;
		}
		//
		///<summary>Check if the object is null or its length is 0.</summary>
		public static bool IsEmpty<T>( this ICollection<T> list ) {
			return list is null || list.Count == 0;
		}
		//
		///<summary>Create an array of a derived class from a list of a base class. Array elements may be null, if the cast was not possible.</summary>
		public static T[] ToArray<T>( this IList<T> list ) where T : class {
			if ( !list.IsFilled() ) return null;
			var a = new T[ list.Count ];
			for ( var i = 0; i < list.Count; i++ ) {
				a[ i ] = list[ i ];
			}
			return a;
		}
		//
		///<summary>Create an array of a derived class from a list of a base class. Array elements may be null, if the cast was not possible.</summary>
		public static T[] ToArray<T>( this ICollection<T> list ) where T : class {
			if ( !list.IsFilled() ) return null;
			var a = new T[ list.Count ];
			var i = 0;
			foreach ( var item in list ) {
				a[ i++ ] = item;
			}
			return a;
		}
		//
		///<summary>Find the maximum value</summary>
		public static T Max<T>( this ICollection<T> list ) where T : IComparable {
			T max = default( T );
			if ( list.IsFilled() ) {
				foreach ( var item in list ) {
					var d = item.CompareTo( max );
					if ( d > 0 ) {
						max = item;
					}
				}
			}
			return max;
		}
		//
		///<summary>Get the first element.</summary>
		public static T First<T>( this IEnumerable<T> list ) {
			if ( list is not null ) {
				foreach ( var value in list ) {
					return value;
				}
			}
			return default( T );
		}
		//
		///<summary>Get the last element. Not efficient, because it's iterating over the complete list.</summary>
		public static T Last<T>( this ICollection<T> list ) {
			var N = list.Count;
			var n = 0;
			foreach ( var value in list ) {
				n++;
				if ( n == N ) return value;
			}
			return default( T );
		}
		//
		///<summary>Get the N's element. Not efficient, because it's iterating over the list.</summary>
		public static T GetAt<T>( this ICollection<T> list, int N ) {
			var n = 0;
			foreach ( var value in list ) {
				if ( n == N ) return value;
				n++;
			}
			return default( T );
		}
	}
	//
	//
	//
	//
	public static class BitArrayEx {
		//
		///<summary>Check if any object within the container has that given value.</summary>
		public static bool Any( this BitArray a, bool t ) {
			var length = a.Length;
			for ( int i = 0; i < length; i++ ) {
				if ( a[ i ] == t ) return true;
			}
			return false;
		}
		//
		///<summary>Return all indexes, which match t.</summary>
		public static List<int> All( this BitArray a, bool t ) {
			var list = new List<int>();
			var length = a.Length;
			for ( int i = 0; i < length; i++ ) {
				if ( a[ i ] == t ) list.Add( i );
			}
			return list;
		}
		//
		///<summary>Check if any 2 bits within the bitmap have the given value.</summary>
		public static bool Match( this BitArray a, BitArray b, bool t = true ) {
			var l = a.Length;
			for ( int i = 0; i < l; i++ ) {
				if ( a[ i ] == t ) {
					if ( b[ i ] == t ) {
						return true;
					}
				}
			}
			return false;
		}
		//
		///<summary>Display the bit array</summary>
		public static string ToString( this BitArray a ) {
			var l = a.Length;
			var ca = new char[ l ];
			for ( int i = 0; i < l; i++ ) {
				ca[ i ] = a[ i ] ? CC.N1 : CC.HY;
			}
			return new string( ca );
		}
		//
		//
		// 
		///<summary>Count the number of on or off bits in a.</summary>
		public static int Count( this BitArray a, bool on ) {
			int found = 0; int length = a.Count;
			for ( var i = 0; i < length; i++ ) {
				if ( on == a.Get( i ) ) found++;
			}
			return found;
		}
		//
		///<summary>Convert the bits into an array. </summary>
		public static ushort[] ToArray( this BitArray a, Tri which ) {
			if ( a is null ) return null;
			int count = 0; int length = a.Count;
			switch ( which ) {
				case Tri.On: count = Count( a, true ); break;
				case Tri.Off: count = Count( a, false ); break;
				case Tri.All: count = 64; break;
			}
			const ushort zero = 0;
			int w = 0; bool b; ushort v = 0;
			var ai = new ushort[ count ];
			for ( int i = 0; i < length; i++ ) {
				b = a.Get( i );
				v = (ushort) i;
				switch ( which ) {
					case Tri.On: if ( b ) ai[ w++ ] = v; break;
					case Tri.Off: if ( !b ) ai[ w++ ] = v; break;
					case Tri.All: ai[ w++ ] = b ? v : zero; break;
				}
			}
			return ai;
		}

	}
	//
	//
	//
	//
	///<summary>Encapsulates locking for a Boolean type.</summary>
	public class LockedBool {
		//
		// This lockable object is required for the lock statement.
		private readonly object _lo = new object();
		//
		// The encapsulated Boolean value
		protected bool _value = false;
		//
		///<summary>Get the value, protected by lock.</summary>
		public bool Value() {
			lock ( _lo ) {
				return _value;
			}
		}
		///<summary>Check if the Boolean value is equal to another one, protected by lock..</summary>
		public bool IsEqual( bool other ) {
			lock ( _lo ) {
				return _value == other;
			}
		}
		///<summary>Turn the Boolean value on or off, protected by lock.</summary>
		public void Set( bool value ) {
			lock ( _lo ) {
				_value = value;
			}
		}
	}
	//
	//
	//
	//
	//
	//
	//
	[Flags]
	public enum Marked : uint {
		None = 0x00000000,
		//
		Bold = 0x00000001,
		Italic = 0x00000002,
		Underline = 0x00000004,
		Strike = 0x00000008,
		//
		Super = 0x00000010,
		Sub = 0x00000020,
		Small = 0x00000040,
		Code = 0x00000080,
		//
		Back1 = 0x00000100,
		Back2 = 0x00000200,
		Back3 = 0x00000400,
		Back4 = 0x00000800,
		//
		Fore1 = 0x00001000,
		Fore2 = 0x00002000,
		Fore3 = 0x00004000,
		Fore4 = 0x00008000,
	}
	//
	public static partial class MarkedUtilities {
		//
		// URL-compatible markup
		public static readonly string MarkedOn = "~(";
		public static readonly string MarkedOff = "~)";
		//
		public static string Mark( this string value, Marked marked ) {
			var sa = new string[] {
				MarkedOn,
				UInt2Hex( (uint) marked, true ),
				CS.P2,
				value,
				MarkedOff };
			return String.Join( null, sa );
		}
		//
		//
		private static readonly char[] _HexDigits = new char[] {
		'0','0','0','1','0','2','0','3','0','4','0','5','0','6','0','7','0','8','0','9','0','A','0','B','0','C','0','D','0','E','0','F',
		'1','0','1','1','1','2','1','3','1','4','1','5','1','6','1','7','1','8','1','9','1','A','1','B','1','C','1','D','1','E','1','F',
		'2','0','2','1','2','2','2','3','2','4','2','5','2','6','2','7','2','8','2','9','2','A','2','B','2','C','2','D','2','E','2','F',
		'3','0','3','1','3','2','3','3','3','4','3','5','3','6','3','7','3','8','3','9','3','A','3','B','3','C','3','D','3','E','3','F',
		'4','0','4','1','4','2','4','3','4','4','4','5','4','6','4','7','4','8','4','9','4','A','4','B','4','C','4','D','4','E','4','F',
		'5','0','5','1','5','2','5','3','5','4','5','5','5','6','5','7','5','8','5','9','5','A','5','B','5','C','5','D','5','E','5','F',
		'6','0','6','1','6','2','6','3','6','4','6','5','6','6','6','7','6','8','6','9','6','A','6','B','6','C','6','D','6','E','6','F',
		'7','0','7','1','7','2','7','3','7','4','7','5','7','6','7','7','7','8','7','9','7','A','7','B','7','C','7','D','7','E','7','F',
		'8','0','8','1','8','2','8','3','8','4','8','5','8','6','8','7','8','8','8','9','8','A','8','B','8','C','8','D','8','E','8','F',
		'9','0','9','1','9','2','9','3','9','4','9','5','9','6','9','7','9','8','9','9','9','A','9','B','9','C','9','D','9','E','9','F',
		'A','0','A','1','A','2','A','3','A','4','A','5','A','6','A','7','A','8','A','9','A','A','A','B','A','C','A','D','A','E','A','F',
		'B','0','B','1','B','2','B','3','B','4','B','5','B','6','B','7','B','8','B','9','B','A','B','B','B','C','B','D','B','E','B','F',
		'C','0','C','1','C','2','C','3','C','4','C','5','C','6','C','7','C','8','C','9','C','A','C','B','C','C','C','D','C','E','C','F',
		'D','0','D','1','D','2','D','3','D','4','D','5','D','6','D','7','D','8','D','9','D','A','D','B','D','C','D','D','D','E','D','F',
		'E','0','E','1','E','2','E','3','E','4','E','5','E','6','E','7','E','8','E','9','E','A','E','B','E','C','E','D','E','E','E','F',
		'F','0','F','1','F','2','F','3','F','4','F','5','F','6','F','7','F','8','F','9','F','A','F','B','F','C','F','D','F','E','F','F',
		};
		//
		// https://johnnylee-sde.github.io/Fast-unsigned-integer-to-hex-string/
		public static void UInt2Hex( this char[] target, uint num ) {
			uint pos = 0; char ch = Char.MinValue;
			for ( int i = 3; i >= 0; i-- ) {
				// Get the lowest byte
				pos = 2 * ( num & 0xFF );
				// Get the first character of this position
				ch = _HexDigits[ pos ];
				target[ i * 2 ] = ch;
				// Get the second character of this position
				ch = _HexDigits[ pos + 1 ];
				target[ i * 2 + 1 ] = ch;
				// Shift right 8 bits
				num = num >> 8;
			}
		}
		//
		// https://johnnylee-sde.github.io/Fast-unsigned-integer-to-hex-string/
		public static string UInt2Hex( uint num, bool trim ) {
			var ca = new char[ 8 ];
			UInt2Hex( ca, num );
			var m = new string( ca );
			if ( trim ) {
				m = m.TrimStart( '0' );
				if ( 0 == m.Length ) m = CS.N0;
			}
			return m;
		}
	}
	//
	//
	//
	//
	public static class ToLongArrayEx {
		//
		///<summary>Convert a single string value into a long array.</summary>
		public static long[] ToLongArray( this string value ) {
			if ( value is not null ) {
				if ( Int32.TryParse( value, out int number ) ) {
					return new long[ 1 ] { number };
				}
			}
			return null;
		}
		//
		///<summary>Convert a single long value into a long array.</summary>
		public static long[] ToLongArray( this long value ) {
			return new long[ 1 ] { value };
		}
		//
		///<summary>Convert a single int value into a long array.</summary>
		public static long[] ToLongArray( this int value ) {
			return new long[ 1 ] { value };
		}
	}
	//
	//
	// https://gist.github.com/Vaskivo/ce0d2f39ecbb91367aa7
	public class DuplicateKeyComparer<TKey> : IComparer<TKey> where TKey : IComparable {
		public int Compare( TKey x, TKey y ) {
			int result = x.CompareTo( y );
			if ( result == 0 ) return 1; // Handle equality as beeing greater
			return result;
		}
	}
	//
	//
	public static class DelegateEx {
		//
		public static bool IsMember( this Delegate value ) {
			return value is not null && value.Method is not null && value.Target is not null;
		}
		//
		public static bool IsStatic( this Delegate value ) {
			return value is not null && value.Method is not null && value.Target is null;
		}
		//
		public static bool IsEmpty( this Delegate value ) {
			return value is null || value.Method is null;
		}
		//
		public static bool IsFilled( this Delegate value ) {
			return value is not null && value.Method is not null;
		}
	}
	//
	//
	//
	//
	public static class EnumEx {
		//
		///<summary>Get the lowest value.</summary>
		public static long MinVal( this Type type ) {
			long min = long.MaxValue;
			// Check everything
			if ( Log.Assert( type is not null ) ) {
				var a = Enum.GetValues( type );
				if ( Log.Assert( a is not null ) ) {
					// Loop over all items
					var I = a.Length;
					for ( int i = 0; i < I; i++ ) {
						// Get and convert value into long
						var v = a.GetValue( i );
						if ( Log.Assert( v is not null ) ) {
							var t = v.ToLong();
							// Check for maximum.
							if ( t < min ) min = t;
						}
					}
				}
			}
			return min;
		}
		//
		///<summary>Get the highest value.</summary>
		public static long MaxVal( this Type type ) {
			long max = long.MinValue;
			// Check everything
			if ( Log.Assert( type is not null ) ) {
				var a = Enum.GetValues( type );
				if ( Log.Assert( a is not null ) ) {
					// Loop over all items
					var I = a.Length;
					for ( int i = 0; i < I; i++ ) {
						// Get and convert value into long
						var v = a.GetValue( i );
						if ( Log.Assert( v is not null ) ) {
							var t = v.ToLong();
							// Check for maximum.
							if ( t > max ) max = t;
						}
					}
				}
			}
			return max;
		}
		//
		///<summary>Get all values as long[].</summary>
		public static long[] LongValues( this Type type ) {
			// Check everything
			if ( Log.Assert( type is not null ) ) {
				var a = Enum.GetValues( type );
				if ( Log.Assert( a is not null ) ) {
					// Loop over all items
					var I = a.Length;
					var result = new long[ I ];
					for ( int i = 0; i < I; i++ ) {
						// Get and convert value into long
						var v = a.GetValue( i );
						if ( Log.Assert( v is not null ) ) {
							result[ i ] = v.ToLong();
						}
					}
					return result;
				}
			}
			return null;
		}
		//
		///<summary>Get number of distinct values.</summary>
		public static int DistinctValues( this Type type ) {
			// Check everything
			if ( Log.Assert( type is not null ) ) {
				var a = Enum.GetValues( type );
				if ( Log.Assert( a is not null ) ) {
					return a.Length;
				}
			}
			return 0;
		}
		//
		///<summary>Try to find the numeric enum value from string. Only for integral values!</summary>
		public static int SearchName( this Type type, string name, Array values, string[] names ) {
			if ( name.IsFilled() ) {
				// Loop over all items
				var I = names.Length;
				Log.Assert( I == values.Length );
				for ( int i = 0; i < I; i++ ) {
					var item = names[ i ];
					// Exact match required!
					if ( item == name ) {
						var value = values.GetValue( i );
						if ( value is not null ) {
							return (int) value;
						}
					}
				}
			}
			return -1;
		}
		//
		///<summary>Try to find the numeric enum value from string. Only for integral values!</summary>
		public static int SearchName( this Type type, string name ) {
			if ( type is not null ) {
				if ( name.IsFilled() ) {
					var values = Enum.GetValues( type );
					var names = Enum.GetNames( type );
					// Loop over all items
					var I = names.Length;
					Log.Assert( I == values.Length );
					for ( int i = 0; i < I; i++ ) {
						var item = names[ i ];
						// Exact match required!
						if ( item == name ) {
							var value = values.GetValue( i );
							if ( value is not null ) {
								return (int) value;
							}
						}
					}
				}
			}
			return -1;
		}
		//
		/////<summary>Return separated string of numerical value and string representation.</summary>
		//public static string Format( this Type type, object o, char separator = CC.CL ) {
		//	if ( 0 == v ) return CS.N0;
		//	var sb = new StringBuilder();
		//	return sb.ToString();
		//}
		//
		//
		// IDEA COLLECTION:
		//// UNTESTED, taken from http://stackoverflow.com/questions/276585/enumeration-extension-methods
		//public static bool TryParse<T>( this Enum theEnum, string strType, out T result ) {
		//	string strTypeFixed = strType.Replace( ' ', '_' );
		//	if ( Enum.IsDefined( typeof( T ), strTypeFixed ) ) {
		//		result = (T) Enum.Parse( typeof( T ), strTypeFixed, true );
		//		return true;
		//	} else {
		//		foreach ( string value in Enum.GetNames( typeof( T ) ) ) {
		//			if ( value.Equals( strTypeFixed,
		//				StringComparison.OrdinalIgnoreCase ) ) {
		//				result = (T) Enum.Parse( typeof( T ), value );
		//				return true;
		//			}
		//		}
		//		result = default( T );
		//		return false;
		//	}
		//}
		//public static void ForEach( this Enum enumType, Action<Enum> action ) {
		//	foreach ( var type in Enum.GetValues( enumType.GetType() ) ) {
		//		action( (Enum) type );
		//	}
		//}
		//public static ConvertType Convert<ConvertType>( this Enum e ) {
		//	object o = null;
		//	Type type = typeof( ConvertType );
		//	if ( type == typeof( int ) ) {
		//		o = Convert.ToInt( e );
		//	} else if ( type == typeof( long ) ) {
		//		o = Convert.ToInt64( e );
		//	} else if ( type == typeof( short ) ) {
		//		o = Convert.ToInt16( e );
		//	} else {
		//		o = Convert.ToString( e );
		//	}
		//	return (ConvertType) o;
		//}
		//public static T ConvertByName<T>( this Enum value ) {
		//	return (T) Enum.Parse( typeof( T ), Enum.GetName( value.GetType(), value ) );
		//}
		//public static T ConvertByValue<T>( this Enum value ) {
		//	return (T) ( (dynamic) ( (int) ( (object) value ) ) );
		//}
		//}
	}
}
