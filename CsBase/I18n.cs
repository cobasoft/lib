﻿// License and Copyright: see License.cs
using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
namespace Cobasoft {
	//
	///<summary>Internationalization</summary>
	public static class I18n {
		//
		public static readonly int Language;
		public static readonly string Culture;
		public static readonly string DateFormat;
		public static readonly string DateNone;
		public static readonly NumberStyles MoneyStyle = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.Currency;
		public static readonly string[] BoolFormat;
		public static readonly string AmountFormat2;
		public static readonly string AmountFormat4;
		public static readonly string MoneyFormat2;
		public static readonly string MoneyFormat4;
		public static readonly string MoneyNone;
		public static readonly string QuantityFormat;
		public static readonly string QuantityNone;
		public static readonly string NumberFormat;
		public static readonly string Separator;
		public static readonly string True;
		public static readonly string False;
		public static readonly CultureInfo CultureLocal;
		public static readonly TextInfo TextInfoLocal;
		public static readonly NumberFormatInfo NumberFormatLocal;
		public static readonly CultureInfo CultureIntern;
		public static readonly TextInfo TextInfoIntern;
		public static readonly NumberFormatInfo NumberFormatIntern;
		public static readonly char DecimalSeparator;
		public static readonly long[] Powers;
		//
		//
		static I18n() {
			// Get format information
			Language = Config.GetInt( "I18n_Language", TC.LngGerman );
			Culture = Config.Get( "I18n_Culture", "en-us" );
			DateFormat = Config.Get( "I18n_DateFormat", "dd.MM.yyyy" );
			DateNone = Config.Get( "I18n_DateNone", "-" );
			BoolFormat = Config.GetStrings( "I18n_BoolFormat", "Ja;Nein" );
			AmountFormat2 = Config.Get( "I18n_AmountFormat2", "0.00" );
			AmountFormat4 = Config.Get( "I18n_AmountFormat4", "0.0000" );
			MoneyFormat2 = Config.Get( "I18n_MoneyFormat2", "0.00 €" );
			MoneyFormat4 = Config.Get( "I18n_MoneyFormat4", "0.0000 €" );
			MoneyNone = Config.Get( "I18n_MoneyNone", "k.A." );
			QuantityFormat = Config.Get( "I18n_QuantityFormat", "0.####" );
			QuantityNone = Config.Get( "I18n_QuantityNone", "-" );
			NumberFormat = Config.Get( "I18n_NumberFormat", "-" );
			Separator = Config.Get( "I18n_Separator", "-" );
			True = Config.Get( "I18n_True", "-" );
			False = Config.Get( "I18n_False", "-" );
			// Create local culture info
			CultureLocal = new CultureInfo( Culture );
			if ( Log.Assert( CultureLocal is not null ) ) {
				TextInfoLocal = CultureLocal.TextInfo;
				NumberFormatLocal = CultureLocal.NumberFormat;
				var s = NumberFormatLocal?.NumberDecimalSeparator;
				s = s is null ? "." : s.Trim();
				DecimalSeparator = s.IsFilled() ? s[ 0 ] : '.';
			}
			// Create international culture info
			CultureIntern = CultureInfo.InvariantCulture;
			if ( Log.Assert( CultureIntern is not null ) ) {
				TextInfoIntern = CultureIntern.TextInfo;
				NumberFormatIntern = CultureIntern.NumberFormat;
			}
			// Powers of 10
			Powers = new long[] { 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000 };
		}
		//
		//
		public static string WeekDay( this DayOfWeek value ) {
			// TASK: Translation
			var s = value.ToString();
			s = s.Left( 3 );
			return s;
		}
		//
		//
		// PARSING
		//
		//
		///<summary>Parse monetary value into decimal, using configured culture and appropriate NumberStyles.</summary>
		public static bool ParseMoney( string value, ref decimal result, bool optional = false ) {
			if ( value.IsFilled() ) {
				return decimal.TryParse( value, MoneyStyle, CultureLocal, out result );
			}
			if ( optional ) {
				result = 0;
				return true;
			}
			return false;
		}
		//
		///<summary>Parse monetary value into double, using configured culture and appropriate NumberStyles.</summary>
		public static bool ParseMoney( string value, ref double result, bool optional = false ) {
			if ( value.IsFilled() ) {
				return double.TryParse( value, MoneyStyle, CultureLocal, out result );
			}
			if ( optional ) {
				result = 0;
				return true;
			}
			return false;
		}
		//
		///<summary>Parse a money value, if it's different from previous value.</summary>
		public static bool DeltaParseMoney( string old, string value, ref decimal result ) {
			if ( 0 != old.CompareWith( value ) ) {
				if ( value.IsFilled() ) {
					return decimal.TryParse( value, MoneyStyle, CultureLocal, out result );
				}
			}
			return false;
		}
		//
		///<summary>Fix incoming money string.</summary>
		public static string FixMoney( string value ) {
			if ( value.IsFilled() ) {
				var comma = value.IndexOf( CC.CM );
				var dot = value.IndexOf( CC.DT );
				if ( -1 < comma && -1 < dot ) {
					if ( comma < dot ) {
						value = value.Replace( CS.CM, CS.Empty );
					} else {
						value = value.Replace( CS.DT, CS.Empty );
					}
				}
				if ( -1 < comma ) {
					value = value.Replace( CS.CM, CS.DT );
				}
			}
			return value;
		}
		//
		//
		///<summary>Parse monetary value into decimal, using invariant culture and appropriate NumberStyles.</summary>
		public static bool ParseMoneyIntern( string value, ref decimal result, bool optional = false ) {
			if ( value.IsFilled() ) {
				value = FixMoney( value );
				return decimal.TryParse( value, MoneyStyle, CultureIntern, out result );
			}
			if ( optional ) {
				result = 0;
				return true;
			}
			return false;
		}
		//
		///<summary>Parse monetary value into double, using invariant culture and appropriate NumberStyles.</summary>
		public static bool ParseMoneyIntern( string value, ref double result, bool optional = false ) {
			if ( value.IsFilled() ) {
				value = FixMoney( value );
				return double.TryParse( value, MoneyStyle, CultureIntern, out result );
			}
			if ( optional ) {
				result = 0;
				return true;
			}
			return false;
		}
		//
		//
		// FORMATTING
		//
		//
		public static CultureInfo FromLocale( string locale ) {
			if ( locale.IsFilled() ) {
				locale = locale.ToLowerInvariant();
				// TASK: similar support for other languages
				if ( "de" == locale || locale.StartsWith( "de_" ) ) {
					locale = "de-DE";
				}
				try {
					return new CultureInfo( locale );
				} catch ( Exception ex ) {
					Log.Exception( ex );
				}
			}
			return CultureLocal;
		}
		//
		//
		public static string FormatDate( this DateTime value, string locale = null ) {
			if ( value == DateTime.MinValue ) return DateNone;
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( DateFormat, ci );
			}
			return value.ToString( DateFormat );
		}
		//
		//
		public static string FormatNumber( this decimal value, string locale = null ) {
			if ( value.IsNear( 0m, 0.0001m ) ) return CS.N0;
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( NumberFormat, ci );
			}
			return value.ToString( NumberFormat );
		}
		public static string FormatNumber( this double value, string locale = null ) {
			return ( (decimal) value ).FormatNumber( locale );
		}
		public static string FormatNumber( this long value, string locale = null ) {
			return ( (decimal) value ).FormatNumber( locale );
		}
		//
		//
		public static string FormatPercentage( this decimal value, string locale = null ) {
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( "0.00%", ci );
			}
			return value.ToString( "0.00%" );
		}
		public static string FormatPercentage( this double value, string locale = null ) {
			return ( (decimal) value ).FormatPercentage( locale );
		}
		public static string FormatPercentage( this long value, string locale = null ) {
			return ( (decimal) value ).FormatPercentage( locale );
		}
		public static string FormatPercent( this long value ) {
			return String.Join( CS.SP, value.ToString(), "%" );
		}
		//
		//
		///<summary>Format money values according to configured format and cultural information. By default, this has 2 decimal places and a currency symbol. If value is near MinValue, MoneyNone will be returned.</summary>
		public static string FormatMoney2( this decimal value, string locale = null ) {
			if ( value.IsNear( 0m, 0.0001m ) ) return MoneyNone;
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( MoneyFormat2, ci );
			}
			return value.ToString( MoneyFormat2 );
		}
		///<summary>Format money values according to configured format and cultural information. By default, this has 2 decimal places and a currency symbol. If value is near MinValue, MoneyNone will be returned.</summary>
		public static string FormatMoney2( this double value, string locale = null ) {
			return ( (decimal) value ).FormatMoney2( locale );
		}
		//
		//
		///<summary>Format money values according to configured format and cultural information. By default, this has 4 decimal places and a currency symbol. If value is near MinValue, MoneyNone will be returned.</summary>
		public static string FormatMoney4( this decimal value, string locale = null ) {
			if ( value.IsNear( 0m, 0.0001m ) ) return MoneyNone;
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( MoneyFormat4, ci );
			}
			return value.ToString( MoneyFormat4 );
		}
		///<summary>Format money values according to configured format and cultural information. By default, this has 4 decimal places and a currency symbol. If value is near MinValue, MoneyNone will be returned.</summary>
		public static string FormatMoney4( this double value, string locale = null ) {
			return ( (decimal) value ).FormatMoney4( locale );
		}
		//
		///<summary>Format amount or quantity values according to locale.</summary>
		public static string FormatAmount4( this decimal value, string locale = null ) {
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( AmountFormat4, ci );
			}
			return value.ToString( AmountFormat4 );
		}
		///<summary>Format amount or quantity values according to locale.</summary>
		public static string FormatAmount4( this double value, string locale = null ) {
			return ( (decimal) value ).FormatAmount4( locale );
		}
		//
		///<summary>Format amount or quantity values according to locale.</summary>
		public static string FormatAmount2( this decimal value, string locale = null ) {
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( AmountFormat2, ci );
			}
			return value.ToString( AmountFormat2 );
		}
		///<summary>Format amount or quantity values according to locale.</summary>
		public static string FormatAmount2( this double value, string locale = null ) {
			return ( (decimal) value ).FormatAmount2( locale );
		}
		//
		//
		public static string FormatQuantity( this decimal value, string locale = null ) {
			if ( value.IsNear( 0m, 0.0001m ) ) return QuantityNone;
			var ci = FromLocale( locale );
			if ( ci is not null ) {
				return value.ToString( QuantityFormat, ci );
			}
			return value.ToString();
		}
		public static string FormatQuantity( this double value, string locale = null ) {
			return ( (decimal) value ).FormatQuantity( locale );
		}
		public static string FormatQuantity( this long value, string locale = null ) {
			return ( (decimal) value ).FormatQuantity( locale );
		}
		//
		///<summary>Format boolean values according to configuration.</summary>
		public static string FormatBool( this bool value ) {
			return BoolFormat[ value ? 0 : 1 ];
		}
		//
		//
		//
		//
		//
		//
		///<summary>Format right-aligned integer with fixed number of digits.</summary>
		public static string Prefixed( this long v, int digits, char pad = '0' ) {
			var n = ( v < 0 );
			if ( n ) v = Math.Abs( v );
			var s = v.ToString();
			var l = s.Length;
			int i = l;
			if ( i < digits ) {
				i = digits;
			}
			if ( n ) ++i;
			var ca = new char[ i ];
			--i;
			char c;
			while ( i >= 0 ) {
				if ( n && 0 == i ) c = '-';
				else if ( l > 0 ) c = s[ --l ];
				else c = pad;
				ca[ i-- ] = c;
			}
			return new string( ca );
		}
		//
		//
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		public static string FormatFixed( this long v, int digits ) {
			var n = ( v < 0 );
			if ( n ) v = Math.Abs( v );
			var s = v.ToString();
			var l = s.Length;
			int i = l;
			if ( i < digits + 1 ) {
				i = digits + 1;
			}
			if ( n ) ++i;
			int d = i - digits;
			var ca = new char[ i + 1 ];
			char c;
			while ( i >= 0 ) {
				if ( i == d ) c = DecimalSeparator;
				else if ( n && 0 == i ) c = '-';
				else if ( l > 0 ) c = s[ --l ];
				else c = '0';
				ca[ i-- ] = c;
			}
			return new string( ca );
		}
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string FormatFixed( this int v, int digits ) {
			return FormatFixed( (long) v, digits );
		}
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string FormatFixed( this uint v, int digits ) {
			return FormatFixed( (long) v, digits );
		}
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string FormatFixed( this ulong v, int digits ) {
			return FormatFixed( (long) v, digits );
		}
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		public static string FormatFixed( this double value, int digits ) {
			value = value * Powers[ digits ];
			value = Math.Round( value, 0 );
			return FormatFixed( (long) value, digits );
		}
		///<summary>Format a decimal number with a fixed number of digits (between 1 and 9).</summary>
		public static string FormatFixed( this decimal value, int digits ) {
			value = value * Powers[ digits ];
			value = Math.Round( value, 0 );
			return FormatFixed( (long) value, digits );
		}
		//
		//
		public static string EscapeCSV( string v ) {
			if ( v is not null ) {
				v = v.Replace( CS.Q2, CS.EmptyQuotes2 );
			}
			return v.Quotes2();
		}
		//
		//
		public static string FormatCSV( this object v ) {
			var tc = v.ToTypeCode(); string s = null;
			switch ( tc ) {
				case TypeCode.Object: Log.Fail(); break;
				case TypeCode.Empty: s = CS.Empty; break;
				case TypeCode.DBNull: s = CS.Empty; break;
				case TypeCode.String: s = EscapeCSV( (string) v ); break;
				case TypeCode.Char: s = ( (char) v ).ToString(); ; break;
				case TypeCode.Boolean: s = (bool) v ? True : False; break;
				case TypeCode.DateTime: ( (DateTime) v ).FormatDate(); break;
				case TypeCode.Byte: s = ( (Byte) v ).ToString(); break;
				case TypeCode.SByte: s = ( (SByte) v ).ToString(); break;
				case TypeCode.Int16: s = ( (Int16) v ).ToString(); break;
				case TypeCode.Int32: s = ( (Int32) v ).ToString(); break;
				case TypeCode.Int64: s = ( (Int64) v ).ToString(); break;
				case TypeCode.Single: s = ( (Single) v ).ToString(); break;
				case TypeCode.UInt16: s = ( (UInt16) v ).ToString(); break;
				case TypeCode.UInt32: s = ( (UInt32) v ).ToString(); break;
				case TypeCode.UInt64: s = ( (UInt64) v ).ToString(); break;
				case TypeCode.Decimal: s = ( (decimal) v ).FormatNumber(); break;
				case TypeCode.Double: s = ( (double) v ).FormatNumber(); break;
			}
			return s;
		}
		//
		//
		public static string ToTitleCase( this string value ) {
			if ( value is null ) return null;
			if ( Log.Deny( TextInfoLocal is null ) ) return value;
			return TextInfoLocal.ToTitleCase( TextInfoLocal.ToLower( value ) );
		}
		//
		//
		//
		// https://docs.oracle.com/cd/B10501_01/text.920/a96518/aspell.htm
		private static string[] _Umlauts = new string[] {
			"Å", "Aa" ,
			"å", "aa" ,
			"Æ", "Ae" ,
			"ä", "ae",
			"Ø", "Oe" ,
			"ö", "oe",
			"ø", "oe" ,
			"ü", "ue",
			"ß", "ss",
			"§", "(s)",
			"Ä", "Ae",
			"Ö", "Oe",
			"Ü", "Ue",
			" ", " " ,
			"·", "." ,
			"●", "." ,
		};
		//
		public static string ExpandUmlauts( this string value ) {
			if ( value is null ) return null;
			var sb = new StringBuilder();
			// Loop over all characters
			foreach ( var c in value ) {
				var found = false;
				// Loop over all replacement values
				for ( int i = 0, n = _Umlauts.Length; i < n; i += 2 ) {
					if ( _Umlauts[ i ][ 0 ] == c ) {
						sb.Append( _Umlauts[ 1 + i ] );
						found = true; break;
					}
				}
				if ( !found ) sb.Append( c );
			}
			return sb.ToString();
		}
		//
		//
		public static int IsoWeek( DateTime dateTime ) {
			return ISOWeek.GetWeekOfYear( dateTime );
		}
		//
	}
}
