﻿// License and Copyright: see License.cs
using System;
using System.Text;
using System.Globalization;
//
//
//
namespace Cobasoft {
	//
	//
	/// <summary>
	/// Used by Object( IToJson obj, int[] fields = null ) to serialize objects.
	/// </summary>
	public interface IToJson {
		///<summary>Get JSON representation.</summary>
		JsonBuilder ToJson( JsonBuilder jb, int[] fields = null );
	}
	//
	//
	///<summary>JSON helper functions.</summary>
	public static class JSON {
		//
		///<summary>Escape a plain string according to JSON rules, according to http://json.org/</summary>
		public static string Escape( string text ) {
			if ( text is null ) return null;
			if ( 0 == text.Length ) return text;
			var sb = new StringBuilder( text.Length + 16 );
			string r;
			for ( int i = 0; i < text.Length; i++ ) {
				char c = text[ i ];
				if ( c < CC.SP ) { // non-printables
					uint u = (uint) c;
					r = "\\u" + u.ToString( "x4" );
				} else {
					switch ( c ) {
						case CC.Q1: goto default;
						case CC.Q2: goto case CC.BS;
						case CC.BS: r = CS.BS + c; break;
						case '\b': r = "\\b"; break;
						case '\f': r = "\\f"; break;
						case '\n': r = "\\n"; break;
						case '\r': r = "\\r"; break;
						case '\t': r = "\\t"; break;
						default: r = c.ToString(); break;
					}
				}
				sb.Append( r );
			}
			return sb.ToString();
		}
		//
		//
		//
		private static char UnescapeUnicode( string text, ref int i ) {
			var left = text.Substring( i + 1, 4 );
			i += 5;
			var n = left.ToInt();
			return (char) n;
		}
		//
		///<summary>Unescape a JSON string.</summary>
		public static string Unescape( string text ) {
			if ( text is null ) return null;
			if ( 0 == text.Length ) return text;
			var sb = new StringBuilder( text.Length );
			for ( int i = 0; i < text.Length; i++ ) {
				char c = text[ i ];
				// Escape? Normal characters are just copied.
				if ( CC.BS == c ) {
					// Get and eat next character
					c = text[ ++i ];
					// Decide about conversion
					switch ( c ) {
						case CC.Q2: c = CC.Q2; break;
						case CC.BS: c = CC.BS; break;
						case 'b': c = '\b'; break;
						case 'f': c = '\f'; break;
						case 'n': c = '\n'; break;
						case 'r': c = '\r'; break;
						case 't': c = '\t'; break;
						case 'u': c = UnescapeUnicode( text, ref i ); break;
						default: Log.Fail(); break;
					}
				}
				sb.Append( c );
			}
			return sb.ToString();
		}
		//
		//
		//
		public static string Prepare( string s ) {
			return s.Replace( '^', '"' ).Trim();
		}
		//
		public static string Finish( string s ) {
			return s.Trim().TrimEnd( ',', ' ', '\n', '\r' );
		}
		//
		//
		///<summary>Get an object from a JSON string.</summary>
		public static DateTime ToDateTime( object o ) {
			if ( o is not null ) {
				var t = o.GetType();
				var typeCode = Type.GetTypeCode( t );
				switch ( typeCode ) {
					case TypeCode.DateTime: return (System.DateTime) o;
					case TypeCode.String:
						var s = (string) o;
						var dt = s.ToDateTime();
						// The following code did not work; the hour was 1 off.
						//DateTime dt;
						//s = s.Replace( ".000Z", "Z" );
						//var result = DateTime.TryParseExact( s, CS.u,
						//	System.Globalization.CultureInfo.InvariantCulture,
						//	System.Globalization.DateTimeStyles.AssumeUniversal,
						//	out dt );
						//Log.Assert( result );
						return dt;
					case TypeCode.Object:
						if ( t.FullName == "System.DateTime" ) {
							return (System.DateTime) o;
						}
						Log.Fail( "Nested objects are not supported" );
						break;
				}
			}
			return DateTime.MinValue;
		}
	}
	//
	// TASK: Make generic. Separate file.
	//
	public class BoolStack {
		//
		public static readonly string ClassName = "BoolStack";
		//
		protected bool[] _stack;
		protected int _idx;
		protected int _idxLast;
		//
		///<summary>Constructor</summary>
		public BoolStack( int size ) {
			_stack = new bool[ size ];
			_idx = 0;
			_idxLast = size - 1;
		}
		// 
		public void Push( bool v ) {
			if ( _idx > _idxLast ) Log.Fail( "too many level." );
			else _stack[ _idx++ ] = v;
		}
		// 
		public bool Pop() {
			if ( _idx < 1 ) return Log.Fail( "empty." );
			else return _stack[ --_idx ];
		}
		//
		public int Level { get { return _idx; } }
		//
		public bool this[ int number ] {
			get {
				if ( number < 0 || number > _idxLast ) return Log.Fail( "bad index." );
				else return _stack[ _idx - 1 ];
			}
		}
	}
	//
	//
	public class JsonBuilder {
		//
		public delegate bool NestedHandler( JsonBuilder jb, TypeCode typeCode, string fullName, object o );
		//
		StringBuilder _sb;
		bool _indent;
		bool _boolDigit;
		BoolStack _stack;
		bool _inAssign;
		NestedHandler _nestedHandler;
		//
		public void SetHandler( NestedHandler handler ) { _nestedHandler = handler; }
		//
		//
		///<summary>Constructor</summary>
		public JsonBuilder( bool indent = false, bool boolDigit = true ) {
			_sb = new StringBuilder( 1024 );
			_indent = indent;
			_boolDigit = boolDigit;
			_stack = new BoolStack( 256 );
			_inAssign = false;
		}
		//
		/// <summary>Create a new JsonBuilder</summary>
		/// <param name="indent">When true, the output is indended by level.</param>
		/// <param name="boolDigit">When true, boolean values are written as numbers instead of true/false.</param>
		public static JsonBuilder Create( bool indent = true, bool boolDigit = true ) {
			return new JsonBuilder( indent, boolDigit );
		}
		//
		///<summary>Call this method to get the result.</summary>
		public string Finish() {
			Log.Assert( 0 == _stack.Level, "0 == _stack.Level" );
			Log.Assert( !_inAssign, "!_inAssign" );
			return _sb.ToString();
		}
		//
		public int Length { get { return _sb.Length; } }
		//
		public string Indent { get { return new String( CC.TB, _stack.Level ); } }
		//
		protected bool AssertNotInAssign { get { return Log.Assert( !_inAssign, "!_inAssign" ); } }
		protected bool AssertInAssign { get { return Log.Assert( _inAssign, "_inAssign" ); } }
		//
		public JsonBuilder EndAssign() { _sb.Append( CS.ClSp ); _inAssign = false; return this; }
		//
		///<summary>Call this to begin an object.</summary>
		public JsonBuilder Object() {
			if ( _inAssign ) EndAssign();
			_stack.Push( true );
			_sb.Append( CS.C1 );
			return this;
		}
		//
		///<summary>Call this to begin an array.</summary>
		public JsonBuilder Array() {
			if ( _inAssign ) EndAssign();
			_stack.Push( false );
			_sb.Append( CS.B1 );
			return this;
		}
		//
		protected static char[] _trimEnd = new char[] { CC.CM, CC.CR, CC.LF, CC.TB, CC.CL, CC.SP };
		//
		///<summary>Call this to close an object or array.</summary>
		public JsonBuilder Close() {
			// Trim remaining, or other characters.
			_sb.TrimEnd( _trimEnd );
			// Close the last open curly brace or bracket.
			var b = _stack.Pop();
			if ( _indent ) _sb.Append( CS.CrLf + Indent );
			_sb.Append( b ? CS.C2 : CS.B2 );
			return this;
		}
		//
		///<summary>Serialize an object implementing IToJson. In this case, Close must not be called separately.</summary>
		public JsonBuilder Object( IToJson obj, int[] fields = null ) {
			obj.ToJson( this, fields );
			return this;
		}
		//
		///<summary>Insert a literal.</summary>
		public JsonBuilder Literal( string v ) {
			_sb.Append( v );
			return this;
		}
		//
		///<summary>Insert a comma.</summary>
		public JsonBuilder Comma() {
			return Literal( CS.CM );
		}
		//
		///<summary>Name. Call this to start a name value pair.</summary>
		public JsonBuilder N( string v ) {
			if ( AssertNotInAssign ) {
				if ( v.IsFilled() ) {
					if ( _indent ) _sb.Append( CS.CrLf + Indent );
					_sb.Append( v.Quote2() );
					_inAssign = true;
				}
			}
			return this;
		}
		//
		///<summary>Base method to output any value. The given string is NOT quoted or escaped. Terminates the name-value pair.</summary>
		public JsonBuilder Value( string v ) {
			if ( AssertInAssign ) {
				EndAssign();
				if ( v is null ) _sb.Append( CS.null_ );
				else if ( 0 == v.Length ) _sb.Append( CS.EmptyQuotes2 );
				else _sb.Append( v );
				_sb.Append( CS.CM );
			}
			return this;
		}
		//
		///<summary>Base method to output any value. The given string quoted but not escaped. Terminates the name-value pair.</summary>
		public JsonBuilder Value2( string v ) {
			if ( AssertInAssign ) {
				EndAssign();
				if ( v is null ) _sb.Append( CS.null_ );
				else if ( 0 == v.Length ) _sb.Append( CS.EmptyQuotes2 );
				else _sb.Append( v.Quote2() );
				_sb.Append( CS.CM );
			}
			return this;
		}
		//
		///<summary>Empty String Value.</summary>
		public JsonBuilder Empty() {
			return Value( CS.EmptyQuotes2 );
		}
		//
		///<summary>String Value.</summary>
		public JsonBuilder V( string v ) {
			return Value( v.Json().Quote2() );
		}
		//
		///<summary>String Value, where null is stored as the empty string.</summary>
		public JsonBuilder U( string v ) {
			if ( v is null ) return Empty();
			return V( v );
		}
		//
		///<summary>String Value, where null is stored as the empty string.
		/// Carriage return and line feed are converted into HTML tag break.</summary>
		public JsonBuilder UH( string v ) {
			if ( v is null ) return Empty();
			v = v.Replace( CS.CrLf, CS.htmlBR ).Replace( CS.CR, CS.htmlBR ).Replace( CS.LF, CS.htmlBR );
			return V( v );
		}
		//
		///<summary>Boolean Value.</summary>
		public JsonBuilder V( bool v ) {
			string s;
			if ( _boolDigit ) {
				s = v ? CS.N1 : CS.N0;
			} else {
				s = v ? CS.true_ : CS.false_;
			}
			return Value( s );
		}
		//
		///<summary>Integer Value.</summary>
		public JsonBuilder V( int v ) {
			return Value( v.ToString() );
		}
		///<summary>When v == 0 write the empty string otherwise write a value.</summary>
		public JsonBuilder U( int v ) {
			if ( 0 == v ) return Empty();
			return V( v );
		}
		//
		///<summary>Double Value.</summary>
		public JsonBuilder V( double v ) {
			if ( Double.IsNaN( v ) ) V( 0 ); // Currently JSON allows no other option
			else if ( Double.IsNegativeInfinity( v ) ) Value( "+1e9999" ); // not tested, yet.
			else if ( Double.IsPositiveInfinity( v ) ) Value( "-1e9999" ); // not tested, yet.
			else Value( v.ToString( "G", CultureInfo.InvariantCulture ) );
			return this;
		}
		///<summary>String Value, where null is stored as the empty string.</summary>
		public JsonBuilder U( double v ) {
			if ( v.IsZero() ) return Empty();
			return V( v );
		}
		//
		///<summary>DateTime Value.</summary>
		public JsonBuilder V( DateTime v ) {
			return Value( v.ToUniversal().Quote2() );
		}
		//
		///<summary>DateTimeOffset Value.</summary>
		public JsonBuilder V( DateTimeOffset v ) {
			DateTime d = v.UtcDateTime;
			return Value( d.ToUniversal().Quote2() );
		}
		//
		//  COMPLEX STRUCTURES
		//
		///<summary>Typed Array, ToString called for every element.</summary>
		public JsonBuilder V<T>( T[] v ) {
			if ( _inAssign ) {
				EndAssign();
			}
			Array();
			for ( int i = 0; i < v.Length; i++ ) {
				T x = v[ i ];
				E( v[ i ] );
			}
			Close();
			_sb.Append( CS.CM );
			return this;
		}
		//
		///<summary>Write any type as one array element.</summary>
		public JsonBuilder V( object o ) {
			if ( AssertInAssign ) {
				EndAssign();
				E( o );
			}
			return this;
		}
		//
		///<summary>Array of objects. All supported types are written as elements.</summary>
		public JsonBuilder V( object[] v ) {
			if ( _inAssign ) {
				EndAssign();
			}
			Array();
			for ( int i = 0; i < v.Length; i++ ) {
				object o = v[ i ];
				E( o );
			}
			return Close();
		}
		//
		///<summary>2-D Array of objects. All supported types are written as elements.</summary>
		public JsonBuilder V( object[,] v ) {
			if ( _inAssign ) {
				EndAssign();
			}
			Array();
			var I = v.GetLength( 0 );
			var J = v.GetLength( 1 );
			for ( int i = 0; i < I; i++ ) {
				Array();
				for ( int j = 0; j < J; j++ ) {
					object o = v[ i, j ];
					E( o );
				}
				Close();
				if ( i < I - 1 ) Comma();
			}
			return Close();
		}
		//
		// ARRAY ELEMENTS
		//
		///<summary>Base method to output any value. The given string is not quoted or escaped. Used within arrays.</summary>
		public JsonBuilder Element( string v ) {
			if ( v is null ) _sb.Append( CS.null_ );
			else if ( 0 == v.Length ) _sb.Append( CS.EmptyQuotes2 );
			else _sb.Append( v );
			_sb.Append( CS.CM );
			return this;
		}
		//
		///<summary>null.</summary>
		public JsonBuilder Null() {
			_sb.Append( CS.null_ ).Append( CS.CM );
			return this;
		}
		//
		///<summary>String Value.</summary>
		public JsonBuilder E( string v ) {
			return Element( v.Json().Quote2() );
		}
		//
		///<summary>Boolean Value.</summary>
		public JsonBuilder E( bool v ) {
			string s;
			if ( _boolDigit ) {
				s = v ? CS.N1 : CS.N0;
			} else {
				s = v ? CS.true_ : CS.false_;
			}
			return Element( s );
		}
		//
		///<summary>Integer Value.</summary>
		public JsonBuilder E( int v ) {
			return Element( v.ToString() );
		}
		//
		///<summary>Integer Value.</summary>
		public JsonBuilder E( uint v ) {
			return Element( v.ToString() );
		}
		//
		///<summary>Integer Value.</summary>
		public JsonBuilder E( long v ) {
			return Element( v.ToString() );
		}
		//
		///<summary>Integer Value.</summary>
		public JsonBuilder E( ulong v ) {
			return Element( v.ToString() );
		}
		//
		///<summary>Double Value.</summary>
		public JsonBuilder E( double v ) {
			if ( Double.IsNaN( v ) ) E( 0 ); // Currently JSON allows no other option
			else if ( Double.IsNegativeInfinity( v ) ) Element( "+1e9999" ); // not tested, yet.
			else if ( Double.IsPositiveInfinity( v ) ) Element( "-1e9999" ); // not tested, yet.
			else Element( v.ToString( "G", CultureInfo.InvariantCulture ) );
			return this;
		}
		//
		///<summary>Decimal Value.</summary>
		public JsonBuilder E( decimal v ) {
			return Element( v.ToString( "G", CultureInfo.InvariantCulture ) );
		}
		//
		///<summary>DateTime Value.</summary>
		public JsonBuilder E( DateTime v ) {
			return Element( v.ToUniversal().Quote2() );
		}
		//
		public JsonBuilder E( Guid v ) {
			return Element( v.ToString().Quote2() );
		}
		//
		///<summary>Write any type as one array element.</summary>
		public JsonBuilder E( object o ) {
			if ( o is null ) {
				Element( null );
			} else if ( o is IToJson ) {
				var tj = o as IToJson;
				tj.ToJson( this );
				_sb.Append( CS.CM );
			} else {
				var t = o.GetType();
				if ( t.IsEnum ) {
					t = Enum.GetUnderlyingType( t );
				}
				var typeCode = Type.GetTypeCode( t );
				switch ( typeCode ) {
					case TypeCode.Boolean: E( (System.Boolean) o ); break;
					case TypeCode.Byte: E( (System.Byte) o ); break;
					case TypeCode.Char: E( (System.Char) o ); break;
					case TypeCode.DBNull: E( null ); break;
					case TypeCode.DateTime: E( (System.DateTime) o ); break;
					case TypeCode.Decimal: E( (System.Decimal) o ); break;
					case TypeCode.Double: E( (System.Double) o ); break;
					case TypeCode.Empty: E( null ); break;
					case TypeCode.Int16: E( (System.Int16) o ); break;
					case TypeCode.Int32: E( (System.Int32) o ); break;
					case TypeCode.Int64: E( (System.Int64) o ); break;
					case TypeCode.SByte: E( (System.SByte) o ); break;
					case TypeCode.Single: E( (System.Single) o ); break;
					case TypeCode.String: E( (System.String) o ); break;
					case TypeCode.UInt16: E( (System.UInt16) o ); break;
					case TypeCode.UInt32: E( (System.UInt32) o ); break;
					case TypeCode.UInt64: E( (System.UInt64) o ); break;
					default: Log.Fail( "Unknown type:" + t.Name ); E( o.ToString() ); break;
					case TypeCode.Object:
						var fullName = t.FullName;
						if ( fullName == "System.Guid" ) {
							var guid = (Guid) o;
							E( guid ); // null or guid
						} else {
							if ( _nestedHandler is not null ) {
								if ( !_nestedHandler( this, typeCode, fullName, o ) ) {
									Log.VerboseV( "Nested object handler failed for", "fullName", fullName );
									E( "FAIL" );
								}
							} else {
								Log.Fail( "Nested objects are not supported" );
								E( "FAIL" );
							}
						}
						break;
				}
			}
			return this;
		}
	}
}
