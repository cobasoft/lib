﻿using System;
using System.Collections.Generic;
//
namespace Cobasoft {
	//
	///<summary>Wrapper class for a key to key-array dictionary.</summary>
	public class KeyMap<T> : IDisposable {
		//
		// Internal dictionary
		private Dictionary<T, HashSet<T>> _dictionary;
		//
		// Constructor
		public KeyMap( int capacity = 64 ) {
			_dictionary = new Dictionary<T, HashSet<T>>( capacity );
		}
		// Destructor, not necessary, but enables 'using'.
		public void Dispose() {
			GC.SuppressFinalize( this );
		}
		//
		///<summary>Add item on primary key.</summary>
		public bool Add( T key, T value ) {
			HashSet<T> map = null;
			if ( _dictionary.ContainsKey( key ) ) {
				map = _dictionary[ key ];
			} else {
				_dictionary[ key ] = map = new HashSet<T>();
			}
			return map.Add( value );
		}
		//
		///<summary>Get all items from primary key.</summary>
		public T[] Get( T key ) {
			if ( _dictionary.ContainsKey( key ) ) {
				var map = _dictionary[ key ];
				if ( map is not null ) {
					var array = new T[ map.Count ];
					map.CopyTo( array );
					return array;
				}
			}
			return null;
		}
		//
		///<summary>Check if primary key exists.</summary>
		public bool Contains( T key ) {
			return _dictionary.ContainsKey( key );
		}
		//
		///<summary>Get primary key count.</summary>
		public int Count( T key ) {
			return _dictionary.Count;
		}
		//
		///<summary>Check if primary and secondary key exist.</summary>
		public bool Contains( T key, T value ) {
			if ( _dictionary.ContainsKey( key ) ) {
				var map = _dictionary[ key ];
				if ( map is not null ) {
					return map.Contains( value );
				}
			}
			return false;
		}
	}
}