﻿#if NEW_TRANSLATION
//
namespace Cobasoft {
	//
	///<summary>Language description for list languages.</summary>
	public class Language {
		//
		///<summary>This also is a TranslationID: reserved range: 100-999 (int)</summary>
		public int LanguageID;
		//
		///<summary>ISO 639-1. https://iso639-3.sil.org/code_tables/639/data (string)</summary>
		public string Code2;
		//
		///<summary>ISO 639-3. https://www.loc.gov/standards/iso639-2/php/code_list.php (string)</summary>
		public string Code3;
		//
		///<summary>Must follow rules for C# identifiers; alphanum, no underscores (string)</summary>
		public string Name;
		//
		///<summary>General comments regarding this language.</summary>
		public string Comment;
		//
		public Language( int languageid, string code2, string code3, string name, string comment ) {
			LanguageID = languageid;
			Code2 = code2;
			Code3 = code3;
			Name = name;
			Comment = comment;
		}
	}
}
#endif