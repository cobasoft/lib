﻿#if NEW_TRANSLATION
// License and Copyright: see License.cs
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
//
namespace Cobasoft {
	//
	//
	///<summary>Global language lookup.</summary>
	public static class Languages {
		//
		private static readonly object _Lock = new object();
		//
		private static Dictionary<int, Language> _dictionary = null;
		//
		///<summary>Initialize from other dictionary.</summary>
		public static void Initialize( Dictionary<int, Language> dictionary ) {
			if ( Log.Assert( dictionary.IsFilled() ) ) {
				lock ( _Lock ) {
					_dictionary = dictionary;
				}
			}
		}
		//
		///<summary>Initialize from array.</summary>
		public static void Initialize( Language[] languages ) {
			if ( Log.Assert( languages.IsFilled() ) ) {
				_dictionary = new Dictionary<int, Language>();
				var I = languages.Length;
				for ( int i = 0; i < I; i++ ) {
					var language = languages[ i ];
					_dictionary.Add( language.LanguageID, language );
				}
			}
		}
		//
		///<summary>Initialize from JSON file.</summary>
		static void InitializeFromFile( string fname = "languages.json" ) {
			try {
				var path = Log.AppBase;
				var fspec = Path.Combine( path, fname );
				var json = File.ReadAllText( fspec );
				if ( Log.Assert( json.IsFilled() ) ) {
					var array = JsonSerializer.Deserialize<Language[]>( json );
					if ( array is not null ) {
						Initialize( array );
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
		}
		//
		///<summary>Check if this singleton is initialized.</summary>
		public static bool IsInitialized() {
			return _dictionary is not null;
		}
		//
		//
		///<summary>Check if this singleton is initialized. If not: Initialize!</summary>
		static void CheckInitialized() {
			if ( !IsInitialized() ) {
				InitializeFromFile();
			}
		}
		//
		///<summary>Try to get the language description.</summary>
		public static Language Get( int languageID ) {
			CheckInitialized();
			if ( _dictionary.TryGetValue( languageID, out Language language ) ) {
				return language;
			}
			return null;
		}
	}
}
#endif
