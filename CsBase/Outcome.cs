﻿// License and Copyright: see License.cs
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
//
//
namespace Cobasoft {
	//
	//
	///<summary>Indicates the outcome of an operation.</summary>
	// The order of these items is important and must not be changed.
	public enum OutcomeCode {
		Unknown = 0,
		Success,
		Warning,
		Error,
		///<summary>The first Data element must be the Exception object.</summary>
		Exception,
		Fatal
	}
	///<summary>The outcome code for a single operation.</summary>
	[DebuggerStepThrough]
	public class Outcome {
		//
		public static readonly string ClassName = "Outcome";
		//
		// Members
		///<summary>The outcome code for a single operation.</summary>
		public OutcomeCode Code;
		///<summary>Contains the message for this operation, if there is no MessageID.</summary>
		public string Message;
		///<summary>If not 0, then this contains a TranslationID.</summary>
		public long MessageID;
		///<summary>Optional numeric value. May be used to transport additional information.</summary>
		public long Value;
		///<summary>Optional data. May be used as parameter for Message.</summary>
		public object[] Data;
		///<summary>After translation, this contains the translated outcomes code, like 'Error' or 'Fehler'.</summary>
		public string TranslatedCode;
		///<summary>After translation, this contains the translated outcomes Message. If there is no translation, it contains the original message.</summary>
		public string TranslatedMessage;
		//
		// Constructors
		///<summary>Constructor</summary>
		public Outcome() {
			Code = OutcomeCode.Unknown;
			Message = null;
			MessageID = 0;
			LogMe();
		}
		///<summary>Constructor</summary>
		public Outcome( OutcomeCode code, string message, long value, params object[] data ) {
			Code = code;
			Message = message;
			MessageID = 0;
			Value = value;
			Data = data;
			LogMe();
		}
		///<summary>Constructor</summary>
		public Outcome( OutcomeCode code, long messageID, long value, params object[] data ) {
			Code = code;
			Message = null;
			MessageID = messageID;
			Value = value;
			Data = data;
			LogMe();
		}
		///<summary>Constructor</summary>
		public Outcome( Exception ex, [CallerMemberName] string member = null ) {
			Code = OutcomeCode.Exception;
			Message = null;
			MessageID = TC.CorException;
			Value = ex.HResult;
			Data = new object[] { ex, member };
			LogMe();
		}
		//
		///<summary>Produces output in this form: "X: [ID:]Message (Value) [data...]"</summary>
		public override string ToString() {
			var sb = new StringBuilder( 500 );
			// Code
			switch ( Code ) {
				case OutcomeCode.Unknown: sb.Append( CS.U ); break;
				case OutcomeCode.Success: sb.Append( CS.S ); break;
				case OutcomeCode.Warning: sb.Append( CS.W ); break;
				case OutcomeCode.Error: sb.Append( CS.E ); break;
				case OutcomeCode.Exception: sb.Append( CS.X ); break;
				case OutcomeCode.Fatal: sb.Append( CS.F ); break;
			}
			sb.Append( CS.CL );
			// MessageID
			var m = Message;
			if ( 0 != MessageID ) {
				m = TL.Get( (int) MessageID, TC.LngEnglish );
				// The MessageID is shown even if the message was not found.
				sb.Append( MessageID.ToString() ).Append( CS.CL );
			}
			// Message can be overwritten by the string from that MessageID.
			if ( m.IsFilled() ) sb.Append( m );
			// Value
			sb.Append( CS.SP ).Append( CS.P1 ).Append( Value.ToString() ).Append( CS.P2 );
			// Data
			if ( Data is not null ) {
				sb.Append( CS.SP );
				var l = Data.Length;
				for ( int i = 0; i < l; i++ ) {
					sb.Append( i.ToString() ).Append( CS.CL );
					var o = Data[ i ];
					if ( o is not null ) {
						sb.Append( o.ToString() ).Append( CS.SP );
					}
				}
			}
			return sb.ToString();
		}
		//
		public static Log.Type GetLogType( OutcomeCode code ) {
			switch ( code ) {
				case OutcomeCode.Unknown: return Log.Type.Data;
				case OutcomeCode.Success: return Log.Type.Success;
				case OutcomeCode.Warning: return Log.Type.Warning;
				case OutcomeCode.Error: return Log.Type.Error;
				case OutcomeCode.Exception: return Log.Type.Exception;
				case OutcomeCode.Fatal: return Log.Type.Exception;
				default: return Log.Type.Data;
			}
		}
		//
		//
		///<summary>Check if this outcome is empty.</summary>
		public bool IsEmpty() {
			return Message.IsEmpty()
				&& 0 == MessageID
				&& TranslatedMessage.IsEmpty()
				&& TranslatedCode.IsEmpty();
		}
		//
		public void LogMe() {
			Log.Write( GetLogType( Code ), "Outcome", ToString() );
		}
		//
		//
		//
		public virtual JsonBuilder ToJson( JsonBuilder jb ) {
			jb.Object()
				.N( "code" ).V( (int) Code )
				.N( "message" ).V( Message )
				.N( "messageID" ).V( MessageID )
				.N( "value" ).V( Value );
			if ( TranslatedCode is not null ) {
				jb.N( "translatedCode" ).V( TranslatedCode );
				jb.N( "translatedMessage" ).V( TranslatedMessage );
			}
			if ( Data.IsFilled() ) {
				jb.N( "Data" ).Array();
				for ( int i = 0; i < Data.Length; i++ ) {
					if ( Data[ i ] is null ) {
						jb.Null();
					} else {
						jb.E( Data[ i ].ToString() );
					}
				}
				jb.Close();
			}
			jb.Close();
			return jb;
		}
		//
		//
		private static Regex _rxBR = new Regex( "<BR[ />]+", Base.RxOptions );
		//
		///<summary>Replace BR tags by CRLF.</summary>
		public void ReplaceBR() {
			if ( Message.IsFilled() ) {
				if ( _rxBR.IsMatch( Message ) ) {
					Message = _rxBR.Replace( Message, CS.CrLf );
				}
			}
		}
	}
	//
	//
	//
	//
	//
	///<summary>Outcomes collection.</summary>
	[DebuggerStepThrough]
	[Serializable]
	public class Outcomes {
		//
		public static readonly string ClassName = "Outcomes";
		//
		private List<Outcome> _list;
		//
		//
		//
		public int Count { get { return _list.Count; } }
		//
		public bool IsEmpty { get { return 0 == _list.Count; } }
		//
		public Outcome First() {
			if ( 0 < Count ) {
				return _list[ 0 ];
			}
			return null;
		}
		//
		//
		//
		///<summary>Constructor</summary>
		public Outcomes() {
			_list = new List<Outcome>();
		}
		//
		///<summary>Constructor with initial value</summary>
		public Outcomes( Outcome outcome ) {
			_list = new List<Outcome> {
				outcome
			};
		}
		//
		//
		///<summary>Constructor with initial value</summary>
		public void AddRange( Outcomes outcomes ) {
			_list.AddRange( outcomes._list );
		}
		//
		//
		///<summary>Constructor with initial value</summary>
		public void Add( Outcome outcome ) {
			if ( !outcome.IsEmpty() ) {
				_list.Add( outcome );
			}
		}
		//
		//
		///<summary>Add a single outcome info.</summary>
		public void Add( OutcomeCode code, string message, long value, params object[] data ) {
			Add( new Outcome( code, message, value, data ) );
		}
		///<summary>Add a single outcome info.</summary>
		public void Add( OutcomeCode code, long messageID, long value, params object[] data ) {
			Add( new Outcome( code, messageID, value, data ) );
		}
		///<summary>Add an exception.</summary>
		public void Add( Exception ex ) {
			Add( new Outcome( OutcomeCode.Exception, TC.CorException, ex.HResult, ex ) );
		}
		//
		///<summary>Add a Success outcome info.</summary>
		public void AddSuccess( long messageID, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Success, messageID, value, data ) );
		}
		///<summary>Add a Warning outcome info.</summary>
		public void AddWarning( long messageID, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Warning, messageID, value, data ) );
		}
		///<summary>Add a Error outcome info.</summary>
		public void AddError( long messageID, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Error, messageID, value, data ) );
		}
		///<summary>Add a Exception outcome info.</summary>
		public void AddException( Exception ex, long value = 0 ) {
			Add( new Outcome( OutcomeCode.Exception, ex.Message, value, ex ) );
		}
#if true
		//
		///<summary>Add a Success outcome info.</summary>
		public void AddSuccess( string message, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Success, message, value, data ) );
		}
		///<summary>Add a Warning outcome info.</summary>
		public void AddWarning( string message, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Warning, message, value, data ) );
		}
		///<summary>Add a Error outcome info.</summary>
		public void AddError( string message, long value, params object[] data ) {
			Add( new Outcome( OutcomeCode.Error, message, value, data ) );
		}
#endif
		//
		//
		///<summary>Count all items with this outcome code.</summary>
		public int CountCode( OutcomeCode code ) {
			if ( Count == 0 ) return 0;
			int n = 0;
			foreach ( var item in _list ) {
				if ( code == item.Code ) n++;
			}
			return n;
		}
		///<summary>Count all items with this outcome code or a higher one.</summary>
		public int CountIncluding( OutcomeCode code ) {
			if ( Count == 0 ) return 0;
			int n = 0;
			foreach ( var item in _list ) {
				if ( code <= item.Code ) n++;
			}
			return n;
		}
		///<summary>Test if all items have exactly this outcome code. If there are no items, the result is false.</summary>
		public bool All( OutcomeCode code ) {
			return CountCode( code ) == Count;
		}
		///<summary>Test if any item have this outcome code or a higher one. If there are no items, the result is false.</summary>
		public bool Any( OutcomeCode code ) {
			return CountIncluding( code ) > 0;
		}
		// ALL
		///<summary>Return true if there are no items or if all items have the outcome code Success.</summary>
		public bool Success() { return 0 == Count || All( OutcomeCode.Success ); }
		// ANY
		///<summary>Test if any items have the outcome code Warning or higher.</summary>
		public bool AnyWarning() { return Any( OutcomeCode.Warning ); }
		///<summary>Test if any items have the outcome code Error or higher.</summary>
		public bool AnyError() { return Any( OutcomeCode.Error ); }
		///<summary>Test if any items have the outcome code Exception or higher.</summary>
		public bool AnyException() { return Any( OutcomeCode.Exception ); }
		///<summary>Test if any items have the outcome code Fatal.</summary>
		public bool AnyFatal() { return Any( OutcomeCode.Fatal ); }
		//
		// UTITLITY METHODS
		//
		///<summary>Join all messages into a string, using the separator. Otherwise return null.</summary>
		public string Join( string separator ) {
			if ( 0 == Count ) return null;
			var translated = TL.Format( this, I18n.Language );
			var sb = new StringBuilder( 500 );
			foreach ( var item in translated._list ) {
				sb.Append( item.TranslatedMessage ).Append( separator );
			}
			return sb.ToString();
		}
		//
		///<summary>Show statistics, for debugging.</summary>
		public override string ToString() {
			var sb = new StringBuilder( 500 );
			var success = CountCode( OutcomeCode.Success );
			var warning = CountCode( OutcomeCode.Warning );
			var error = CountCode( OutcomeCode.Error );
			var exception = CountCode( OutcomeCode.Exception );
			var fatal = CountCode( OutcomeCode.Fatal );
			sb.Append( ClassName ).Append( CS.SP )
				.Append( CS.B1 ).Append( Count.ToString() ).Append( CS.B2 )
				.Append( " S=" ).Append( success.ToString() )
				.Append( " W=" ).Append( warning.ToString() )
				.Append( " E=" ).Append( error.ToString() )
				.Append( " X=" ).Append( exception.ToString() )
				.Append( " F=" ).Append( fatal.ToString() )
				.Append( CS.SP );
			foreach ( var item in _list ) {
				sb.Append( item.ToString() ).Append( CS.SP );
			}
			return sb.ToString();
		}
		///<summary>Make the list of outcomes unique.</summary>
		public void UniqueMessages() {
			var sd = new Dictionary<string, Outcome>();
			foreach ( var item in _list ) {
				if ( !sd.ContainsKey( item.Message ) ) {
					sd.Add( item.Message, item );
				}
			}
			_list.Clear();
			_list.AddRange( sd.Values );
		}
		//
		//
		public JsonBuilder ToJson( JsonBuilder jb ) {
			jb.N( Outcomes.ClassName ).Array();
			foreach ( var item in _list ) {
				item.ToJson( jb );
				jb.Comma();
			}
			jb.Close();
			return jb;
		}
		//
		public void ToLog( [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Log.VerboseV( member, "OUTCOMES", file, line.ToString() );
			foreach ( var outcome in _list ) {
				outcome.LogMe();
			}
		}
		//
		///<summary>Replace BR tags by CRLF.</summary>
		public void ReplaceBR() {
			foreach ( var outcome in _list ) {
				outcome.ReplaceBR();
			}
		}
		//
		///<summary>Formats and translates Outcomes.
		/// If Data is present and the translated string contains placeholders (like "{0}"), 
		/// then the data will be formatted into the placeholders. 
		/// If Data contains Translate objects, these will be translated before being used.
		///</summary>
		public void Format( int lngID, TL.TranslateID translateID ) {
			int c = 0;
			foreach ( var o in _list ) {
				// Get readable, translated code
				switch ( o.Code ) {
					case OutcomeCode.Unknown: c = TC.CorUnknown; break;
					case OutcomeCode.Success: c = TC.CorSuccess; break;
					case OutcomeCode.Warning: c = TC.CorWarning; break;
					case OutcomeCode.Error: c = TC.CorError; break;
					case OutcomeCode.Exception: c = TC.CorException; break;
					case OutcomeCode.Fatal: c = TC.CorFatal; break;
				}
				o.ReplaceBR();
				// Get the translated outcome texts
				o.TranslatedCode = translateID( c, lngID );
				o.TranslatedMessage = translateID( (int) o.MessageID, lngID );
				if ( o.TranslatedMessage is null ) {
					if ( o.Message is not null ) {
						o.TranslatedMessage = o.Message;
					}
				} else {
					if ( o.Message is null ) {
						o.Message = o.TranslatedMessage;
					}
				}
				// Format the data
				var data = o.Data;
				if ( data is not null && 0 < data.Length ) {
					// Check if the data contains Translate objects.
					for ( int i = 0; i < data.Length; i++ ) {
						var v = data[ i ];
						if ( v is not null && v is Translate ) {
							// Replace the Translate object with the translated string.
							data[ i ] = ( (Translate) v ).Get( lngID );
						}
					}
					// Formatting
					if ( o.TranslatedMessage is not null ) {
						if ( o.TranslatedMessage.Contains( '{' ) ) {
							o.TranslatedMessage = String.Format( o.TranslatedMessage, data );
						}
					}
				}
			}
		}
		//
		//
		//
		///<summary>General purpose delegate, which uses Outcomes</summary>
		public delegate bool Task( Outcomes outcomes, params object[] args );
		//
	}
	//
	//
	public static class OutcomesEx {
		//
		public static JsonBuilder Add( this JsonBuilder jb, Outcomes outcomes ) {
			return outcomes.ToJson( jb );
		}
	}
	//
	//
	//
	//
	//
	//
	//
	///<summary>Combination of Outcomes with optional user defined data.</summary>
	[DebuggerStepThrough]
	[Serializable]
	public class Results {
		//
		public static readonly string ClassName = "Results";
		//
		///<summary>Outcomes.</summary>
		public Outcomes Outcomes;
		//
		///<summary>String for storing custom data.</summary>
		public string Info;
		//
		///<summary>Another string for storing custom data.</summary>
		public string Note;
		//
		///<summary>Object for storing custom data.</summary>
		public object Data;
		//
		///<summary>Constructor</summary>
		public Results() {
			Outcomes = new Outcomes();
		}
		//
		///<summary>Translation (of Outcomes only).</summary>
		public Results Translate( int lngID ) {
			TL.Format( Outcomes, lngID );
			return this;
		}

	}
	//
}
