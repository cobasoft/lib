﻿// License and Copyright: see License.cs
using System;
using System.Collections;
using System.IO;
//
namespace Cobasoft {
	//
	public enum FileSystemFields {
		Extension,
		CreationTime,
		LastWriteTime,
		LastAccessTime,
		FullName,
		Name,
	}
	//
	public class RecursiveFileAction {
		//
		///<summary>Return false to break the loop.</summary>
		public delegate bool FileActionDelegate( Outcomes outcomes, object data, int level, FileInfo fi );
		public delegate bool FolderActionDelegate( Outcomes outcomes, object data, int level, DirectoryInfo di );
		//
		///<summary>Limit the depth of the recursion. 0 == root only.</summary>
		public int Depth;
		//
		///<summary>Sort direction.</summary>
		public Tri Order;
		//
		///<summary>Sort field.</summary>
		public FileSystemFields Field;
		//
		///<summary>Optional delay time in milliseconds.</summary>
		public int Delay = 0;
		private TimeSpan _delaySpan;
		//
		///<summary>Wildcard.</summary>
		public string Wildcard = "*.*";
		//
		///<summary>This delegate is called for every file. It must be set before calling 'Run'.</summary>
		public FileActionDelegate FileAction;
		//
		///<summary>This delegate is called for every folder. It must be set before calling 'Run'.</summary>
		public FolderActionDelegate FolderAction;
		//
		///<summary>This data is passed to every call to the delegates. It must be set before calling 'Run'.</summary>
		public object Data;
		//
		///<summary>Contains messages of all exceptions and errors.</summary>
		public Outcomes Outcomes = new Outcomes();
		//
		//
		//
		//
		///<summary>Sort file information according to Order and Field.</summary>
		public void Sort( FileSystemInfo[] files ) {
			if ( Log.Assert( files != null ) ) {
				Array.Sort( files, new FileSystemInfoComparer( Order, Field ) );
			}
		}
		///<summary>Sort file information according to Order and Field.</summary>
		public void Sort( FileInfo[] files ) {
			Sort( (FileSystemInfo[]) files );
		}
		///<summary>Sort folder information according to Order and Field.</summary>
		public void Sort( DirectoryInfo[] dirs ) {
			Sort( (FileSystemInfo[]) dirs );
		}
		//
		// Check if the last file write time is below the threshold
		private bool CheckAge( FileInfo fi ) {
			if ( 0 == Delay ) return true;
			var written = fi.LastWriteTimeUtc;
			var threshold = written + _delaySpan;
			var now = DateTime.UtcNow;
			return threshold < now;
		}
		//
		protected bool Recurse( DirectoryInfo root, int level = 0 ) {
			try {
				// Process the files in this folder
				if ( Wildcard.IsEmpty() ) Wildcard = "*.*";
				var exts = Wildcard.Split( CA.SC, StringSplitOptions.RemoveEmptyEntries );
				if ( exts.IsFilled() ) {
					foreach ( var ext in exts ) {
						// This is not reliable! Even with Refresh it will deliver folders, which do not exist anymore.
						root.Refresh();
						if ( root.Exists ) {
							FileInfo[] files = null;
							try {
								files = root.GetFiles( ext );
							} catch ( Exception ex ) {
								Log.SmallException( ex );
							}
							if ( files is not null && 0 < files.Length ) {
								Sort( files );
								for ( int i = 0; i < files.Length; i++ ) {
									var fi = files[ i ];
									if ( CheckAge( fi ) ) {
										bool b = FileAction is null || FileAction( Outcomes, Data, level, fi );
										if ( !b ) return false;
									}
								}
							}
						}
					}
				}
				// Optionally limit recursion
				if ( Depth < 1 || level + 1 < Depth ) {
					// Process the directories in this folder
					// This is not reliable! Even with Refresh it will deliver folders, which do not exist anymore.
					root.Refresh();
					if ( root.Exists ) {
						DirectoryInfo[] dirs = null;
						try {
							dirs = root.GetDirectories();
						} catch ( Exception ex ) {
							Log.SmallException( ex );
						}
						if ( dirs != null && 0 < dirs.Length ) {
							Sort( dirs );
							for ( int i = 0; i < dirs.Length; i++ ) {
								var di = dirs[ i ];
								var b = FolderAction is null || FolderAction( Outcomes, Data, level + 1, di );
								if ( b ) b = Recurse( di, level + 1 );
								if ( !b ) return false; // stop
							}
						}
					}
				}
				return true;
			} catch ( Exception ex ) {
				Outcomes.Add( ex );
			}
			return false;
		}
		//
		///<summary>Run the recursion, starting with path.</summary>
		public bool Run( string path ) {
			try {
				if ( Log.Assert( path.IsFilled() ) ) {
					if ( FileAction is not null || FolderAction is not null ) {
						var root = new DirectoryInfo( path );
						if ( root is not null && root.Exists ) {
							_delaySpan = new TimeSpan( 0, 0, 0, 0, Delay );
							//
							var b = FolderAction is null || FolderAction( Outcomes, Data, 0, root );
							if ( b ) Recurse( root );
							return true;
						}
					}
				} else {
					Outcomes.Add( OutcomeCode.Error, TC.CorNoActionWasSet, 0 );
				}
			} catch ( Exception ex ) {
				Outcomes.Add( ex );
			}
			return false;
		}
		//
	}
	//
	//
	//
	//
	public class FileSystemInfoComparer : IComparer {
		//
		///<summary>Sort direction.</summary>
		public Tri Order;
		//
		///<summary>Sort field.</summary>
		public FileSystemFields Field;
		//
		//
		public FileSystemInfoComparer( Tri order, FileSystemFields field ) {
			Order = order;
			Field = field;
		}
		//
		public int Compare( Object x, Object y ) {
			var a = x as FileSystemInfo;
			var b = y as FileSystemInfo;
			int d = 0;
			switch ( Field ) {
				case FileSystemFields.Extension:
					d = a.Extension.CompareWith( b.Extension );
					break;
				case FileSystemFields.CreationTime:
					d = DateTime.Compare( a.CreationTimeUtc, b.CreationTimeUtc );
					break;
				case FileSystemFields.LastWriteTime:
					d = DateTime.Compare( a.LastWriteTimeUtc, b.LastWriteTimeUtc );
					break;
				case FileSystemFields.LastAccessTime:
					d = DateTime.Compare( a.LastAccessTimeUtc, b.LastAccessTimeUtc );
					break;
				case FileSystemFields.FullName:
					d = a.FullName.CompareWith( b.FullName );
					break;
				case FileSystemFields.Name:
					d = a.Name.CompareWith( b.Name );
					break;
				default: Log.Fail(); break;
			}
			if ( Tri.Descending == Order ) d *= -1;
			return d;
		}
		//
	}
}
