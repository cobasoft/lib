using System;
using System.Collections.Specialized;
//
namespace Cobasoft {
	//
	public class StringTemplate {
		//
		private StringDictionary _template = null;
		private string _path = null;
		private bool _good = false;
		//
		public bool Found => _template is not null;
		public bool Good => _good;
		public int Count => _template.Count;
		public string Path => _path;
		//
		///<summary>Load a template file and split it into parts.</summary>
		private void Initialize( string templatePath ) {
			try {
				var template = Util.SplitFile( templatePath );
				if ( template is not null && template.Count > 0 ) {
					_template = template;
					_path = templatePath;
					_good = true;
				} else {
					Log.WarningV( "Template", "missing or empty", templatePath );
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
		}
		//
		///<summary>Initialize template dictionary from file.</summary>
		public StringTemplate( string templatePath ) {
			Initialize( templatePath );
		}
		//
		///<summary>Get and check a template part.</summary>
		public string Get( string name ) {
			if ( !_good ) {
				Log.WarningV( Log.CM(), "template is bad", _path );
			} else {
				if ( _template is null ) {
					Log.WarningV( Log.CM(), "no template loaded", _path );
				} else {
					if ( name.IsEmpty() ) {
						Log.WarningV( Log.CM(), "name is empty", _path );
					} else {
						if ( !_template.ContainsKey( name ) ) {
							Log.WarningV( Log.CM(), "name not found", name, "_path", _path );
						} else {
							var part = _template[ name ];
							if ( part.IsEmpty() ) {
								Log.WarningV( Log.CM(), "part is empty", name, "_path", _path );
							} else {
								return part;
							}
						}
					}
				}
			}
			// Once good is false, it cannot be come true again.
			_good = false;
			return null;
		}

	}
}