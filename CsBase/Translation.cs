﻿#if NEW_TRANSLATION
using System;
using System.Collections.Generic;
//
namespace Cobasoft {
	//
	///<summary>Targets for code generation.</summary>
	[Flags]
	public enum TranslationTargets : ushort {
		None = 0, // Unspecified
		Lib = 1, // Base libraries
		App = 2, // Main application (server)
		UI = 4, // UI (HTML)
		JS = 8, // JavaScript
	}
	//
	///<summary>Translation (T9n) from TranslationID to String, depending on language.</summary>
	public class Translation {
		//
		///<summary>Starting with 100. 100-999 are Language translations (int)</summary>
		public int TranslationID;
		//
		///<summary>Targets for code generation.</summary>
		public TranslationTargets Targets;
		//
		///<summary>Mnemonic for identifying this translation string.</summary>
		public string Mnemonic;
		//
		///<summary>Translation hints and comments.</summary>
		public string Comment;
		//
		///<summary>Dictionary of translations per language.</summary>
		public Dictionary<int, string> Texts;
		//
		//
		///<summary>Constructor</summary>
		public Translation( int translationID, int targets,
			string mnemonic, string comment, params string[] texts ) {
			TranslationID = translationID;
			Targets = (TranslationTargets) targets;
			Mnemonic = mnemonic;
			Comment = comment;
			// Store all texts. LanguageIDs are assigned automatically.
			var I = texts.Length;
			Texts = new Dictionary<int, string>( I );
			for ( int i = 0; i < I; i++ ) {
				Texts.Add( 100 + i, texts[ i ] );
			}
		}
	}
}
#endif
