﻿// License and Copyright: see License.cs
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
//
namespace Cobasoft {
	//
#if NEW_TRANSLATION
	//
	///<summary>Global translation lookup.</summary>
	public static class TL {
		//
		private static readonly object _Lock = new object();
		//
		private static Dictionary<int, Translation> _dictionary = null;
		private static Dictionary<long, string> _index = null;
		private static Dictionary<string, int> _names = null;
		//
		///<summary>Initialize from array.</summary>
		public static void Initialize( Translation[] translations ) {
			if ( Log.Assert( translations.IsFilled() ) ) {
				lock ( _Lock ) {
					// Create dictionaries
					_dictionary = new Dictionary<int, Translation>();
					_index = new Dictionary<long, string>();
					_names = new Dictionary<string, int>();
					// Loop over source array
					var I = translations.Length;
					for ( int i = 0; i < I; i++ ) {
						var translation = translations[ i ];
						// Add to primary dictionary
						_dictionary.Add( translation.TranslationID, translation );
						// Loop over all contained translations
						foreach ( var kv in translation.Texts ) {
							// Create the combined key
							var translationID = translation.TranslationID;
							var key = Base.MakeKey( translation.TranslationID, kv.Key );
							// Add to fast lookup index
							_index.Add( key, kv.Value );
							// Add to fast reverse lookup index
							_names.Add( translation.Mnemonic, translationID );
						}

					}
				}
			}
		}
		//
		//
		///<summary>Initialize from other dictionary.</summary>
		public static void Initialize( Dictionary<int, Translation> dictionary ) {
			if ( Log.Assert( dictionary.IsFilled() ) ) {
				Initialize( dictionary.Values.ToArray() );
			}
		}
		//
		///<summary>Initialize from JSON file.</summary>
		static void InitializeFromFile( string fname = "translations.json" ) {
			try {
				var path = Log.AppBase;
				var fspec = Path.Combine( path, fname );
				var json = File.ReadAllText( fspec );
				if ( Log.Assert( json.IsFilled() ) ) {
					var array = JsonSerializer.Deserialize<Translation[]>( json );
					if ( array is not null ) {
						Initialize( array );
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
		}
		//
		///<summary>Check if this singleton is initialized.</summary>
		public static bool IsInitialized() {
			return _dictionary is not null;
		}
		//
		//
		///<summary>Check if this singleton is initialized. If not: Initialize!</summary>
		static void CheckInitialized() {
			if ( !IsInitialized() ) {
				InitializeFromFile();
			}
		}
		//
		///<summary>Try to get the translation description.</summary>
		public static Translation GetTranslation( int translationID ) {
			CheckInitialized();
			if ( _dictionary.TryGetValue( translationID, out Translation translation ) ) {
				return translation;
			}
			return null;
		}
		//
		///<summary>Try to get a single language string.</summary>
		public static string Get( int translationID, int languageID ) {
			CheckInitialized();
			var key = Base.MakeKey( translationID, languageID );
			if ( _index.TryGetValue( key, out string value ) ) {
				return value;
			}
			return CS.QQQ;
		}
		//
		///<summary>Try to get translationID for given mnemonic.</summary>
		public static int Get( string mnemonic ) {
			CheckInitialized();
			if ( _names.TryGetValue( mnemonic, out int value ) ) {
				return value;
			}
			return 0;
		}
		//
		///<summary>Try to get translated string, then use it for String.Format.</summary>
		public static string Format( int tlnID, int lngID, params object[] args ) {
			var s1 = Get( tlnID, lngID );
			if ( s1 is not null ) {
				return String.Format( s1, args );
			}
			return null;
		}
		//
		///<summary>Formats and translates Outcomes.</summary>
		///<remarks>
		/// If Data is present and the translated string contains placeholders (like "{0}"), 
		/// then the data will be formatted into the placeholders. 
		/// If Data contains Translate objects, these will be translated before being used.
		///</remarks>
		public static Outcomes Format( Outcomes outcomes, int lngID ) {
			outcomes.Format( lngID, Get );
			return outcomes;
		}
		//
		///<summary>Delegate for language translations.</summary>
		public delegate string TranslateID( int tlnID, int lngID );
	}
#else
	///<summary>Provide translation.</summary>
	public class TranslationLookup {
		//
		private SortedDictionary<long, string> _dictionary = null;
		private SortedDictionary<string, int> _lookup = null;
		//
		public TranslationLookup() {
		}
		//
		///<summary>Initialize the key-string dictionary with a copy of the given dictionary.</summary>
		public void SetDictionary( Dictionary<long, string> dictionary ) {
			Log.VerboseV( Log.CM(), "Count", dictionary.Count );
			// Create a copy to become independent of the original source
			_dictionary = new SortedDictionary<long, string>( dictionary );
			Log.Assert( CheckDictionary() );
		}
		//
		///<summary>Initialize the mnemonic lookup dictionary with a copy of the given dictionary.</summary>
		public void SetLookup( Dictionary<string, int> lookup ) {
			Log.VerboseV( Log.CM(), "Count", lookup.Count );
			// Create a copy to become independent of the original source
			_lookup = new SortedDictionary<string, int>( lookup );
		}
		//
		///<summary>Cross-check the dictionary.</summary>
		public bool CheckDictionary() {
			if ( Log.Deny( _dictionary is null ) ) return false;
			// TASK: Implement this!
			//using ( new LogTime( true ) ) {
			//	// Allocate languages
			//	var languages = new Dictionary<int, List<int>>();
			//	long[] keys = null;
			//	_dictionary.Keys.CopyTo( keys, 0 );
			//	for ( int i = 0; i < keys.Length; i++ ) {
			//		var key = keys[ i ];
			//		SplitKey( key, out int tlnID, out int lngID );
			//		// If languages does not already contain the language ID, create it.
			//		if ( !languages.ContainsKey( lngID ) ) {
			//			languages[ lngID ] = new List<int>();
			//		}
			//		// Put this translation ID into this list.
			//		languages[ lngID ].Add( tlnID ); ;
			//	}
			//	// Find minimum and maximum values
			//	int min = int.MaxValue, max = 0;
			//	foreach ( var kv in languages ) {
			//		var list = kv.Value;
			//		if ( list[ 0 ] > min ) min = list[ 0 ];
			//		if ( list[ list.Count - 1 ] < max ) max = list[ list.Count - 1 ];
			//	}
			//	// Cross-check all languages
			//	for ( int i = min; i <= max; i++ ) {
			//	}
			//}
			return true;
		}
		//
		//
		///<summary>Return the translated value.</summary>
		public string Get( int tlnID, int lngID ) {
			if ( 0 == tlnID || 0 == lngID || _dictionary is null ) return null;
			var key = Base.MakeKey( tlnID, lngID );
			if ( _dictionary.TryGetValue( key, out string value ) ) {
				return value;
			}
			Log.WarningV( "Translation NOT FOUND", "tlnID", tlnID, "lngID", lngID );
			return null;
		}
		//
		///<summary>Get the translated string. If found, use it for String.Format.</summary>
		public string Format( int tlnID, int lngID, params object[] args ) {
			var s1 = Get( tlnID, lngID );
			if ( s1 is not null ) {
				return String.Format( s1, args );
			}
			return null;
		}
		//
		///<summary>Return the translated value.</summary>
		public string Get( string name, int lngID ) {
			if ( name.IsNullOrEmpty() || 0 == lngID || _lookup is null ) return null;
			if ( _lookup.TryGetValue( name, out int tlnID ) ) {
				return Get( tlnID, lngID );
			}
			Log.WarningV( "Lookup NOT FOUND", "name", name, "lngID", lngID );
			return null;
		}
		//
		///<summary>Get the translated string. If found, use it for String.Format.</summary>
		public string Format( string name, int lngID, params object[] args ) {
			if ( name.IsNullOrEmpty() || 0 == lngID || _lookup is null ) return null;
			if ( _lookup.TryGetValue( name, out int tlnID ) ) {
				return Format( tlnID, lngID, args );
			}
			Log.WarningV( "Lookup NOT FOUND", "name", name, "lngID", lngID );
			return null;
		}
		//
		///<summary>Formats and translates Outcomes.
		/// If Data is present and the translated string contains placeholders (like "{0}"), 
		/// then the data will be formatted into the placeholders. 
		/// If Data contains Translate objects, these will be translated before being used.
		///</summary>
		public Outcomes Format( Outcomes outcomes, int lngID ) {
			outcomes.Format( lngID, Get );
			return outcomes;
		}
	}
	//
	//
	//
	//
	//
	//
	///<summary>Global translation provider class.</summary>
	public class TL {
		//
		private static readonly object _Lock = new object();
		//
		private static TranslationLookup _Singleton = null;
		//
		public static bool IsInitialized() {
			return _Singleton is not null;
		}
		//
		//
		public static void Initialize( Dictionary<long, string> dictionary, Dictionary<string, int> lookup ) {
			lock ( _Lock ) { // protect against multiple entry
				_Singleton = new TranslationLookup();
				_Singleton.SetDictionary( dictionary );
				_Singleton.SetLookup( lookup );
				_Singleton.CheckDictionary();
			}
		}
		//
		//
		// This must be kept synchronous with JSON file.
		public class Translation {
			public int TranslationID { get; set; }
			public int LanguageID { get; set; }
			public string Hint { get; set; }
			public string Value { get; set; }
		}
		//
		//
		///<summary>Initialize from JSON file. Currently supports only key lookup, no strings.</summary>
		public static void Initialize( Translation[] translations ) {
			using ( new LogTime( true ) ) {
				try {
					// Create the long-key based dictionary
					var dictionary1 = new Dictionary<long, string>();
					foreach ( var item in translations ) {
						if ( item.TranslationID < 100000 ) {
							var key = Base.MakeKey( item.TranslationID, item.LanguageID );
							dictionary1.Add( key, item.Value );
						}
					}
					if ( Log.Assert( dictionary1.Count > 0 ) ) {
						//// Create the string-key based lookup table.
						//var names = Enum.GetNames( typeof( TC ) );
						//var values = (int[]) Enum.GetValues( typeof( TC ) );
						var dictionary2 = new Dictionary<string, int>();
						//for ( int i = 0; i < values.Length; i++ ) {
						//	dictionary2.Add( names[ i ], values[ i ] );
						//}
						//if ( Log.Assert( dictionary2.Count > 0 ) ) {
						TL.Initialize( dictionary1, dictionary2 );
						//
						//// Check the database content against the values.
						//foreach ( var item in translations ) {
						//	var found = false;
						//	for ( int i = 0; i < values.Length; i++ ) {
						//		if ( item.TranslationID == values[ i ] ) {
						//			found = true; break;
						//		}
						//	}
						//	if ( !found ) Log.WarningV( "Translation not found", "item", item );
						//}
						//}
					}
				} catch ( Exception ex ) {
					Log.Exception( ex );
				}
			}
		}
		//
		//
		static void InitializeFromFile() {
			try {
				var path = Log.AppBase;
				var fspec = Path.Combine( path, "translation.json" );
				var json = File.ReadAllText( fspec );
				if ( json.IsFilled() ) {
					var translations = JsonSerializer.Deserialize<Translation[]>( json );
					if ( translations is not null ) {
						Initialize( translations );
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
		}
		//
		//
		static void CheckInitialized() {
			if ( !IsInitialized() ) {
				InitializeFromFile();
			}
		}
		//
		///<summary>Split a 64-bit key into l1 and l2.</summary>
		public static void SplitKey( long key, out int tlnID, out int lngID ) {
			tlnID = (int) ( ( (ulong) key ) >> 32 );
			lngID = (int) (uint) (ulong) key;
		}
		//
		///<summary>Create new Translate object, for insertion into Outcomes, like: "outcomes.AddError( 1234, 0, TL.Val( 3456 ) );"</summary>
		public static Translate Val( int tlnID ) {
			return new Translate( tlnID );
		}
		//
		//
		// ECHO METHODS 
		//
		public static string Get( int tlnID, int lngID ) {
			CheckInitialized();
			return _Singleton.Get( tlnID, lngID );
		}
		public static string Get( string name, int lngID ) {
			CheckInitialized();
			return _Singleton.Get( name, lngID );
		}
		public static string Format( int tlnID, int lngID, params object[] args ) {
			CheckInitialized();
			return _Singleton.Format( tlnID, lngID, args );
		}
		public static string Format( string name, int lngID, params object[] args ) {
			CheckInitialized();
			return _Singleton.Format( name, lngID, args );
		}
		public static Outcomes Format( Outcomes outcomes, int lngID ) {
			CheckInitialized();
			return _Singleton.Format( outcomes, lngID );
		}
		//
		///<summary>Delegate for language translations.</summary>
		public delegate string TranslateID( int tlnID, int lngID );
	}
	//
#endif
	//
	///<summary>For passing translatable values within Outcomes, like: "outcomes.AddError( 1234, 0, new Translate( 3456 ) );"</summary>
	public struct Translate {
		//
		private readonly int _translationID;
		//
		public Translate( int translationID ) {
			_translationID = translationID;
		}
		public string Get( int translationID ) {
			return TL.Get( _translationID, translationID );
		}
	}
	//
}
