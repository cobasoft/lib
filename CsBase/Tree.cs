﻿// License and Copyright: see License.cs
// Inspired by: https://stackoverflow.com/questions/66893/tree-data-structure-in-c-sharp
//
using System;
using System.Collections.Generic;
//
namespace Cobasoft {
	//
	// 
	///<summary>Custom callback for processing and filtering. Return false to stop iteration.</summary>
	public delegate bool TreeVisitor<T>( Tree<T> root, Tree<T> node, int level, int position, object data );
	//
	///<summary>Custom callback for cloning.</summary>
	public delegate T TreeDataClone<T>( Tree<T> root, Tree<T> node, int level, int position, object filterInfo, object data );
	//
	//
	// TASK: Implement non-generic base class. Allow more custom data.
	///<summary>Simple generic, recursive tree.</summary>
	public class Tree<T> : IToJson {
		//
		//
		///<summary>Data item of this tree node.</summary>
		public T Data;
		//
		///<summary>Children of this tree node.</summary>
		public List<Tree<T>> Children;
		//
		//
		///<summary>Reserved for users of this class. Not directly used here.</summary>
		public int Position;
		//
		//
		///<summary>Reserved for users of this class. Not directly used here.</summary>
		public object Cargo;
		//
		//
		///<summary>If this is turned on by TreeVisitor, immediate children are not processed.</summary>
		public bool Stop;
		//
		//
		//
		///<summary>Empty constructor. Not publicly visible because Data must be initialized.</summary>
		private Tree() {
		}
		//
		///<summary>Constructor with data and empty childrens list.</summary>
		public Tree( T data ) {
			this.Data = data;
			Children = new List<Tree<T>>();
		}
		//
		//
		//
		public override string ToString() {
			return Log.VarVal( "Tree", "Data", Data, "Children", Children.Count, "Position", Position, "Cargo", Cargo );
		}
		//
		//
		//
		///<summary>Recursive processing of all items, unless stopped.</summary>
		private static bool _Traverse( Tree<T> root, Tree<T> node, int level, int position, object data, TreeVisitor<T> visitor, TreeVisitor<T> revisit ) {
			// Item itself
			if ( visitor is not null ) {
				if ( !visitor( root, node, level, position, data ) ) return false;
			}
			if ( !node.Stop ) {
				// All kids
				position = 0;
				if ( node.Children is not null ) {
					foreach ( var kid in node.Children ) {
						if ( !_Traverse( root, kid, 1 + level, position++, data, visitor, revisit ) ) return false;
					}
				}
			} else {
				node.Stop = false;
			}
			// Revisit
			if ( revisit is not null ) {
				if ( !revisit( root, node, level, position, data ) ) return false;
			}
			return true;
		}
		//
		///<summary>Recursive processing of all items, unless stopped.</summary>
		public void Traverse( TreeVisitor<T> visitor, object data, TreeVisitor<T> revisit = null ) {
			_Traverse( this, this, 0, 0, data, visitor, revisit );
		}
		//
		//
		//
		///<summary>Recursive cloning of all items. Filtering and cloning methods are provided by caller.</summary>
		private static bool _Clone( Tree<T> root, Tree<T> node, int level, int position, Tree<T> clone, TreeVisitor<T> filter, object filterInfo, TreeDataClone<T> cloner ) {
			position = 0;
			if ( node.Children is not null ) {
				foreach ( var kid in node.Children ) {
					var found = true;
					if ( filter is not null ) {
						found = filter( root, kid, level, position, filterInfo );
					}
					if ( found ) {
						var data = kid.Data;
						if ( cloner is not null ) data = cloner( root, kid, level, position, filterInfo, data );
						var newkid = new Tree<T>( data );
						if ( clone.Children is not null ) {
							clone.Children.Add( newkid );
						}
						_Clone( root, kid, 1 + level, position++, newkid, filter, filterInfo, cloner );
					}
				}
			}
			return true;
		}
		//
		///<summary>Recursive cloning of all items. Filtering and cloning methods are provided by caller.</summary>
		public Tree<T> Clone( TreeVisitor<T> filter, object filterInfo, TreeDataClone<T> cloner ) {
			var data = Data;
			if ( cloner is not null ) data = cloner( this, this, 0, 0, filterInfo, data );
			var clone = new Tree<T>( data );
			_Clone( this, this, 0, 0, clone, filter, filterInfo, cloner );
			return clone;
		}
		//
		//
		//
		///<summary>Remove given item from children.</summary>
		public void Remove( Tree<T> kid ) {
			if ( Children.IsFilled() ) {
				Children.Remove( kid );
			}
		}
		//
		//
		///<summary>Write all items to logging.</summary>
		private static bool _DumpLog( Tree<T> root, Tree<T> node, int level, int position, object data ) {
			// Indentation
			level = level % 10;
			var t = (T) node.Data;
			var s = t is null ? CS.null_ : t.ToString();
			var k = node.Children;
			var n = k is null ? CS.N0 : k.Count.ToString();
			Log.DataV( CS.Tabs[ level ], "P", position.ToString(), "L", level.ToString(), "N", n, s );
			return true; // continue
		}
		//
		///<summary>Write all items to logging.</summary>
		public void DumpLog() {
			Traverse( _DumpLog, null );
		}
		//
		//
		///<summary>Find first item matching the predicate.</summary>
		private static bool _Find( Tree<T> root, Tree<T> node, int level, int position, object data ) {
			var quad = data as Quadruple<Predicate<T>, List<T>, bool, int>;
			var match = quad.A;
			var list = quad.B;
			var all = quad.C;
			if ( match is null || match( node.Data ) ) {
				list.Add( node.Data );
				if ( !all && 1 == list.Count ) {
					return false;
				}
			}
			return true; // continue
		}
		//
		///<summary>Find first item matching the predicate.</summary>
		public T Find( Predicate<T> match ) {
			var list = new List<T>();
			var quad = new Quadruple<Predicate<T>, List<T>, bool, int>( match, list, false, 0 );
			Traverse( _Find, quad );
			if ( list.IsFilled() ) {
				return list[ 0 ];
			}
			return default( T );
		}
		//
		///<summary>Find all items matching the predicate. Can also be used to flatten the tree.</summary>
		public List<T> FindAll( Predicate<T> match = null ) {
			var list = new List<T>();
			var quad = new Quadruple<Predicate<T>, List<T>, bool, int>( match, list, true, 0 );
			Traverse( _Find, quad );
			return list;
		}
		//
		//
		//
		//
		///<summary>Serialize the complete tree into json. Fields are currently not being used.</summary>
		private static bool _ToJson( Tree<T> root, Tree<T> node, int level, int position, JsonBuilder jb ) {
			jb.Object();
			var hasData = false;
			if ( node.Data is not null ) {
				hasData = true;
				if ( node.Data is T t ) {
					if ( t is IToJson tj ) {
						jb.N( "data" );
						tj.ToJson( jb );
					}
				}
			}
			if ( node.Children is not null ) {
				if ( hasData ) {
					jb.Comma();
				}
				jb.N( "children" ).Array();
				position = 0;
				foreach ( var kid in node.Children ) {
					if ( !_ToJson( root, kid, 1 + level, position++, jb ) ) {
						return false; // stop
					}
				}
				jb.Close();
			}
			jb.Close();
			return true; // continue
		}
		//
		///<summary>Serialize the complete tree into json. Fields are currently not being used.</summary>
		public JsonBuilder ToJson( JsonBuilder jb, int[] fields = null ) {
			_ToJson( this, this, 0, 0, jb );
			return jb;
		}
		//																								   
	}
	//
	//
	///<summary>Simple generic tree.</summary>
	public static class TreeEx {
		//
		///<summary>Check if tree is null or empty.</summary>
		public static bool IsEmpty<T>( this Tree<T> t ) {
			return t is null || t.Data is null;
		}
		///<summary>Check if tree is null or empty.</summary>
		public static bool IsNullOrEmpty<T>( this Tree<T> t ) {
			return t is null || t.Data is null;
		}
		//
		///<summary>Check if tree is null or empty.</summary>
		public static bool IsFilled<T>( this Tree<T> t ) {
			return !t.IsNullOrEmpty();
		}
		//																								   
	}
}
