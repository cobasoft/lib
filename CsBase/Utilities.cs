﻿// License and Copyright: see License.cs
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Specialized;
//
namespace Cobasoft {
	//
	//
	///<summary> Contains miscellaneous tool methods. </summary>
	public static partial class Util {
		//
		//
		// see MSDN: "Naming Files, Paths, and Namespaces"
		//
		private static string[] badNames = new string[] { "CON", "PRN", "AUX", "NUL",
			"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9",
			"LPT1", "LPT2", "LPT3", "LPT4", "LPT5", "LPT6", "LPT7", "LPT8", "LPT9" };
		private static string wildChars = "*?";
		private static string ctrlChars = "\x0\x1\x2\x3\x4\x5\x6\x7\x8\x9\xA\xB\xC\xD\xE\xF\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1A\x1B\x1C\x1D\x1E\x1F";
		private static string badChars = "<>:\"/|\x5C"; // the backslash must be the last character in this string!
														//
														// an additional backslash is required to escape the preceding backslash.
		private static Regex rxBad = new Regex( CS.B1 + ctrlChars + badChars + CS.BS + wildChars + CS.B2 );
		//
		private static readonly char[] pathSeps = new char[] {
			Path.DirectorySeparatorChar,
			Path.AltDirectorySeparatorChar,
			//th.PathSeparator, -- for separating multiple paths
			Path.VolumeSeparatorChar,
		};
		//
		///<summary>Split a path according to the file system rules.</summary>
		public static string[] SplitPath( string path ) {
			if ( path is null ) return null;
			return path.Split( pathSeps, StringSplitOptions.RemoveEmptyEntries );
		}
		//
		///<summary>Exception handler around Path.GetFullPath.</summary>
		public static string GetFullPath( string path, out string message ) {
			try {
				message = null;
				return Path.GetFullPath( path );
			} catch ( Exception ex ) {
				Log.Exception( "GetFullPath", ex );
				message = ex.Message;
			}
			return null;
		}
		//
		///<summary>Check for valid file name or wildcard.</summary>
		public static bool CheckFileName( string name, bool wildcard, ref bool ctrl, ref bool bad, ref bool wild, ref bool dtdt, ref bool dtbk, ref bool trailsp, ref bool traildt, ref bool dos ) {
			if ( name is null ) return false;
			ctrl = name.ContainsChar( ctrlChars );
			bad = name.ContainsChar( badChars );
			wild = !wildcard && name.ContainsChar( wildChars );
			dtdt = name.Contains( ".." );
			dtbk = name.StartsWith( ".\\" );
			trailsp = name.EndsWith( " " );
			traildt = name.EndsWith( "." );
			dos = badNames.Contains( name.Trim().ToUpper() );
			// return the result of all checks
			return !( ctrl || bad || wild || dtdt || dtbk || trailsp || traildt || dos );
		}
		//
		private static void CheckFileNameAddOutcome( Outcomes outcomes, int reason ) {
			outcomes.Add( OutcomeCode.Error, TC.CorValueMustNotContain, 0, reason );
		}
		//
		///<summary>Check for valid file name or wildcard. Return outcomes with detailed error information.</summary>
		public static Outcomes CheckFileName( string name, bool wildcard = false ) {
			var outcomes = new Outcomes();
			if ( name.IsNullOrEmpty() ) {
				outcomes.Add( OutcomeCode.Error, TC.CorValueMustNotBeEmpty, 0 );
				return outcomes;
			}
			bool ctrl = false; bool bad = false; bool wild = false; bool dtdt = false; bool dtbk = false; bool trailsp = false; bool traildt = false; bool dos = false;
			CheckFileName( name, wildcard, ref ctrl, ref bad, ref wild, ref dtdt, ref dtbk, ref trailsp, ref traildt, ref dos );
			if ( ctrl ) CheckFileNameAddOutcome( outcomes, TC.CorControlCharacters );
			if ( bad ) CheckFileNameAddOutcome( outcomes, TC.CorReservedCharacters );
			if ( wild ) CheckFileNameAddOutcome( outcomes, TC.CorWildcardCharacters );
			if ( dtdt ) CheckFileNameAddOutcome( outcomes, TC.CorDoubleDotCharacters );
			if ( dtbk ) CheckFileNameAddOutcome( outcomes, TC.CorLeadingDotBackslashCharacters );
			if ( trailsp ) CheckFileNameAddOutcome( outcomes, TC.CorTrailingSpaceCharacter );
			if ( !dtdt && traildt ) CheckFileNameAddOutcome( outcomes, TC.CorTrailingDotCharacter );
			if ( dos ) CheckFileNameAddOutcome( outcomes, TC.CorReservedDosNames );
			return outcomes;
		}
		//
		///<summary>Remove invalid characters from filename.</summary>
		public static string FixFileName( string name, bool wildcard = false ) {
			var oldname = name;
			bool ctrl = false; bool bad = false; bool wild = false; bool dtdt = false; bool dtbk = false; bool trailsp = false; bool traildt = false; bool dos = false;
			var good = CheckFileName( name, wildcard, ref ctrl, ref bad, ref wild, ref dtdt, ref dtbk, ref trailsp, ref traildt, ref dos );
			if ( ctrl || bad || wild ) name = name.RxReplace( rxBad, CS.Empty );
			if ( dtdt ) name = name.Replace( "..", CS.UL );
			if ( dtbk ) name = name.Substring( 2 );
			if ( trailsp ) name = name.TrimEnd( CC.SP );
			if ( traildt ) name = name.TrimEnd( CC.DT );
			if ( dos ) name = CS.UL + name;
			if ( !good ) Log.InfoV( "FixFileName", "oldname", oldname, "name", name );
			return name;
		}
		//
		///<summary>Check a folder name or path. Return outcomes with detailed error information.</summary>
		public static Outcomes CheckFolderName( string name ) {
			var outcomes = new Outcomes();
			if ( name.IsNullOrEmpty() ) {
				outcomes.Add( OutcomeCode.Error, TC.CorFolderNameMustNotBeEmpty, 0 );
				return outcomes;
			}
			var parts = SplitPath( name );
			if ( parts.IsNullOrEmpty() ) {
				outcomes.Add( OutcomeCode.Error, TC.CorFolderNameIsInvalid, 0 );
				return outcomes;
			}
			var min = name.StartsWith( CS.BSBS ) ? 3 : 2;
			if ( parts.Length < min ) {
				outcomes.Add( OutcomeCode.Error, TC.CorFolderNameIsIncomplete, 0 );
				return outcomes;
			}
			// Check every part
			for ( int i = 0; i < parts.Length; i++ ) {
				var s = parts[ i ];
				// The 1st && 2nd entries may be null
				if ( s.IsNullOrEmpty() && ( i < 2 || i == parts.Length - 1 ) ) continue;
				var temp = CheckFileName( s );
				// Summarize the outcomes
				outcomes.AddRange( temp );
			}
			if ( outcomes.Success() ) {
				string message;
				var full = GetFullPath( name, out message );
				if ( full.IsNullOrEmpty() ) {
					outcomes.Add( OutcomeCode.Error, message, 0 );
					return outcomes;
				}
			}
			outcomes.UniqueMessages();
			return outcomes;
		}
		//
		//
		private static string badUploadExtensions = "*.ashx,*.asmx,*.aspq,*.aspx,*.axd,*.cshtm,*.cshtml,*.dll,*.exe,*.rem,*.shtm,*.shtml,*.soap,*.stm,*.vbhtm,*.vbhtml,*_AppService.axd,*ScriptResource.axd";
		//
		public static bool CheckUploadExtension( string name ) {
			var ext = Path.GetExtension( name );
			return !badUploadExtensions.Contains( ext );
		}
		//
		//
		//
		public static string ReplaceNames( string s, string[] keys, string[] vals ) {
			for ( int i = 0; i < keys.Length; i++ ) {
				s = s.Replace( keys[ i ], vals[ i ] );
			}
			return s;
		}
		//
		//
		public static string ReadFile( string fspec ) {
			try {
				string s = null;
				using ( var stream = new StreamReader( fspec ) ) {
					s = stream.ReadToEnd();
				}
				return s;
			} catch ( Exception ex ) {
				Log.ExceptionV( "ReadFile", ex, "fspec", fspec );
			}
			return null;
		}
		//
		//
		public static bool WriteFile( string fspec, string s, bool append = false ) {
			try {
				using ( var stream = new StreamWriter( fspec, append, Encoding.UTF8 ) ) {
					stream.Write( s );
				}
				return true;
			} catch ( Exception ex ) {
				Log.ExceptionV( "WriteFile", ex, "fspec", fspec );
			}
			return false;
		}
		//
		//
		public static bool DeleteFile( FileInfo fi ) {
			if ( Log.Deny( fi is null ) ) return false;
			try {
				fi.Delete();
				return true;
			} catch ( Exception ex ) {
				try { // Try to remove read-only flag.
					if ( fi.IsReadOnly ) {
						fi.IsReadOnly = false;
						fi.Delete();
						ex = null;
					}
				} catch ( Exception ex2 ) {
					ex = ex2;
				}
				if ( ex is not null ) {
					Log.Exception( "DeleteFile failed: " + fi.FullName, ex );
				}
			}
			return false;
		}
		//
		public static bool DeleteFile( string name ) {
			if ( Log.Deny( name.IsEmpty() ) ) return false;
			return Util.DeleteFile( new FileInfo( name ) );
		}
		//
		public static bool DeleteExistingFile( FileInfo fi ) {
			if ( fi.Exists ) {
				return Util.DeleteFile( fi );
			}
			return false;
		}
		//
		public static bool DeleteExistingFile( string name ) {
			return Util.DeleteExistingFile( new FileInfo( name ) );
		}
		//
		public static int DeleteWildcardFiles( string path, string wildcard ) {
			int count = 0;
			try {
				string[] names = Directory.GetFiles( path, wildcard, SearchOption.TopDirectoryOnly );
				if ( names is not null ) {
					for ( int i = 0; i < names.Length; i++ ) {
						string name = names[ i ];
						if ( Log.Assert( name.IsFilled() ) ) {
							if ( File.Exists( name ) ) {
								var fi = new FileInfo( name );
								if ( DeleteFile( fi ) ) count++;
							}
						}
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( "DeleteWildcardFiles failed: " + wildcard, ex );
			}
			return count;
		}
		//
		//
		public static int MoveWildcardFiles( string folder, string filename, string wildcards, string targetFolder ) {
			int count = 0;
			try {
				filename = Path.GetFileNameWithoutExtension( filename );
				var parts = wildcards.Split( CC.SC );
				foreach ( var part in parts ) {
					var wildcard = filename + part;
					string[] names = Directory.GetFiles( folder, wildcard, SearchOption.TopDirectoryOnly );
					if ( names is not null ) {
						for ( int i = 0; i < names.Length; i++ ) {
							string fspec = names[ i ];
							if ( File.Exists( fspec ) ) {
								var targetName = Path.GetFileName( fspec );
								var target = Path.Combine( targetFolder, targetName );
								var outcomes = MoveFile( fspec, target );
								if ( outcomes.Success() ) count++;
							}
						}
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( "MoveWildcardFiles failed: " + wildcards, ex );
			}
			return count;
		}
		//
		//
		public static string GetAssemblyPath( Type type ) {
			var a = Assembly.GetAssembly( type );
			var location = a.Location;
			var path = a.Location;
			path = Path.GetDirectoryName( path );
			Log.VerboseV( "GetAssemblyPath", "Location", location, "CodeBase", path );
			return path;
		}
		//
		//
		//
		//
		//
		// FILE AND DIRECTORY OPS
		//
		//
		//
		public static bool AllowEveryoneFullControl( string target ) {
			try {
				var fi = new FileInfo( target );
				if ( Log.Assert( fi.Exists ) ) {
#if CS_NET_CORE
					Log.Missing();
#else
					var fsec = fi.GetAccessControl();
					var rule = new FileSystemAccessRule( "Everyone", FileSystemRights.FullControl, AccessControlType.Allow );
					fsec.AddAccessRule( rule );
					fi.SetAccessControl( fsec );
					return true;
#endif
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( "AllowEveryone", target, ex );
			}
			return false;
		}
		//
		//
		public static bool SetReadOnly( string target, bool value ) {
			try {
				var attributes = File.GetAttributes( target );
				attributes = attributes & ~FileAttributes.ReadOnly; // clear
				if ( value ) {
					attributes |= FileAttributes.ReadOnly; // set
				}
				File.SetAttributes( target, attributes ); // store
				return true;
			} catch ( Exception ex ) {
				Log.ExceptionV( "SetReadOnly", target, ex );
			}
			return false;
		}
		//
		//
		public static bool CopyFile( string source, string target ) {
			try {
				var fi = new FileInfo( source );
				if ( Log.Assert( fi.Exists ) ) {
					var fi2 = fi.CopyTo( target, true );
					return fi2 is not null && fi2.Exists;
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( "CopyFile", target, ex );
			}
			return false;
		}
		//
		//
		public static bool CopyFile( string source, string target, string name, string rename = null ) {
			try {
				if ( rename is null ) { rename = name; }
				var from = Path.Combine( source, name );
				var into = Path.Combine( target, rename );
				var fi = new FileInfo( from );
				var fi2 = fi.CopyTo( into, true );
				return fi2 is not null && fi2.Exists;
			} catch ( Exception ex ) {
				Log.ExceptionV( "CopyFile", ex, "source", source, "target", target, "name", name );
			}
			return false;
		}
		//
		//
		public static Outcomes MoveFile( FileInfo fi, string target ) {
			const string MN = "MoveFile";
			var outcomes = new Outcomes();
			string source = null;
			try {
				source = fi.FullName;
				if ( File.Exists( source ) ) {
					SetReadOnly( source, false );
					DeleteExistingFile( target );
					// Try to move the file. This is much faster than copying, but it may fail.
					// Attention: MoveTo modifies FileInfo.
					fi.MoveTo( target );
					outcomes.Add( OutcomeCode.Success, TC.CorFileMovedTo, 0, target );
					// MoveTo does not move, but only copy the file, so try again.
					if ( File.Exists( source ) ) {
						Log.WarningV( "MoveFile: source was not removed", "source", source );
						DeleteExistingFile( source );
					}
				}
			} catch ( FileNotFoundException ) {
				outcomes.Add( OutcomeCode.Error, TC.CorFileNotFound, 0, source );
			} catch ( Exception ex ) {
				uint hr = Base.GetHResult( ex );
				if ( hr == 0x80070020 ) { // the process cannot access the file because it is being used by another process. 
					outcomes.Add( OutcomeCode.Error, TC.CorSourceFileIsLocked, 0, source );
				} else if ( hr == 0x800700b7 ) { // Cannot create a file when that file already exists.
					outcomes.Add( OutcomeCode.Error, TC.CorTargetFileIsLocked, 0, target );
				} else {
					try { // moving was not possible, now try to copy the file.
						fi.CopyTo( target, true );
						outcomes.Add( OutcomeCode.Success, TC.CorFileCopiedTo, 0, target );
						try { // try to delete the source file
							fi.Delete();
						} catch ( Exception ex4 ) {
							outcomes.Add( OutcomeCode.Exception, TC.CorCannotDeleteSource, 0, ex4, source );
						}
					} catch ( Exception ex3 ) {
						Log.Exception( MN, ex3 );
						outcomes.Add( ex3 );
					}
				}
			}
			Log.WriteV( outcomes.AnyError() ? Log.Type.Error : Log.Type.Verbose, MN, "source", source, "target", target, "outcomes", outcomes.ToString() );
			return outcomes;
		}
		//
		//
		public static Outcomes MoveFile( string source, string target ) {
			var fi = new FileInfo( source );
			return MoveFile( fi, target );
		}
		//
		//
		public static bool RenameFile( string path, string from, string into ) {
			try {
				var source = Path.Combine( path, from );
				var target = Path.Combine( path, into );
				File.Move( source, target );
				Log.VerboseV( "RenameFile OK", "path", path, "from", from, "into", into );
				return true;
			} catch ( Exception ex ) {
				Log.ExceptionV( "RenameFile", ex, "path", path, "from", from, "into", into );
			}
			return false;
		}
		//
		//
		// Adapted from: https://stackoverflow.com/questions/7344978/verifying-path-equality-with-net
		///<summary>Check if the given strings point to same folder or file.</summary>
		public static bool IsSameLocalFile( string f1, string f2 ) {
			try {
				var path1 = Path.GetFullPath( f1 ).Trim( CA.FsBsDt );
				var path2 = Path.GetFullPath( f2 ).Trim( CA.FsBsDt );
				var same = string.Equals( path1, path2, StringComparison.OrdinalIgnoreCase );
				//Log.VerboseV( Log.CM(), "same", same, "f1", f1, "f2", f2 );
				return same;
			} catch ( Exception ex ) {
				Log.ExceptionV( Log.CM(), ex, "f1", f1, "f2", f2 );
			}
			return false;
		}

		//
		//
		public static long FileLength( string fspec ) {
			try {
				var fi = new FileInfo( fspec );
				if ( fi.Exists ) {
					return fi.Length;
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( "FileLength", ex, "fspec", fspec );
			}
			return -1;
		}
		//
		//
		///<summary>Returns LastWriteTimeUtc</summary>
		public static DateTime FileChangedAt( string fspec ) {
			try {
				var fi = new FileInfo( fspec );
				if ( Log.Assert( fi.Exists ) ) {
					return fi.LastWriteTimeUtc;
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( "FileModified", ex, "fspec", fspec );
			}
			return DateTime.MinValue;
		}
		//
		//
		///<summary>Returns LastWriteTimeUtc</summary>
		public static DateTime FileCreatedAt( string fspec ) {
			try {
				var fi = new FileInfo( fspec );
				return fi.CreationTimeUtc;
			} catch ( Exception ex ) {
				Log.ExceptionV( "FileModified", ex, "fspec", fspec );
			}
			return DateTime.MinValue;
		}
		//
		//
		public static bool CreateDirectory( string target ) {
			if ( target is null ) return false;
			try {
				if ( Directory.Exists( target ) ) return true;
				var di = Directory.CreateDirectory( target );
				return di is not null && di.Exists;
			} catch ( Exception ex ) {
				Log.ExceptionV( "CreateDirectory", target, ex );
			}
			return false;
		}
		//
		//
		public static bool DeleteDirectory( string target ) {
			if ( target.IsFilled() ) {
				try {
					if ( Directory.Exists( target ) ) {
						Directory.Delete( target, true );
					}
					return true;
				} catch ( Exception ex ) {
					Log.ExceptionV( Log.CM(), target, ex );
				}
			}
			return false;
		}
		//
		//
		public static bool CopyDirectoryTree( DirectoryInfo source, DirectoryInfo target ) {
			const string MN = "CopyDirectoryTree";
			Log.VerboseV( MN, "source", source.FullName, "target", target.FullName );
			try {
				if ( !source.Exists ) return false;
				var targetName = target.FullName;
				// create target folder
				if ( !CreateDirectory( targetName ) ) return false;
				// copy all the files into the new directory
				var files = source.GetFiles();
				foreach ( var fi in files ) {
					var fspec = Path.Combine( targetName, fi.Name );
					Log.DataV( MN, fspec );
					fi.CopyTo( fspec, true );
				}
				// copy all the sub directories using recursion
				var dirs = source.GetDirectories();
				foreach ( var dir in dirs ) {
					var next = target.CreateSubdirectory( dir.Name );
					if ( !CopyDirectoryTree( dir, next ) ) return false; // break on error
				}
				return true;
			} catch ( Exception ex ) {
				Log.Exception( MN, ex );
			}
			return false;
		}
		//
		//
		public static bool CopyDirectoryTree( string source, string target ) {
			try {
				var di1 = new DirectoryInfo( source );
				var di2 = new DirectoryInfo( target );
				return CopyDirectoryTree( di1, di2 );
			} catch ( Exception ex ) {
				Log.Exception( ex );
				return false;
			}
		}
		//
		//
		///<summary>Try to create unique folder below the system temporary folder. Should work on the first try, but otherwise repeat up to 10 times.</summary>
		public static string CreateUniqueTemporaryFolder() {
			var temp = Path.GetTempPath();
			for ( int i = 0; i < 10; i++ ) {
				var guid = Guid.NewGuid();
				var subFolder = guid.ToString();
				var fullPath = Path.GetTempPath() + subFolder;
				if ( !Directory.Exists( fullPath ) ) {
					if ( CreateDirectory( fullPath ) ) {
						Log.VerboseV( Log.CM(), fullPath );
						return fullPath;
					}
				}
			}
			Log.WarningV( Log.CM(), "FAILED in temp", temp );
			return null;
		}
		//
		// File I/O on a file which is potentially locked by another thread or process
		//
		private static void HandleException( string label, string method, ref int count, int max, Exception ex, string fspec ) {
			uint hr = (uint) ex.HResult;
			if ( 0x80070020 == hr ) { // locked
				Log.VerboseV( label + CS.UL + method, "count", count.ToString(), "exception", ex.Message );
			} else {
				Log.ExceptionV( label + CS.UL + method, ex, "file name", fspec );
			}
			// Increment counter and sleep if it is not the last try
			if ( ++count < max ) Thread.Sleep( 50 );
		}
		//
		// Try to write a string to a file. Give up after trying max times.
		public static bool TryWriteFile( string label, string fspec, string data, int max ) {
			bool result = false;
			var count = 0;
			do {
				try {
					using ( FileStream fs = new FileStream( fspec, FileMode.Create, FileAccess.Write, FileShare.None ) ) {
						using ( StreamWriter sw = new StreamWriter( fs ) ) {
							sw.Write( data );
							result = true;
							Log.Verbose( "TryWriteFile OK: " + fspec );
						}
					}
				} catch ( Exception ex ) {
					HandleException( label, "TryWriteFile", ref count, max, ex, fspec );
				}
			} while ( !result && count < max );
			return result;
		}
		//
		// Try to read the complete contents of the file. Give up after max number of tries.
		public static string TryReadFile( string label, string fspec, int max ) {
			string result = null;
			var count = 0;
			do {
				try {
					using ( FileStream fs = new FileStream( fspec, FileMode.Open, FileAccess.Read, FileShare.Read ) ) {
						StreamReader sr = new StreamReader( fs );
						result = sr.ReadToEnd();
					}
				} catch ( Exception ex ) {
					HandleException( label, "Read", ref count, max, ex, fspec );
				}
			} while ( result is null && count < max );
			return result;
		}
		//
		// Try to read the complete text of the file. Give up after max number of tries.
		public static string TryReadText( string label, string fspec, int max, Encoding encoding = null ) {
			string result = null;
			var count = 0;
			do {
				try {
					result = encoding is null
						? File.ReadAllText( fspec )
						: File.ReadAllText( fspec, encoding );
				} catch ( Exception ex ) {
					HandleException( label, "ReadText", ref count, max, ex, fspec );
				}
			} while ( result is null && count < max );
			return result;
		}
		//
		///<summary>Get all file informations matching semicolon-separated wildcards.</summary>
		public static FileInfo[] GetFileInfos( this DirectoryInfo folder, string wildcards ) {
			var parts = wildcards.Trim( CC.SC ).Split( CA.SC, StringSplitOptions.RemoveEmptyEntries );
			var files = new Dictionary<String, FileInfo>();
			foreach ( var part in parts ) {
				var subset = folder.GetFiles( part );
				foreach ( var fi in subset ) {
					if ( fi.Name.IsFilled() ) {
						files.Add( fi.Name, fi );
					}
				}
			}
			var result = new FileInfo[ files.Count ];
			var i = 0;
			foreach ( var kv in files ) {
				result[ i++ ] = kv.Value;
			}
			return result;
		}
		//
		///<summary>Get all file names matching semicolon-separated wildcards.</summary>
		public static string[] GetFileNames( this DirectoryInfo folder, string wildcards ) {
			var parts = wildcards.Trim( CC.SC ).Split( CA.SC, StringSplitOptions.RemoveEmptyEntries );
			var files = new List<String>();
			foreach ( var part in parts ) {
				var subset = folder.GetFiles( part );
				foreach ( var fi in subset ) {
					if ( fi.Name.IsFilled() ) {
						files.Add( fi.FullName );
					}
				}
			}
			return files.ToArray();
		}
		//
		// Check if the current line contains the start pattern. If yes, return the name.
		public static bool StartBlock( string line, out string name ) {
			// Check the input argument
			if ( line.IsFilled() ) {
				// Get the indexes
				var head = line.IndexOf( "[[" );
				var tail = line.IndexOf( "]]" );
				// Found?
				if ( head > 0 && tail > 0 ) {
					// Get the name
					name = line.Substring( head + 2, tail - head - 2 );
					Log.Assert( name.Length < 64 );
					return true;
				}
			}
			name = null;
			return false;
		}
		//
		// Finish the current block.
		private static void FinishBlock( StringDictionary dictionary, string name, string[] parts, int from, int unto ) {
			var s = String.Join( CS.CrLf, parts, from, unto - from );
			s += CS.CrLf;
			dictionary[ name ] = s;
		}
		//
		///<summary>Split on input string into text blocks. Parts are separated by '[[NAME]]' patterns. Every text block is stored under its name into the dictionary.</summary>
		public static StringDictionary SplitString( string s ) {
			try {
				if ( s.IsFilled() ) {
					// Split the text into lines
					var parts = s.Split( CA.CrLf, StringSplitOptions.RemoveEmptyEntries );
					if ( parts is not null ) {
						var dictionary = new StringDictionary();
						// Loop over all lines
						int from = -1, i = 0;
						string name = null;
						string newname = null;
						var I = parts.Length;
						for ( ; i < I; i++ ) {
							// If this is a start line, store the name and start the body
							if ( StartBlock( parts[ i ], out newname ) ) {
								// If another block already exists, store it.
								if ( from > 0 && i > from ) {
									FinishBlock( dictionary, name, parts, from, i );
								}
								// Start the next block
								from = i + 1;
								name = newname;
							}
						}
						// If another block already exists, store it.
						if ( from > 0 && i >= from ) {
							FinishBlock( dictionary, name, parts, from, i );
						}
						return dictionary;
					}
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( Log.CM(), ex );
			}
			return null;
		}
		//
		///<summary>Split on input file into text blocks. Parts are separated by '[[NAME]]' patterns. Every text block is stored under its name into the dictionary.</summary>
		public static StringDictionary SplitFile( string fspec ) {
			try {
				if ( fspec.IsFilled() ) {
					// Try to read the complete text with standard encoding
					string s = TryReadText( Log.CM(), fspec, 1 );
					return SplitString( s );
				}
			} catch ( Exception ex ) {
				Log.ExceptionV( Log.CM(), ex, "fspec", fspec );
			}
			return null;
		}
		//
	}
	//
	//
	//
	//
	public static class CurrentVersion {
		//
		//
		public static string Assembly {
			get {
				return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
		}
		//
		//
		public static string Build {
			get {
				return FileVersionInfo.GetVersionInfo( System.Reflection.Assembly.GetExecutingAssembly().Location ).FileVersion;
			}
		}
		//
		//
		public static string Product {
			get {
				return FileVersionInfo.GetVersionInfo( System.Reflection.Assembly.GetExecutingAssembly().Location ).ProductVersion;
			}
		}
		//
		//
		public static string DbVersion( int v ) {
			if ( 0 > v ) v = -1 * v;
			var s = v.ToString();
			if ( 8 == s.Length ) {
				return s.Substring( 0, 4 ) + CS.DT + s.Substring( 4, 2 ) + CS.DT + s.Substring( 6, 2 );
			}
			return s;
		}
		//
		//public static string ExeVersion2 {
		//	get {
		//		object[] aobjAttribute = Assembly.GetExecutingAssembly().GetCustomAttributes( typeof( AssemblyFileVersionAttribute ), true );
		//		if ( aobjAttribute != null && aobjAttribute[ 0 ] != null ) {
		//			Version = ( aobjAttribute[ 0 ] as AssemblyFileVersionAttribute ).Version;
		//		}
		//	}
		//}
	}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	///<summary>Cache Service interface</summary>
	public interface ICacheService {
		//
		///<summary>Get an object with the given ID.</summary>
		T Get<T>( string cacheID ) where T : class;
		//
		///<summary>Store the given object.</summary>
		void Set<T>( string cacheID, T value ) where T : class;
	}
	//
	//
	//
	//
	//
	public class NameValidityCheck {
		// Limits. A == minimum, B == maximum.
		public Pair<int, int> Limit;
		public Pair<int, int> Upper;
		public Pair<int, int> Lower;
		public Pair<int, int> Digit;
		public Pair<int, int> Punct;
		public Pair<int, int> Control;
		public Pair<int, int> Higher;
		public Pair<int, int> Space;
		public Pair<int, int> Other;
		//
		///<summary>Test a single limit. Update outcomes collection.</summary>
		public static void CheckLimit( Outcomes outcomes, int value, Pair<int, int> limit, int kind ) {
			if ( value < limit.A ) {
				outcomes.Add( OutcomeCode.Error, TC.CorValueMustContainAtLeast, 0, limit.A, new Translate( kind ) );
			} else if ( value > limit.B ) {
				outcomes.Add( OutcomeCode.Error, TC.CorValueMustContainNoMoreThan, 0, limit.B, new Translate( kind ) );
			}
		}
		//
		///<summary>Test all limits. Return outcomes collection.</summary>
		public Outcomes Test( string value ) {
			var outcomes = new Outcomes();
			// Check length limits
			if ( value is null ) {
				outcomes.Add( OutcomeCode.Error, TC.CorValueMustNotBeEmpty, 0 );
			} else {
				// Count all the relevant character classes
				int u = 0, l = 0, d = 0, p = 0, c = 0, h = 0, s = 0, x = 0, t = value.Length;
				foreach ( var ch in value ) {
					if ( Char.IsUpper( ch ) ) u++;
					else if ( Char.IsLower( ch ) ) l++;
					else if ( Char.IsDigit( ch ) ) d++;
					else if ( Char.IsPunctuation( ch ) ) p++;
					else if ( (int) ch < 32 ) c++;
					else if ( (int) ch > 127 ) h++; // includes umlaut characters!!!
					else if ( Char.IsWhiteSpace( ch ) ) s++;
					else x++;
				}
				// Check the character class limits (leading space is mandatory).
				//CheckLimit( outcomes, t, Limit, TC._ );
				CheckLimit( outcomes, u, Upper, TC.CorUppercase );
				CheckLimit( outcomes, l, Lower, TC.CorLowercase );
				CheckLimit( outcomes, d, Digit, TC.CorDigit );
				CheckLimit( outcomes, p, Punct, TC.CorPunctuation );
				CheckLimit( outcomes, c, Control, TC.CorControl );
				CheckLimit( outcomes, h, Higher, TC.CorExtended );
				CheckLimit( outcomes, s, Space, TC.CorSpace );
				CheckLimit( outcomes, x, Other, TC.CorOther );
			}
			return outcomes;
		}
	}
}
