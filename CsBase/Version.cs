﻿using System;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
//
namespace Cobasoft {
	//
	//
	public class AssemblyVersion {
		//
		public static int CompareVersion( int[] left, int[] right ) {
			for ( int i = 0; i < left.Length; i++ ) {
				var l = left[ i ];
				var r = right[ i ];
				if ( l == r ) continue;
				if ( l < r ) return -1;
				return 1;
			}
			return 0; // equal
		}
		//
		public static int[] GetVersion( string name ) {
			int[] version = null;
			if ( Log.Assert( name.IsFilled() ) ) {
				try {
					var info = new FileInfo( name );
					if ( info is not null ) {
						if ( info.Exists ) {
							var date = info.LastWriteTimeUtc;
							version = new int[] { date.Year, date.Month, date.Day, date.Hour, date.Minute };
						}
					}
				} catch ( Exception ex ) {
					Log.Exception( ex );
				}
			}
			Log.DataV( Log.CM(), "version", version.Join( CS.HY ), "name", name );
			return version;
		}
		//
		public static string GetVersionString( int[] version ) {
			string result = null;
			if ( Log.Assert( version is not null && 5 == version.Length ) ) {
				// Creates string array
				var sa = new string[] {
						version[ 0 ].ToString(), CS.HY,
						version[ 1 ].ToString( "00" ), CS.HY,
						version[ 2 ].ToString( "00" ),
						version[ 3 ].ToString( "00" ) };
				// Join string
				result = String.Join( null, sa );
			}
			Log.DataV( Log.CM(), "result", result );
			return result;
		}
		//
		public static string GetVersionString( string location ) {
			try {
				if ( location.IsFilled() ) {
					var info = new FileInfo( location );
					if ( info is not null ) {
						var version = GetVersion( location );
						return GetVersionString( version );
					}
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
			return "";
		}
		//
		public static string GetVersionString() {
			string name = null, version = null;
			try {
				var assembly = Assembly.GetExecutingAssembly();
				if ( assembly is not null ) {
					name = assembly.Location;
					version = GetVersionString( name );
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
			return version;
		}
		//
		public static string GetPath() {
			string path = null;
			try {
				var assembly = Assembly.GetExecutingAssembly();
				if ( assembly is not null ) {
					path = Path.GetDirectoryName( assembly.Location );
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
			Log.DataV( Log.CM(), "path", path );
			return path;
		}
		//
		public static string GetName() {
			string name = null;
			try {
				var assembly = Assembly.GetExecutingAssembly();
				if ( assembly is not null ) {
					name = Path.GetFileNameWithoutExtension( assembly.Location );
				}
			} catch ( Exception ex ) {
				Log.Exception( ex );
			}
			Log.DataV( Log.CM(), "name", name );
			return name;
		}
		//
		public static string GetExeName() {
			Log.DataV( Log.CM(), "name", System.AppDomain.CurrentDomain.FriendlyName );
			return System.AppDomain.CurrentDomain.FriendlyName;
		}
		//
	}
}
