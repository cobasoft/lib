﻿// License and Copyright: see License.cs
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
//
#if CS_NET_CORE
using Microsoft.Extensions.Configuration;
#else
using System.Configuration;
using System.Web;
using System.Web.Configuration;
#endif
//
//
namespace Cobasoft {
	//
	///<summary>Basic utilities for accessing the application configuration.</summary>
	public class ConfigBase {
		//
		private static readonly string ClassName = "ConfigBase";
		//
#if CS_NET_CORE
		private static string _configFspec;
		private static IConfigurationRoot _Config;
#else
		private static Configuration _Config;
#endif
		//
#if CS_NET_CORE
		public static bool GetExeInfo( out string name, out string folder ) {
			var domain = AppDomain.CurrentDomain;
			if ( domain is null ) {
				name = folder = null;
			} else {
				name = domain.FriendlyName;
				folder = domain.BaseDirectory;
			}
			return !String.IsNullOrEmpty( name );
		}
#endif
		//
		//
		private static void Initialize( string configFileName = null ) {
			if ( _Config is null ) {
				try {
#if CS_NET_CORE
					bool found = false;
					string fname = null;
					string fspec = null;
					// Check the given location
					if ( configFileName is not null && 0 < configFileName.Length ) {
						fspec = configFileName;
						fname = Path.GetFileName( fspec );
						found = File.Exists( fspec );
					}
					if ( !found ) {
						// Try current directory
						var hasInfo = GetExeInfo( out var name, out var folder );
						if ( hasInfo ) {
							var current = Directory.GetCurrentDirectory();
							fname = name + ".json";
							fspec = Path.Combine( current, fname );
							found = File.Exists( fspec );
							if ( !found ) {
								// Try executable location
								if ( !String.IsNullOrEmpty( folder ) ) {
									fspec = Path.Combine( folder, fname );
									found = File.Exists( fspec );
								}
							}
						}
					}
					if ( found ) {
						var folder = Path.GetDirectoryName( fspec );
						var cb = new ConfigurationBuilder();
						cb.SetBasePath( folder );
						cb.AddJsonFile( fname );
						//
						_Config = cb.Build();
						// Only set this, when initialization was successful.
						_configFspec = fspec;
					}
#else
					if ( HttpContext.Current != null ) {
						_Config = WebConfigurationManager.OpenWebConfiguration( "~" );
					} else {
						bool found = false;
						string fname = null;
						string fspec = null;
						// Check the given location
						if ( configFileName is not null && 0 < configFileName.Length ) {
							fspec = configFileName;
							fname = Path.GetFileName( fspec );
							found = File.Exists( fspec );
						}
						if ( found ) {
							// Create a custom configuration file map
							var configMap = new ExeConfigurationFileMap();
							configMap.ExeConfigFilename = configFileName;
							// Open the configuration file
							_Config = ConfigurationManager.OpenMappedExeConfiguration( configMap, ConfigurationUserLevel.None );
						} else
							_Config = ConfigurationManager.OpenExeConfiguration( ConfigurationUserLevel.None );
					}
#endif
				} catch ( Exception ex ) {
					Log.Exception( ex );
				}
			}
		}
		//
		//
		public static void ResetConfig( string configFileName = null ) {
			_Config = null;
			if ( configFileName is not null && 0 < configFileName.Length ) {
				Initialize( configFileName );
			}
		}
		//
		//
		static ConfigBase() {
			Initialize();
		}
		//
		//
		///<summary>File name of the current configuration file.</summary>
		public static string FileName {
			get {
				Initialize();
#if CS_NET_CORE
				return _configFspec;
#else
				return _Config.FilePath;
#endif
			}
		}
		//
		//
		//
		///<summary>Indicates that this configuration is read-only.</summary>
		public static bool IsReadOnly() {
#if CS_NET_CORE
			return true;
#else
			return _Config.AppSettings.IsReadOnly();
#endif
		}
		private readonly static Type _StringType = typeof( string );
		//
		//
		//
		///<summary>Most basic function for retrieving values from configuration.</summary>
		public static string Get( string name, string value = null ) {
			if ( _Config is null ) {
				Initialize();
				if ( _Config is null ) return value;
			}
			try {
				object o = null;
#if CS_NET_CORE
				// 2024-06-02 Suddenly, this method returns empty string instead of null. Pending Microsoft request.
				o = _Config.GetValue( _StringType, name, value );
				if ( o is not null ) {
					value = (string) o;
				}
#else
				o = _Config.AppSettings.Settings[ name ];
				if ( o is not null ) {
					var kv = o as KeyValueConfigurationElement;
					if ( Log.Assert( kv is not null ) ) {
						value = kv.Value;
					}
				}
#endif
				if ( o is null && String.IsNullOrEmpty( value ) ) {
					Log.WarningV( Log.CM(), "Configuration value not found. Name", name );
				}
			} catch ( Exception ex ) {
				Log.Exception( ClassName + CS.ClSp + name, ex );
			}
			Log.DebugV( "Config.Get", name, value );
			return value;
		}
		//
		///<summary>Most basic function for setting values in configuration. Depending on configuration system, this may not always possible.</summary>
		public static bool Set( string name, string value ) {
#if CS_NET_CORE
			Log.Fail();
			return false;
#else
			try {
				if ( _Config.AppSettings.Settings[ name ] is not null ) {
					_Config.AppSettings.Settings[ name ].Value = value;
				} else {
					_Config.AppSettings.Settings.Add( name, value );
				}
				_Config.Save( ConfigurationSaveMode.Modified );
				ConfigurationManager.RefreshSection( "appSettings" );
				return true;
			} catch ( Exception ex ) {
				Log.Exception( ClassName + CS.ClSp + name + CS.ClSp + value, ex );
				return false;
			}
#endif
		}
	}
	/*
	Data Type	Range
	byte		0 .. 255
	sbyte		-128 .. 127
	short		-32,768 .. 32,767
	ushort		0 .. 65,535
	int			-2,147,483,648 .. 2,147,483,647
	uint		0 .. 4,294,967,295
	long		-9,223,372,036,854,775,808 .. 9,223,372,036,854,775,807
	ulong		0 .. 18,446,744,073,709,551,615
	float		-3.402823e38 .. 3.402823e38
	double		-1.79769313486232e308 .. 1.79769313486232e308
	decimal		-79228162514264337593543950335 .. 79228162514264337593543950335
	char		A Unicode character.
	string		A string of Unicode characters.
	bool		True or False.
	object		An object.
	Tri			-1,0,1;N,U,T
	*/
	//
	///<summary>Comfortable and type save application configuration access.</summary>
	public class Config {
		//
		//
		static Config() {
		}
		//
		//
		public static void Reset( string configFileName = null ) {
			ConfigBase.ResetConfig( configFileName );
		}
		//
		///<summary>Full file name of the current configuration file.</summary>
		public static string FileName { get { return ConfigBase.FileName; } }
		//
		///<summary>Indicates that this configuration is read-only.</summary>
		public static bool IsReadOnly() { return ConfigBase.IsReadOnly(); }
		//
		///<summary>Most basic function for retrieving values from configuration.</summary>
		public static string Get( string name, string value = null ) { return ConfigBase.Get( name, value ); }
		//
		///<summary>Get array of semicolon-separated strings. May return null.</summary>
		public static string[] GetStrings( string name, string value = null ) {
			var s = ConfigBase.Get( name, value );
			if ( String.IsNullOrEmpty( s ) ) return null;
			var parts = s.Split( CA.SC, StringSplitOptions.RemoveEmptyEntries );
			return parts;
		}
		//
		///<summary>Store an array of strings into a semicolon-separated string. (The values must not contain semicolons!)</summary>
		public static bool SetStrings( string name, string[] values ) {
			return SetString( name, String.Join( CS.SC, values ) );
		}
		//
		///<summary>Get an array of integer values from semicolon-separated strings. Only valid numbers are returned.</summary>
		public static int[] GetIntegers( string name, string value = null ) {
			var sa = GetStrings( name, value );
			if ( sa is null ) return null;
			var l = sa.Length;
			if ( 0 == l ) return null;
			var list = new List<int>();
			for ( int i = 0; i < l; i++ ) {
				if ( int.TryParse( sa[ i ], out var o ) ) {
					list.Add( o );
				}
			}
			if ( 0 == list.Count ) return null;
			return list.ToArray();
		}
		//
		///<summary>Get an array of decimal values from semicolon-separated strings. Only valid numbers are returned.</summary>
		public static decimal[] GetDecimals( string name, string value = null ) {
			var sa = GetStrings( name, value );
			if ( sa is null ) return null;
			var l = sa.Length;
			if ( 0 == l ) return null;
			var list = new List<decimal>();
			for ( int i = 0; i < l; i++ ) {
				if ( decimal.TryParse( sa[ i ], out var o ) ) {
					Log.Assert( 0 < o );
					list.Add( o );
				}
			}
			if ( 0 == list.Count ) return null;
			return list.ToArray();
		}
		//
		///<summary>Store an array of integer values into a semicolon-separated string.</summary>
		public static bool SetIntegers( string name, int[] values ) {
			return SetString( name, String.Join( CS.SC, values ) );
		}
		public static DateTime ParseDateTime( string value ) {
			if ( value is null || 0 == value.Length ) return DateTime.MinValue;
			value = value.Replace( CS.UL, CS.SP );
			// Only supports "2008-01-01 00:00:00Z"
			var ok = DateTime.TryParse( value, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal, out DateTime dt );
			if ( ok ) return dt;
			return DateTime.MinValue;
		}
		//
		public static DateTime GetDateTime( string name, DateTime value ) {
			var s = Get( name );
			if ( String.IsNullOrWhiteSpace( s ) ) return value;
			return ParseDateTime( s );
		}
		//
		public static string GetString( string name, string value = null ) { return ConfigBase.Get( name, value ); }
		public static bool SetString( string name, string value ) { return ConfigBase.Set( name, value ); }
		//
		//
		///<summary>Convert character into bool.</summary>
		public static bool ParseBool( char c, bool fallback = false ) {
			switch ( c ) {
				// 1, Yes, True, Ja, Checked
				case '1':
				case 'Y':
				case 'y':
				case 'T':
				case 't':
				case 'J':
				case 'j':
				case 'C':
				case 'c':
					return true;
				// 0, No, False, Unchecked
				case '0':
				case 'N':
				case 'n':
				case 'F':
				case 'f':
				case 'U':
				case 'u':
					return false;
			}
			return fallback;
		}
		//
		///<summary>Convert character into Tri.</summary>
		public static Tri ParseTri( char c, Tri fallback = Tri.Zero ) {
			switch ( c ) {
				// 1, Yes, True
				case '1':
				case 'Y':
				case 'y':
				case 'T':
				case 't':
					return Tri.Yes;
				// 0, No, False
				case '0':
				case 'N':
				case 'n':
				case 'F':
				case 'f':
					return Tri.No;
				// -1, Uncertain
				case '-':
				case 'U':
				case 'u':
					return Tri.No;
			}
			return fallback;
		}
		//
		///<summary>Convert string into bool.</summary>
		public static bool ParseBool( string s, bool fallback = false ) {
			if ( String.IsNullOrEmpty( s ) ) return fallback;
			s = s.Trim();
			if ( 0 == s.Length ) return fallback;
			return ParseBool( s[ 0 ] );
		}
		//
		///<summary>Convert string into Tri.</summary>
		public static Tri ParseTri( string s, Tri fallback = Tri.Zero ) {
			if ( String.IsNullOrEmpty( s ) ) return fallback;
			s = s.Trim();
			if ( 0 == s.Length ) return fallback;
			return ParseTri( s[ 0 ] );
		}
		//
		///<summary>Convert object into bool. Supports: numbers and strings: yes, true, ja, checked, 1.</summary>
		public static bool ParseBool( object o, bool fallback = false ) {
			if ( o is null ) return fallback;
			var t = o.GetType();
			var tc = Type.GetTypeCode( t );
			switch ( tc ) {
				case TypeCode.Boolean: return (bool) o;
				case TypeCode.Byte: return 0 != (byte) o;
				case TypeCode.Char: return ParseBool( (char) o );
				case TypeCode.DBNull: return fallback;
				case TypeCode.DateTime: return fallback;
				case TypeCode.Decimal: return 0 != (Decimal) o;
				case TypeCode.Double: return 0 != (Double) o;
				case TypeCode.Empty: return fallback;
				case TypeCode.Int16: return 0 != (Int16) o;
				case TypeCode.Int32: return 0 != (Int32) o;
				case TypeCode.Int64: return 0 != (Int64) o;
				case TypeCode.Object: return fallback;
				case TypeCode.SByte: return 0 != (SByte) o;
				case TypeCode.Single: return 0 != (Single) o;
				case TypeCode.String: return ParseBool( (string) o );
				case TypeCode.UInt16: return 0 != (UInt16) o;
				case TypeCode.UInt32: return 0 != (UInt32) o;
				case TypeCode.UInt64: return 0 != (UInt64) o;
				default: break;
			}
			return fallback;
		}
		//
		public static ulong ParseULong( string value ) { if ( value is null ) return 0; return ulong.Parse( value ); }
		public static long ParseLong( string value ) { if ( value is null ) return 0; return long.Parse( value ); }
		public static uint ParseUInt( string value ) { if ( value is null ) return 0; return uint.Parse( value ); }
		public static int ParseInt( string value ) { if ( value is null ) return 0; return int.Parse( value ); }
		public static ushort ParseUShort( string value ) { if ( value is null ) return 0; return ushort.Parse( value ); }
		public static short ParseShort( string value ) { if ( value is null ) return 0; return short.Parse( value ); }
		public static double ParseDouble( string value ) { if ( value is null ) return 0; return double.Parse( value ); }
		public static float ParseFloat( string value ) { if ( value is null ) return 0; return float.Parse( value ); }
		public static decimal ParseDecimal( string value ) { if ( value is null ) return 0; return decimal.Parse( value ); }
		//
		public static bool GetBool( string name ) { return ParseBool( Get( name ) ); }
		public static Tri GetTri( string name ) { return ParseTri( Get( name ) ); }
		public static ulong GetULong( string name ) { return ParseULong( Get( name ) ); }
		public static long GetLong( string name ) { return ParseLong( Get( name ) ); }
		public static uint GetUInt( string name ) { return ParseUInt( Get( name ) ); }
		public static int GetInt( string name ) { return ParseInt( Get( name ) ); }
		public static ushort GetUShort( string name ) { return ParseUShort( Get( name ) ); }
		public static short GetShort( string name ) { return ParseShort( Get( name ) ); }
		public static double GetDouble( string name ) { return ParseDouble( Get( name ) ); }
		public static float GetFloat( string name ) { return ParseFloat( Get( name ) ); }
		public static decimal GetDecimal( string name ) { return ParseDecimal( Get( name ) ); }
		//
		public static bool GetBool( string name, bool value ) { return ParseBool( Get( name, ( value ? 1 : 0 ).ToString() ) ); }
		public static Tri GetTri( string name, Tri value ) { return ParseTri( Get( name, ( (int) value ).ToString() ) ); }
		public static ulong GetULong( string name, ulong value ) { return ParseULong( Get( name, value.ToString() ) ); }
		public static long GetLong( string name, long value ) { return ParseLong( Get( name, value.ToString() ) ); }
		public static uint GetUInt( string name, uint value ) { return ParseUInt( Get( name, value.ToString() ) ); }
		public static int GetInt( string name, int value ) { return ParseInt( Get( name, value.ToString() ) ); }
		public static uint GetUShort( string name, uint value ) { return ParseUShort( Get( name, value.ToString() ) ); }
		public static int GetShort( string name, int value ) { return ParseShort( Get( name, value.ToString() ) ); }
		public static double GetDouble( string name, double value ) { return ParseDouble( Get( name, value.ToString() ) ); }
		public static float GetFloat( string name, float value ) { return ParseFloat( Get( name, value.ToString() ) ); }
		public static decimal GetDecimal( string name, decimal value ) { return ParseDecimal( Get( name, value.ToString() ) ); }
		//
		public static void Get( string name, ref string value ) { value = GetString( name ); }
		public static void Get( string name, ref bool value ) { value = GetBool( name ); }
		public static void Get( string name, ref Tri value ) { value = GetTri( name ); }
		public static void Get( string name, ref ulong value ) { value = GetULong( name ); }
		public static void Get( string name, ref long value ) { value = GetLong( name ); }
		public static void Get( string name, ref uint value ) { value = GetUInt( name ); }
		public static void Get( string name, ref int value ) { value = GetInt( name ); }
		public static void Get( string name, ref ushort value ) { value = GetUShort( name ); }
		public static void Get( string name, ref short value ) { value = GetShort( name ); }
		public static void Get( string name, ref double value ) { value = GetDouble( name ); }
		public static void Get( string name, ref float value ) { value = GetFloat( name ); }
		public static void Get( string name, ref decimal value ) { value = GetDecimal( name ); }
		public static void Get( string name, ref DateTime value ) { value = GetDateTime( name, DateTime.MinValue ); }
		public static void Get( string name, ref string[] value ) { value = GetStrings( name ); }
		public static void Get( string name, ref int[] value ) { value = GetIntegers( name ); }
		//
		public static bool Set( string name, string value ) { return ConfigBase.Set( name, value ); }
		public static bool Set( string name, bool value ) { return SetString( name, value ? CS.N1 : CS.N0 ); }
		public static bool Set( string name, long value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, int value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, ulong value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, uint value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, double value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, float value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, decimal value ) { return SetString( name, value.ToString() ); }
		public static bool Set( string name, DateTime value ) { return SetString( name, value.ISO() ); }
		public static bool Set( string name, string[] value ) { return SetStrings( name, value ); }
		public static bool Set( string name, int[] value ) { return SetIntegers( name, value ); }
#if CS_NET_CORE
		public static Int128 ParseInt128( string value ) { if ( value is null ) return 0; return Int128.Parse( value ); }
		public static Int128 GetInt128( string name ) { return ParseInt128( Get( name ) ); }
		public static void Get( string name, ref Int128 value ) { value = GetInt128( name ); }
#endif

	}
}
