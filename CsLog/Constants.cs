﻿// License and Copyright: see License.cs
using System;

namespace Cobasoft {
	//
	///<summary>Global, static character constants.</summary>
	public static partial class CC {
		public const char AM = '&';
		public const char AS = '*';
		public const char PD = '#';
		public const char AT = '@';
		public const char DL = '$';
		public const char B1 = '[';
		public const char B2 = ']';
		public const char BS = '\\';
		public const char C1 = '{';
		public const char C2 = '}';
		public const char CL = ':';
		public const char CM = ',';
		public const char DT = '.';
		public const char MD = '·'; // U+00B7: Middle Dot;
		public const char EQ = '=';
		public const char FS = '/';
		public const char GT = '>';
		public const char LT = '<';
		public const char N0 = '0';
		public const char N1 = '1';
		public const char N2 = '2';
		public const char N3 = '3';
		public const char N4 = '4';
		public const char N5 = '5';
		public const char N6 = '6';
		public const char N7 = '7';
		public const char N8 = '8';
		public const char N9 = '9';
		public const char LF = '\n';
		public const char CR = '\r';
		public const char P1 = '(';
		public const char P2 = ')';
		public const char PP = '|';
		public const char Q0 = '`';
		public const char Q1 = '\'';
		public const char Q2 = '\"';
		public const char QM = '?';
		public const char SC = ';';
		public const char SP = ' ';
		public const char TB = '\t';
		public const char UL = '_';
		public const char HY = '-';
		public const char MN = '−'; // U+2212 MINUS SIGN
		public const char EX = '!';
		public const char CT = '^';
		public const char TL = '~';
		public const char PC = '%';
		public const char PL = '+';
		public const char A = 'A';
		public const char B = 'B';
		public const char C = 'C';
		public const char D = 'D';
		public const char E = 'E';
		public const char F = 'F';
		public const char G = 'G';
		public const char H = 'H';
		public const char I = 'I';
		public const char J = 'J';
		public const char K = 'K';
		public const char L = 'L';
		public const char M = 'M';
		public const char N = 'N';
		public const char O = 'O';
		public const char P = 'P';
		public const char Q = 'Q';
		public const char R = 'R';
		public const char S = 'S';
		public const char T = 'T';
		public const char U = 'U';
		public const char V = 'V';
		public const char W = 'W';
		public const char X = 'X';
		public const char Y = 'Y';
		public const char Z = 'Z';
		public const char a = 'a';
		public const char b = 'b';
		public const char c = 'c';
		public const char d = 'd';
		public const char e = 'e';
		public const char f = 'f';
		public const char g = 'g';
		public const char h = 'h';
		public const char i = 'i';
		public const char j = 'j';
		public const char k = 'k';
		public const char l = 'l';
		public const char m = 'm';
		public const char n = 'n';
		public const char o = 'o';
		public const char p = 'p';
		public const char q = 'q';
		public const char r = 'r';
		public const char s = 's';
		public const char t = 't';
		public const char u = 'u';
		public const char v = 'v';
		public const char w = 'w';
		public const char x = 'x';
		public const char y = 'y';
		public const char z = 'z';
		// Monetary
		public const char moneyEuro = '€';
		// List Separator
		public const char FFF9 = '\xFFF9'; // INTERLINEAR ANNOTATION ANCHOR
		public const char FFFA = '\xFFFA'; // INTERLINEAR ANNOTATION SEPARATOR
		public const char FFFB = '\xFFFB'; // INTERLINEAR ANNOTATION TERMINATOR

		// Special symbols
		public const char Micro = '\x00B5'; // MICRO SIGN
		public const char Delta = '\x0394'; // GREEK CAPITAL LETTER DELTA
		public const char Sum = '\x2211'; // N-ARY SUMMATION
	}
	//
	///<summary>Global, static string constants.</summary>
	public static partial class CS {
		//
		// Internal database constants
		public static readonly string DbcNull = "[{Null}]";
		public static readonly string DbcHold = "[{Hold}]";
		//
		// Single-Character String Constants
		//
		public const string AM = "&";
		public const string AS = "*";
		public const string PD = "#";
		public const string AT = "@";
		public const string DL = "$";
		public const string B1 = "[";
		public const string B2 = "]";
		public const string BS = "\\";
		public const string C1 = "{";
		public const string C2 = "}";
		public const string CL = ":";
		public const string CM = ",";
		public const string DT = ".";
		public const string EQ = "=";
		public const string FS = "/";
		public const string GT = ">";
		public const string LT = "<";
		public const string N0 = "0";
		public const string N1 = "1";
		public const string N2 = "2";
		public const string N3 = "3";
		public const string N4 = "4";
		public const string N5 = "5";
		public const string N6 = "6";
		public const string N7 = "7";
		public const string N8 = "8";
		public const string N9 = "9";
		public const string LF = "\n";
		public const string CR = "\r";
		public const string P1 = "(";
		public const string P2 = ")";
		public const string P3 = "()";
		public const string PP = "|";
		public const string Q0 = "`";
		public const string Q1 = "'";
		public const string Q2 = "\"";
		public const string QM = "?";
		public const string SC = ";";
		public const string SP = " ";
		public const string TB = "\t";
		public const string UL = "_";
		public const string UL2 = "__";
		public const string HY = "-";
		public const string MN = "−"; // U+2212 MINUS SIGN
		public const string EX = "!";
		public const string CT = "^";
		public const string TL = "~";
		public const string PC = "%";
		public const string PL = "+";
		public const string A = "A";
		public const string B = "B";
		public const string C = "C";
		public const string D = "D";
		public const string E = "E";
		public const string F = "F";
		public const string G = "G";
		public const string H = "H";
		public const string I = "I";
		public const string J = "J";
		public const string K = "K";
		public const string L = "L";
		public const string M = "M";
		public const string N = "N";
		public const string O = "O";
		public const string P = "P";
		public const string Q = "Q";
		public const string R = "R";
		public const string S = "S";
		public const string T = "T";
		public const string U = "U";
		public const string V = "V";
		public const string W = "W";
		public const string X = "X";
		public const string Y = "Y";
		public const string Z = "Z";
		public const string a = "a";
		public const string b = "b";
		public const string c = "c";
		public const string d = "d";
		public const string e = "e";
		public const string f = "f";
		public const string g = "g";
		public const string h = "h";
		public const string i = "i";
		public const string j = "j";
		public const string k = "k";
		public const string l = "l";
		public const string m = "m";
		public const string n = "n";
		public const string o = "o";
		public const string p = "p";
		public const string q = "q";
		public const string r = "r";
		public const string s = "s";
		public const string t = "t";
		public const string u = "u";
		public const string v = "v";
		public const string w = "w";
		public const string x = "x";
		public const string y = "y";
		public const string z = "z";
		// List Separator
		public const string FFF9 = "\xFFF9"; // INTERLINEAR ANNOTATION ANCHOR
		public const string FFFA = "\xFFFA"; // INTERLINEAR ANNOTATION SEPARATOR
		public const string FFFB = "\xFFFB"; // INTERLINEAR ANNOTATION TERMINATOR

		// Special symbols
		public const string Micro = "\x00B5"; // MICRO SIGN
		public const string Delta = "\x0394"; // GREEK CAPITAL LETTER DELTA
		public const string Sum = "\x2211"; // N-ARY SUMMATION

		// Other String Constants
		public const string Empty = "";
		public const string NULL = "NULL";
		public const string Null = "(Null)";
		public const string null_ = "null";
		public const string true_ = "true";
		public const string false_ = "false";
		public const string TRUE = "TRUE";
		public const string FALSE = "FALSE";
		public const string OK = "OK";
		public const string FAIL = "FAIL";
		public const string Error = "Error";
		public const string Timeout = "Timeout";
		public const string NotFound = "NotFound";
		public const string Modified = "Modified";
		public const string BO = "BO_";
		public const string RO = "RO_";
		public const string ClSp = ": ";
		public const string DtSp = ". ";
		public const string SpDt = " .";
		public const string CmSp = ", ";
		public const string CmNl = ",\n";
		public const string ScSp = "; ";
		public const string ScNl = ";\n";
		public const string SpGtSp = " > ";
		public const string SpLtSp = " < ";
		public const string SpExSp = " ! ";
		public const string SpFsSp = " / ";
		public const string TokenPrefix = "  #!# ";
		public const string _QM_ = "_?_";
		public const string QQQ = "???";
		public const string FsGt = "/>";
		public const string CrLf = "\r\n";
		public const string EmptyQuotes1 = "''";
		public const string EmptyQuotes2 = "\"\"";
		public const string N0_2 = "00";
		public const string N0_3 = "000";
		public const string N0_4 = "0000";
		public const string N0_5 = "00000";
		public const string N0_6 = "000000";
		public const string N0_7 = "0000000";
		public const string N0_8 = "00000000";
		public const string N0_9 = "000000000";
		public const string F22 = "F22";
		public const string EXEC = "EXEC";
		public const string EXECSP = "EXEC ";
		public const string SP4 = "    ";
		public const string SP8 = "        ";
		public const string SP64 = "                                                                ";
		public const string Dt3 = "...";
		public const string oX = "0x";
		public const string op = "op";
		public const string on = "on";
		public const string off = "off";
		public const string TS = "TS";
		public const string chkd = "checked";
		public const string X6 = "X6";
		public const string CheckInPrefix = "_~_";
		public const string _of_ = " of ";
		public const string _in_ = " in ";
		public const string BSBS = "\\\\";
		public const string UTC = "UTC";
		public const string ID = "ID";
		// often used in logging
		public const string Init = "Init";
		public const string Exit = "Exit";
		public const string Initialize = "Initialize";
		public const string CobasoftEventTime = "CobasoftEventTime";
		// ASP.NET & Web
		public const string ASPXAUTH = ".ASPXAUTH";
		public const string ASPNET_SessionId = "ASP.NET_SessionId";
		public const string HTTP_USER_AGENT = "HTTP_USER_AGENT";
		// File Extensions
		public const string cs = "cs";
		public const string sql = "sql";
		// Object Model
		public const string Picklist = "Picklist";
		public const string DisplayKey = "DisplayKey";
		public const string DisplayValue = "DisplayValue";
		public const string DisplayTitle = "DisplayTitle";
		// HTML
		public const string htmlSP = "&nbsp;";
		public const string htmlBR = "<br/>";
		// Monetary
		public const string moneyEuro = "€";
		// Arrays
		public readonly static string[] Spaces = new string[] { "", " ", "  ", "   ", "    ", "     ", "      ", "       ", "        ", "         ", "          " };
		public readonly static string[] Tabs = new string[] { "", "\t", "\t\t", "\t\t\t", "\t\t\t\t", "\t\t\t\t\t", "\t\t\t\t\t\t", "\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t", "\t\t\t\t\t\t\t\t\t\t" };
	}
	//
	// Separators and other string arrays
	public static partial class CA {
		public static readonly char[] AM = new char[] { CC.AM };
		public static readonly char[] AT = new char[] { CC.AT };
		public static readonly char[] BS = new char[] { CC.BS };
		public static readonly char[] C1 = new char[] { CC.C1 };
		public static readonly char[] C2 = new char[] { CC.C2 };
		public static readonly char[] CL = new char[] { CC.CL };
		public static readonly char[] CM = new char[] { CC.CM };
		public static readonly char[] EQ = new char[] { CC.EQ };
		public static readonly char[] FS = new char[] { CC.FS };
		public static readonly char[] SC = new char[] { CC.SC };
		public static readonly char[] SP = new char[] { CC.SP };
		public static readonly char[] TB = new char[] { CC.TB };
		public static readonly char[] UL = new char[] { CC.UL };
		public static readonly char[] CmSp = new char[] { CC.CM, CC.SP };
		public static readonly char[] CrLf = new char[] { CC.CR, CC.LF };
		public static readonly char[] ScCrLf = new char[] { CC.SC, CC.CR, CC.LF };
		public static readonly char[] ScCrLfSp = new char[] { CC.SC, CC.CR, CC.LF, CC.SP };
		public static readonly char[] CmCrLf = new char[] { CC.CM, CC.CR, CC.LF };
		public static readonly char[] CmCrLfSp = new char[] { CC.CM, CC.CR, CC.LF, CC.SP };
		public static readonly char[] FsBs = new char[] { CC.FS, CC.BS };
		public static readonly char[] FsBsDt = new char[] { CC.FS, CC.BS, CC.DT };
		public static readonly char[] FsBsDtUl = new char[] { CC.FS, CC.BS, CC.DT, CC.UL };
		public static readonly char[] Parentheses = new char[] { CC.P1, CC.P2 };
		public static readonly char[] SpTb = new char[] { CC.SP, CC.TB };
		public static readonly char[] SpTbCrLf = new char[] { CC.SP, CC.TB, CC.CR, CC.LF };
		public static readonly char[] ContactPickSeparators = new char[] { CC.AM, CC.DL, CC.EQ, CC.SP };
		public static readonly char[] EmailSeparators = new char[] { CC.CM, CC.SP, CC.SC };
		public static readonly char[] FilterSeparators = new char[] { CC.AM, CC.EQ, CC.SP, CC.TB };
		public static readonly char[] PicklistSeparators = new char[] { CC.AM, CC.EQ };
		public static readonly char[] SearchDelimiters = new char[] { CC.C1, CC.C2 };
		public static readonly char[] SearchSeparators = new char[] { CC.SP };
		public static readonly char[] SearchSeparatorsOld = new char[] { CC.CM, CC.SP, CC.SC, CC.HY, CC.FS, CC.DT };
		public static readonly char[] TokenPatternSeparators = new char[] { CC.B1, CC.C2 };
		// List Separator
		public static readonly char[] FFF9 = new char[] { CC.FFF9 }; // INTERLINEAR ANNOTATION ANCHOR
		public static readonly char[] FFFA = new char[] { CC.FFFA }; // INTERLINEAR ANNOTATION SEPARATOR
		public static readonly char[] FFFB = new char[] { CC.FFFB }; // INTERLINEAR ANNOTATION TERMINATOR
	}
	//
	// https://en.wikipedia.org/wiki/Balanced_ternary
	///<summary>Three-state value.</summary>
	public enum Tri : sbyte {
		Yes = 1, No = 0, Uncertain = -1,
		First = 1, Current = 0, Last = -1,
		Append = 1, Keep = 0, Overwrite = -1, Before = -1,
		Continue = 1, Retry = 0, Skip = 0, Abort = -1,
		Positive = 1, Zero = 0, Negative = -1,
		Bigger = 1, Equal = 0, Smaller = -1,
		Ascending = 1, Unsorted = 0, Descending = -1,
		On = 1, Off = 0, All = -1
	};
	//
	//
	///<summary> Contains extension methods for Tri. </summary>
	public static class TriEx {
		///<summary>Depending on the value of triVal return one of the three parameters.</summary>
		public static T Choose<T>( this Tri triVal, T yes, T no, T uncertain ) {
			switch ( triVal ) {
				case Tri.Yes: return yes;
				case Tri.No: return no;
			}
			return uncertain;
		}
		//
	}
}