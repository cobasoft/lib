﻿using System;
//
//
namespace Cobasoft {
	//
	//
	//
	///<summary>Extension methods for DateTime.</summary>
	public static partial class DateTimeEx {
		//
		//
		///<summary>Format date as version in the format: Y.M.DDHH, where baseYear is subtracted from the given year.</summary>
		public static string FormatVersion( DateTime from, int baseYear = 2016 ) {
			var version = from.ToString( "M.ddHH" );
			return ( from.Year - baseYear ).ToString() + "." + version;
		}
		//
		///<summary>Test if it is the MinValue.</summary>
		public static bool IsEmpty( this DateTime dt ) {
			return DateTime.MinValue.Year == dt.Year;
		}
		///<summary>Test if it is the MinValue.</summary>
		public static bool IsEmpty( this DateTimeOffset dt ) {
			return DateTimeOffset.MinValue.Year == dt.Year;
		}
		///<summary>Test if it is NOT the MinValue.</summary>
		public static bool IsFilled( this DateTime dt ) {
			return DateTime.MinValue.Year != dt.Year;
		}
		///<summary>Test if it is NOT the MinValue.</summary>
		public static bool IsFilled( this DateTimeOffset dt ) {
			return DateTimeOffset.MinValue.Year != dt.Year;
		}
		///<summary>Get the milliseconds delta.</summary>
		public static long Delta( this DateTime start, DateTime finish ) {
			return ( finish.Ticks - start.Ticks ) / 10000L;
		}
		///<summary>Format the date time value with the "u" option.</summary>
		public static string ToUniversal( this DateTime v ) {
			return v.ToString( CS.u );
		}
		///<summary>Compute absolute day number ( Year * 1000 + DayOfYear ).</summary>
		public static long AbsoluteDay( this DateTime v ) {
			if ( v.IsEmpty() ) return 0;
			return (long) v.Year * 1000L + v.DayOfYear;
		}
		///<summary>Compute absolute minute ( Hour * 100 + Minute ).</summary>
		public static int DayMinutes( this DateTime v ) {
			if ( v.IsEmpty() ) return -1;
			return v.Hour * 100 + v.Minute;
		}
		///<summary>Compute absolute second ( Hour * 10000 + Minute * 100 + Second ).</summary>
		public static int DaySeconds( this DateTime v ) {
			if ( v.IsEmpty() ) return -1;
			return v.Hour * 10000 + v.Minute * 100 + v.Second;
		}
		//
		// FAST FORMATTING
		//
		///<summary>Format the date time value as a parsable ISO format: "2008-01-11T16:07:12Z".</summary>
		public static string ISO( this DateTime dt ) {
			var Y = dt.Year;
			var M = dt.Month;
			var D = dt.Day;
			var h = dt.Hour;
			var m = dt.Minute;
			var s = dt.Second;
			var ca = new char[] {
				(char) ( Y / 1000 % 10 + '0' ),
				(char) ( Y / 100 % 10 + '0' ),
				(char) ( Y / 10 % 10 + '0' ),
				(char) ( Y % 10 + '0' ),
				'-',
				(char) ( M / 10 + '0' ),
				(char) ( M % 10 + '0' ),
				'-',
				(char) ( D / 10 + '0' ),
				(char) ( D % 10 + '0' ),
				'T',
				(char) ( h / 10 + '0' ),
				(char) ( h % 10 + '0' ),
				':',
				(char) ( m / 10 + '0' ),
				(char) ( m % 10 + '0' ),
				':',
				(char) ( s / 10 + '0' ),
				(char) ( s % 10 + '0' ),
				'Z',
			};
			return new string( ca );
		}
		///<summary>Return a sortable date and time (with seconds) with only one separator.</summary>
		public static string YmdHms( this DateTime dt ) {
			// F 0000006579 : ConvertLoop1   DateTimeTest.cs.1
			// F 0000003973 : ConvertLoop2   DateTimeTest.cs.2
			var Y = dt.Year;
			var M = dt.Month;
			var D = dt.Day;
			var h = dt.Hour;
			var m = dt.Minute;
			var s = dt.Second;
			var ca = new char[] {
				(char) ( Y / 1000 + '0' ),
				(char) ( Y / 100 % 10 + '0' ),
				(char) ( Y / 10 % 10 + '0' ),
				(char) ( Y % 10 + '0' ),
				(char) ( M / 10 + '0' ),
				(char) ( M % 10 + '0' ),
				(char) ( D / 10 + '0' ),
				(char) ( D % 10 + '0' ),
				CC.HY,
				(char) ( h / 10 + '0' ),
				(char) ( h % 10 + '0' ),
				(char) ( m / 10 + '0' ),
				(char) ( m % 10 + '0' ),
				(char) ( s / 10 + '0' ),
				(char) ( s % 10 + '0' ),
			};
			return new string( ca );
		}
		///<summary>Return a sortable date and time with only one separator.</summary>
		public static string YmdHm( this DateTime dt ) {
			var Y = dt.Year;
			var M = dt.Month;
			var D = dt.Day;
			var h = dt.Hour;
			var m = dt.Minute;
			var ca = new char[] {
				(char) ( Y / 1000 + '0' ),
				(char) ( Y / 100 % 10 + '0' ),
				(char) ( Y / 10 % 10 + '0' ),
				(char) ( Y % 10 + '0' ),
				(char) ( M / 10 + '0' ),
				(char) ( M % 10 + '0' ),
				(char) ( D / 10 + '0' ),
				(char) ( D % 10 + '0' ),
				CC.HY,
				(char) ( h / 10 + '0' ),
				(char) ( h % 10 + '0' ),
				(char) ( m / 10 + '0' ),
				(char) ( m % 10 + '0' ),
			};
			return new string( ca );
		}
		//
		//
		public static string ToLocalString( this DateTime dt, bool iso ) {
			return iso ? dt.ISO() : dt.ToString();
		}
	}
}
