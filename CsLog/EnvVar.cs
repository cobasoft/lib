﻿// License and Copyright: see License.cs
using System;
//
//
//
namespace Cobasoft {
	//
	///<summary>Comfortable and type save environment variables readonly access.</summary>
	public class EnvVar {
		//
		//
		static EnvVar() {
		}
		//
		///<summary>Most basic function for retrieving values from environment variables.</summary>
		public static string Get( string name, string value = null ) {
			var result = Environment.GetEnvironmentVariable( name );
			if ( result is null ) {
				result = value;
			}
			return result;
		}
		//
		///<summary>Get array of semicolon-separated strings. May return null.</summary>
		public static string[] GetStrings( string name, string value = null ) {
			var s = Get( name, value );
			if ( string.IsNullOrEmpty( s ) ) return null;
			var parts = s.Split( CA.SC, StringSplitOptions.RemoveEmptyEntries );
			return parts;
		}
		////
		/////<summary>Get an array of integer values from semicolon-separated strings. Only valid numbers are returned.</summary>
		//public static int[] GetIntegers( string name, string value = null ) {
		//	var sa = GetStrings( name, value );
		//	if ( sa is null ) return null;
		//	var l = sa.Length;
		//	if ( 0 == l ) return null;
		//	var list = new List<int>();
		//	for ( int i = 0; i < l; i++ ) {
		//		if ( int.TryParse( sa[ i ], out var o ) ) {
		//			Log.Assert( 0 < o );
		//			list.Add( o );
		//		}
		//	}
		//	if ( 0 == list.Count ) return null;
		//	return list.ToArray();
		//}
		////
		/////<summary>Get an array of decimal values from semicolon-separated strings. Only valid numbers are returned.</summary>
		//public static decimal[] GetDecimals( string name, string value = null ) {
		//	var sa = GetStrings( name, value );
		//	if ( sa is null ) return null;
		//	var l = sa.Length;
		//	if ( 0 == l ) return null;
		//	var list = new List<decimal>();
		//	for ( int i = 0; i < l; i++ ) {
		//		if ( decimal.TryParse( sa[ i ], out var o ) ) {
		//			Log.Assert( 0 < o );
		//			list.Add( o );
		//		}
		//	}
		//	if ( 0 == list.Count ) return null;
		//	return list.ToArray();
		//}
		//
		//
		public static bool GetBool( string name ) { return Config.ParseBool( Get( name ) ); }
		public static ulong GetULong( string name ) { return Config.ParseULong( Get( name ) ); }
		public static long GetLong( string name ) { return Config.ParseLong( Get( name ) ); }
		public static uint GetUInt( string name ) { return Config.ParseUInt( Get( name ) ); }
		public static int GetInt( string name ) { return Config.ParseInt( Get( name ) ); }
		public static double GetDouble( string name ) { return Config.ParseDouble( Get( name ) ); }
		public static float GetFloat( string name ) { return Config.ParseFloat( Get( name ) ); }
		public static decimal GetDecimal( string name ) { return Config.ParseDecimal( Get( name ) ); }
		//
		//
		public static DateTime GetDateTime( string name, string value = null ) {
			return Config.ParseDateTime( Get( name, value ) );
		}
		public static bool GetBool( string name, bool value ) { return Config.ParseBool( Get( name, value ? CS.N1 : CS.N0 ) ); }
		public static ulong GetULong( string name, ulong value ) { return Config.ParseULong( Get( name, value.ToString() ) ); }
		public static long GetLong( string name, long value ) { return Config.ParseLong( Get( name, value.ToString() ) ); }
		public static uint GetUInt( string name, uint value ) { return Config.ParseUInt( Get( name, value.ToString() ) ); }
		public static int GetInt( string name, int value ) { return Config.ParseInt( Get( name, value.ToString() ) ); }
		public static double GetDouble( string name, double value ) { return Config.ParseDouble( Get( name, value.ToString() ) ); }
		public static float GetFloat( string name, float value ) { return Config.ParseFloat( Get( name, value.ToString() ) ); }
		public static decimal GetDecimal( string name, decimal value ) { return Config.ParseDecimal( Get( name, value.ToString() ) ); }
		public static DateTime GetDateTime( string name, DateTime value ) { return GetDateTime( name, value.ISO() ); }
		//
		public static void Get( string name, ref string value ) { value = Get( name ); }
		public static void Get( string name, ref bool value ) { value = GetBool( name ); }
		public static void Get( string name, ref ulong value ) { value = GetULong( name ); }
		public static void Get( string name, ref long value ) { value = GetLong( name ); }
		public static void Get( string name, ref uint value ) { value = GetUInt( name ); }
		public static void Get( string name, ref int value ) { value = GetInt( name ); }
		public static void Get( string name, ref double value ) { value = GetDouble( name ); }
		public static void Get( string name, ref float value ) { value = GetFloat( name ); }
		public static void Get( string name, ref decimal value ) { value = GetDecimal( name ); }
		public static void Get( string name, ref DateTime value ) { value = GetDateTime( name ); }
		public static void Get( string name, ref string[] value ) { value = GetStrings( name ); }
		//public static void Get( string name, ref int[] value ) { value = GetIntegers( name ); }
		//
		//
		//
		///<summary>Replace all environment variables within a string.</summary>
		public static string Replace( string v ) {
			if ( Log.Assert( v is not null ) ) {
				var p1 = v.IndexOf( CC.C1 );
				if ( -1 < p1 ) {
					var p2 = v.IndexOf( CC.C2 );
					if ( p1 < p2 ) {
						// Split into parts. Beginning of every part is the variable name
						var parts = v.Split( CA.C1, StringSplitOptions.RemoveEmptyEntries );
						if ( parts is not null ) {
							var I = parts.Length;
							for ( int i = 0; i < I; i++ ) {
								// Split part into name and remainder
								var part = parts[ i ];
								var sub = part.Split( CA.C2, StringSplitOptions.RemoveEmptyEntries );
								if ( sub is not null && sub.Length > 0 ) {
									var name = sub[ 0 ];
									var env = Get( name );
									if ( env is not null ) {
										// Directly replace with its environment variable content
										sub[ 0 ] = env;
									}
								}
								// Replace the existing array position
								parts[ i ] = String.Join( CS.Empty, sub );
							}
							// Replace the complete value
							v = String.Join( CS.Empty, parts );
						}
					}
				}
			}
			return v;
		}
		//
	}
}
