﻿// License and Copyright: see License.cs
using System;
//
namespace Cobasoft {
	//
	///<summary>Cryptography utilities.</summary>
	public static class HexEx {
		//
		// TASK: Improved performance, write unit tests. BTW: base64 encoding is more efficient.
		///<summary>Convert a byte array into a readable string, using hexadecimal encoding.</summary>
		public static string ToHex( byte[] bytes ) {
			if ( bytes is null || bytes.Length == 0 ) return null;
			string s = ""; string t;
			for ( int idx = 0; idx < bytes.Length; idx++ ) {
				t = Convert.ToString( bytes[ idx ], 16 );
				if ( t.Length < 2 ) t = "0" + t;
				s += t;
			}
			return s;
		}
		//
		///<summary>Convert a string of hexadecimal digits into a byte array.</summary>
		public static byte[] FromHex( string message ) {
			if ( message is null || message.Length == 0 ) return null;
			byte[] s = new byte[ message.Length / 2 ];
			for ( int idx = 0; idx < message.Length; idx += 2 ) {
				s[ idx / 2 ] = Convert.ToByte( message.Substring( idx, 2 ), 16 );
			}
			return s;
		}
		public static char ToHex( byte b ) {
			if ( b < 0 || b > 15 ) throw new ArgumentException();
			return b < 10 ? (char) ( b + 48 ) : (char) ( b + 55 );
		}
		//
		public static string ToHex( long n, int len ) {
			char[] ch = new char[ len-- ]; byte b;
			for ( int i = len; i >= 0; i-- ) {
				b = (byte) ( (uint) ( n >> 4 * i ) & 15 );
				ch[ len - i ] = ToHex( b );
			}
			return new string( ch );
		}
	}
}
