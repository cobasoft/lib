﻿using System;
using System.Threading;

namespace Cobasoft {
	//
	///<summary>Utility class for locking. Meant to be used within 'using'.</summary>
	public class Lock : IDisposable {
		//
		public enum LockType { Reader, Writer, Upgradable };
		//
		protected LockType _lt;
		protected ReaderWriterLockSlim _rwl;
		//
		public Lock( ReaderWriterLockSlim rwl, LockType lt = LockType.Reader, int msec = 1000 ) {
			//#pragma warning disable 0618
			bool ok = false;
			switch ( lt ) {
				case LockType.Reader: ok = rwl.TryEnterReadLock( msec ); break;
				case LockType.Writer: ok = rwl.TryEnterWriteLock( msec ); break;
				case LockType.Upgradable: ok = rwl.TryEnterUpgradeableReadLock( msec ); break;
			}
			if ( ok ) {
				_lt = lt;
				_rwl = rwl;
			} else {
				throw new TimeoutException( "Cannot acquire " + lt.ToString() + " lock" );
			}
		}
		public void Dispose() {
			switch ( _lt ) {
				case LockType.Reader: _rwl.ExitReadLock(); break;
				case LockType.Writer: _rwl.ExitWriteLock(); break;
				case LockType.Upgradable: _rwl.ExitUpgradeableReadLock(); break;
			}
		}
	}
}
