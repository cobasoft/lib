// License and Copyright: see License.cs
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
//
namespace Cobasoft {
	//
	///<summary>Static class for application logging and tracing. This class is meant to be very fast, stable, resilient and memory efficient.</summary>
	[DebuggerStepThrough]
	public static class Log {
		//
		private readonly static object _Lock = new object();
		//
		//
		///<summary>Get TickCount as unsigned integer.</summary>
		public static uint TickCount {
			get {
				return (uint) System.Environment.TickCount;
			}
		}
		//
		//
		///<summary>If Linear is turned on, CR (\r) is replaced by this character.</summary>
		public static readonly char AltCarriageReturn = '\x16c7';
		//
		///<summary>If Linear is turned on, LF (\n) is replaced by this character.</summary>
		public static readonly char AltLineFeed = '\x16b4';
		//
		///<summary>
		/// Scope code characters. 
		/// Scopes determine, what is written to the Log. 
		/// For example, Log.Data() is only written if the 'D' flag is on.
		/// Scopes are specified as strings in configuration, but are kept internally as bit flags within an integer value.
		/// If scopes are turned off, this also prevents argument formatting for related methods, so it speeds up logging.
		///</summary>
		public static readonly string ScopeCodes = "-XEWIMVFGDSTOL-";
		//
		///<summary>Scope code strings. Meant to be indexed by Log.Type, like SCOPECODES[ (int) <see cref="Log.Type.Exception"/> ].</summary>
		public static readonly string[] SCOPECODES = new string[] { "-", "X", "E", "W", "I", "M", "V", "F", "G", "D", "S", "T", "O", "L", "-" };
		//
		//
		///<summary>Log types. Each is also represented by a character shortcut, which can be used for filtering output <see cref="ScopeCodes"/>.</summary>
		public enum Type : byte {
			None = 0,
			Exception,
			Error,
			Warning,
			Info,
			Memory,
			Verbose,
			Entry,
			Exit,
			Data,
			ShowMessage,
			Time,
			Success,
			Literal,
			Invalid
		};
		//
		// Flags
		private static bool _FlushOn;
		private static bool _ToFile;
		private static bool _ToConsole;
		private static bool _ToTrace;
		private static bool _ToListener;
		private static bool _DoBreak;
		private static bool _DoHalt;
		private static bool _Linear;
		// Scope
		private static uint _Scope;
		private static uint _CallerScope;
		// Name
		private static string _FilePath;
		private static string _FileName;
		// TextWriter
		private static TextWriter _Writer;
		// Statistics
		private static Process _Process;
		private static long _WorkingSet;
		//private static long _VirtualMemory;
		private static long _HandleCount;
		// Cycle
		private static long _Size;
		private static long _FileSize;
		private static long _FileAge;
		// Additional Output
		private static Listener _Listener;
		// Time
		private static Hashtable _Times;
		// Information
		private static string _assemblyLocation;
		private static string _codeBase;
		private static string _domainBase;
		private static string _currentDirectory;
		//
		// 
		//
		///<summary>Enable or disable output options.</summary>
		public static void Enable( bool file = true, bool console = true, bool trace = true, bool listener = true ) {
			ToFile = file;
			ToConsole = console;
			ToTrace = trace;
			ToListener = listener;
		}
		//
		//
		///<summary>Get scope flags from a string like "XEWI".</summary>
		///<remarks>
		/// If the scope is missing, "XEWI" is the default.
		/// If only "*" it is specified, maximum logging is enabled.
		///</remarks>
		public static uint GetScope( string scope ) {
			if ( String.IsNullOrEmpty( scope ) ) return 0;
			if ( "*" == scope ) return GetScope( ScopeCodes );
			uint flags = 0;
			for ( int i = 0; i < scope.Length; i++ ) {
				var p = ScopeCodes.IndexOf( scope[ i ] ) - 1;
				if ( p > -1 ) {
					flags |= (uint) ( 1u << p );
				}
			}
			return flags;
		}
		//
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Test if the bit is set.</summary>
		public static bool GetBit( uint value, int pos ) {
			return 0 != ( value & ( 1 << pos ) );
		}
		//
		//
		///<summary>Get scope string from scope flags.</summary>
		public static string GetScope( uint flags ) {
			var sb = new StringBuilder();
			var I = SCOPECODES.Length;
			for ( int i = 0; i < I; i++ ) {
				var bit = GetBit( flags, i );
				if ( bit ) {
					sb.Append( SCOPECODES[ 1 + i ] );
				}
			}
			return sb.ToString();
		}
		//
		//
		///<summary>Get current scope string.</summary>
		public static string GetScope() {
			return Log.GetScope( Log.Scope );
		}
		//
		//
		///<summary>Get current caller scope string.</summary>
		public static string GetCallerScope() {
			return Log.GetScope( Log.CallerScope );
		}
		//
		//
		///<summary>Use a string like "XEWI" to set _Scope. Only traces, where Type is matching Scope are written to output.</summary>
		public static ulong SetScope( string scope ) {
			return Scope = GetScope( scope );
		}
		//
		//
		///<summary>Enable a <see cref="Log.Type"/> scope.</summary>
		public static ulong SetScope( Type type ) {
			return _Scope = _Scope | ( 1u << (int) type );
		}
		//
		//
		///<summary>Disabler a <see cref="Log.Type"/> scope.</summary>
		public static ulong ClearScope( Type type ) {
			return _Scope = _Scope & ~( 1u << (int) type );
		}
		//
		//
		///<summary>Enable a <see cref="Log.Type"/> caller scope.</summary>
		public static ulong SetCallerScope( Type type ) {
			return _CallerScope = _CallerScope | ( 1u << (int) type );
		}
		//
		//
		///<summary>Disabler a <see cref="Log.Type"/> caller scope.</summary>
		public static ulong ClearCallerScope( Type type ) {
			return _CallerScope = _CallerScope & ~( 1u << (int) type );
		}
		//
		///<summary>Use a string like "XEWI" to set _CallerScope. Caller information is only written to output, if the current Type is matching the Scope.</summary>
		public static ulong SetCallerScope( string scope ) {
			return CallerScope = GetScope( scope );
		}
		//
		///<summary>Reset scope.</summary>
		public static void ResetScope() {
			Scope = uint.MaxValue;
		}
		//
		///<summary>Reset caller scope.</summary>
		public static void ResetCallerScope() {
			CallerScope = uint.MaxValue;
		}
		//
		///<summary>Get Type from a character code. Might return -1, if the code is not known.</summary>
		public static int GetType( char code ) {
			return ScopeCodes.IndexOf( code );
		}
		//
		///<summary>Convert Type into a scope code characters.</summary>
		public static char GetCode( Type type ) {
			var i = (int) type;
			return ScopeCodes[ i ];
		}
		//
		//
		///<summary>Assembly path information.</summary>
		private static void GetAssemblyPaths() {
			// Executing Assembly
			var assembly = Assembly.GetExecutingAssembly();
			if ( assembly is not null ) {
				// For desktop like: "Z:\\src\\lib\\CsTestLog\\bin\\Debug\\net6.0\\Cobasoft.Log.dll"
				// For ASP.NET like: "C:\\Users\\...\\AppData\\Local\\Temp\\1\\Temporary ASP.NET Files\\...\\Cobasoft.Log.dll"
				if ( !String.IsNullOrEmpty( assembly.Location ) ) {
					_assemblyLocation = Path.GetDirectoryName( assembly.Location ).Trim( CC.BS );
				}
			}
			// Code Base
			var entry = Assembly.GetEntryAssembly();
			// For ASP.NET: null
			if ( entry is not null ) {
				// For desktop like: "Z:\\src\\lib\\CsTestLog\\bin\\Debug\\net6.0\\CsTestLog.dll"
				if ( !String.IsNullOrEmpty( entry.Location ) ) {
					_codeBase = Path.GetDirectoryName( entry.Location ).Trim( CC.BS );
				}
			}
			// Domain Base
			var domain = AppDomain.CurrentDomain;
			if ( domain is not null ) {
				// For desktop: null
				// For ASP.NET like: "C:\inetpub\wwwroot\CS\bin"
				if ( !String.IsNullOrEmpty( domain.RelativeSearchPath ) ) {
					_domainBase = domain.RelativeSearchPath.Trim( CC.BS );
				}
			}
			// Current Directory
			_currentDirectory = Directory.GetCurrentDirectory().Trim( CC.BS );
		}
		//
		///<summary>Initialize</summary>
		public static void Init() {
			try {
				lock ( _Lock ) {
					// Configuration
#if CS_NET_CORE
					var prefix = "Cobasoft:Log:";
#else
					var prefix = "Log";
#endif
					_CallerScope = _Scope = 0;
					// Earliest possible initialization from environment variable
					_ToTrace = EnvVar.GetBool( "CsLogTrace", false );
					// Try get initialization from configuration
					_Scope = GetScope( Config.Get( prefix + "Scope", ScopeCodes ) );
					_CallerScope = GetScope( Config.Get( prefix + "CallerScope", ScopeCodes ) );
					_FlushOn = Config.GetBool( prefix + "Flush", false );
					_ToFile = Config.GetBool( prefix + "File", false );
					_ToTrace = Config.GetBool( prefix + "Trace", false );
					_ToListener = Config.GetBool( prefix + "Listener", false );
					_ToConsole = Config.GetBool( prefix + "Console", false );
					_DoBreak = Config.GetBool( prefix + "Break", false );
					_DoHalt = Config.GetBool( prefix + "Halt", false );
					_Linear = Config.GetBool( prefix + "Linear", false );
					_FileSize = Config.GetLong( prefix + "FileSize", 20000000 );
					_FileAge = Config.GetLong( prefix + "FileAge", 168 );
					_FilePath = Config.Get( prefix + "Path", "/log/" );
					// Try update initialization from environment variables.
					// Environment variables always win!
					_Scope = GetScope( EnvVar.Get( "CsLogScope", GetScope( _Scope ) ) );
					_CallerScope = GetScope( EnvVar.Get( "CsLogCallerScope", GetScope( _CallerScope ) ) );
					_FlushOn = EnvVar.GetBool( "CsLogFlush", _FlushOn );
					_ToFile = EnvVar.GetBool( "CsLogFile", _ToFile );
					_ToTrace = EnvVar.GetBool( "CsLogTrace", _ToTrace );
					_ToListener = EnvVar.GetBool( "CsLogListener", _ToListener );
					_ToConsole = EnvVar.GetBool( "CsLogConsole", _ToConsole );
					_DoBreak = EnvVar.GetBool( "CsLogBreak", _DoBreak );
					_DoHalt = EnvVar.GetBool( "CsLogHalt", _DoHalt );
					_Linear = EnvVar.GetBool( "CsLogLinear", _Linear );
					_FileSize = EnvVar.GetLong( "CsLogFileSize", _FileSize );
					_FileAge = EnvVar.GetLong( "CsLogFileAge", _FileAge );
					_FilePath = EnvVar.Get( "CsLogPath", _FilePath );
					_FilePath = EnvVar.Replace( _FilePath );
					//
					_Process = Process.GetCurrentProcess();
					GetAssemblyPaths();
					Open();
					Header();
					CheckLogBreak();
					var types = Enum.GetNames( typeof( Type ) ).Length;
					Assert( SCOPECODES.Length <= 32 ); // uint
					Assert( SCOPECODES.Length == types );
					Assert( SCOPECODES.Length == ScopeCodes.Length );
				}
			} catch ( Exception ex ) {
				Trace.WriteLine( "EXCEPTION in log static constructor: " + ex.ToString() );
			}
		}
		//
		// Static Constructor. Settings are read from configuration and environment.
		static Log() {
			Init();
		}
		//
		///<summary>
		/// Get or set the FlushOn flag.
		/// If this is turned on, every line is immediately written to the log file.
		/// That makes logging much slower, but allows to write trace before program crashes.
		///</summary>
		public static bool IsFlush {
			get {
				return _FlushOn;
			}
			set {
				lock ( _Lock ) {
					_FlushOn = value;
				}
			}
		}
		///<summary>Get or set the ToFile flag. If this is turned off, no log file is written.</summary>
		public static bool ToFile {
			get {
				return _ToFile;
			}
			set {
				lock ( _Lock ) {
					if ( _ToFile && !value ) {
						// Close logging file, if it's open.
						Close();
					}
					var must_open = !_ToFile && value;
					//
					_ToFile = value;
					//
					if ( must_open ) {
						// Open logging file, if it's closed.
						Open();
					}
				}
			}
		}
		///<summary>Get or set ToConsole flag. If this is turned off, no System.Console trace is written.</summary>
		public static bool ToConsole {
			get {
				return _ToConsole;
			}
			set {
				lock ( _Lock ) {
					_ToConsole = value;
				}
			}
		}
		///<summary>Get or set the ToTrace flag. If this is turned off, no System.Diagnostics.Trace is written.</summary>
		public static bool ToTrace {
			get {
				return _ToTrace;
			}
			set {
				lock ( _Lock ) {
					_ToTrace = value;
				}
			}
		}
		///<summary>Get or set the ToListener flag. If this is turned off, no custom listener is being called.</summary>
		public static bool ToListener {
			get {
				return _ToListener;
			}
			set {
				lock ( _Lock ) {
					_ToListener = value;
				}
			}
		}
		///<summary>Gets the LogBreak configuration value.</summary>
		public static bool IsBreak {
			get {
				return _DoBreak;
			}
		}
		///<summary>
		/// Get or set the DoHalt flag. 
		/// If this is turned on, every warning, error or exception breaks 
		/// into the debugger, if a debugger is attached.
		/// To turn this off during runtime, while being breaked in a debugger, 
		/// you can run one of the following statements in the Immediate window:
		/// Log.NoHalt() or
		/// Log.IsHalt = false;
		/// </summary>
		public static bool IsHalt {
			get {
				return _DoHalt;
			}
			set {
				lock ( _Lock ) {
					_DoHalt = value;
				}
			}
		}
		///<summary>
		/// Get or set the Linear flag. 
		/// If this is turned on, line feed (LF,\n) and character return (CR,\r) characters 
		/// in messages are replaced by ᚴ (U+16B4, runic kaun) and ᛇ (U+16C7, runic eiwaz)
		/// to create single-line output.
		/// LogVw can display these as linebreaks.
		/// </summary>
		public static bool Linear {
			get {
				return _Linear;
			}
			set {
				lock ( _Lock ) {
					_Linear = value;
				}
			}
		}
		//
		///<summary>Get or set the scope value. This is a bit flag, with one bit for each value in <see cref="ScopeCodes"/>.</summary>
		public static uint Scope {
			get {
				return _Scope;
			}
			set {
				lock ( _Lock ) {
					_Scope = value;
				}
			}
		}
		//
		///<summary>Get all set the caller scope value. This is a bit flag, with one bit for each value in <see cref="ScopeCodes"/>.</summary>
		public static uint CallerScope {
			get {
				return _CallerScope;
			}
			set {
				lock ( _Lock ) {
					_CallerScope = value;
				}
			}
		}
		//
		///<summary>Get or set the maximum file size. If the value is bigger than 0, a new log file will be opened, when the size is exceeded.</summary>
		public static long FileSize {
			get {
				return _FileSize;
			}
			set {
				lock ( _Lock ) {
					_FileSize = value;
				}
			}
		}
		//
		///<summary>Get or set the maximum log file age (in hours). If the value is bigger than 0, older log files will be deleted automatically.</summary>
		public static long FileAge {
			get {
				return _FileAge;
			}
			set {
				lock ( _Lock ) {
					_FileAge = value;
				}
			}
		}
		//
		///<summary>Get or set the current log file path. If it is modified, the old log file is closed and then reopened.</summary>
		public static string FilePath {
			get {
				return _FilePath;
			}
			set {
				lock ( _Lock ) {
					// Only if changed
					if ( 0 != String.Compare( _FilePath, value, StringComparison.InvariantCultureIgnoreCase ) ) {
						Close();
						_FilePath = value;
						Open();
					}
				}
			}
		}
		// Information
		//
		///<summary>Get current output file name.</summary>
		public static string FileName {
			get {
				lock ( _Lock ) {
					return _FileName;
				}
			}
		}
		///<summary>Get the assembly location path, without filename. For ASP.NET applications, this is in AppData...Temporary ASP.NET Files.</summary>
		public static string AssemblyLocation {
			get {
				return _assemblyLocation;
			}
		}
		///<summary>Get Assembly.GetEntryAssembly().Location, without filename. For ASP.NET applications, this is null.</summary>
		public static string CodeBase {
			get {
				return _codeBase;
			}
		}
		//
		///<summary>Get AppDomain.CurrentDomain.RelativeSearchPath. For desktop applications, this is null.</summary>
		public static string DomainBase {
			get {
				return _domainBase;
			}
		}
		//
		///<summary>Try to get DomainBase, but if that is null, return CodeBase.</summary>
		public static string AppBase {
			get {
				return _domainBase ?? _codeBase;
			}
		}
		//
		///<summary>Get the current directory at the time when Cobasoft.Log was initialized.</summary>
		public static string CurrentDirectory {
			get {
				return _currentDirectory;
			}
		}
		//
		//
		private static bool DeleteOldLogFile( FileInfo file ) {
			try {
				file.Delete();
				return true;
			} catch ( Exception ex ) {
				Trace.WriteLine( "EXCEPTION in DeleteOldLogFile: " + " file: " + file.FullName + ex.ToString() );
				FileAge = 0; // stop trying to delete files
			}
			return false;
		}
		//
		private static void CheckFileAge( DirectoryInfo folder ) {
			if ( 0 < _FileAge ) {
				var age = new TimeSpan( (int) _FileAge, 0, 0 );
				var limit = DateTime.UtcNow - age;
				var subset = folder.GetFiles( "*.log" );
				var list = new List<FileInfo>();
				foreach ( var fi in subset ) {
					if ( fi.LastWriteTimeUtc < limit ) {
						list.Add( fi );
					}
				}
				foreach ( var item in list ) {
					if ( !DeleteOldLogFile( item ) ) break;
				}
			}
		}
		//
		//
		//
		//
		///<summary>Try to find position of b within a, invariant culture, ignore case. Return -1 if not found.</summary>
		public static int IndexOfII( this string a, string b ) {
			if ( a is not null ) {
				if ( b is not null ) {
					return a.IndexOf( b, StringComparison.InvariantCultureIgnoreCase );
				}
			}
			return -1;
		}
		//
		//
		///<summary>Allow paths relative to assembly location.</summary>
		private static void FixPath() {
			// Must be called within lock()!
			if ( _FilePath is not null ) {
				var sa = new string[] {
					"{AssemblyLocation}", AssemblyLocation,
					"{AppBase}", AppBase,
					"{CodeBase}", CodeBase,
					"{DomainBase}", DomainBase,
					"{CurrentDirectory}", CurrentDirectory,
				};
				var p = _FilePath;
				var I = sa.Length;
				for ( int i = 0; i < I; i++ ) {
					var n = sa[ i++ ];
					if ( -1 < p.IndexOfII( n ) ) {
						var v = sa[ i ];
						p = p.Replace( n, v );
						break;
					}
				}
				_FilePath = p;
			}
		}
		//
		private static bool CheckPath() {
			// Must be called within lock()!
			if ( _FilePath is not null ) {
				FixPath();
				var path = new DirectoryInfo( _FilePath );
				if ( !path.Exists ) {
					path.Create();
				}
				if ( path.Exists ) {
					CheckFileAge( path );
					return true;
				}
			}
			return false;
		}
		//
		//
		///<summary>Close the file writer. This will stop tracing to file, until Open is being called. This message should normally not be called by applications.</summary>
		public static void Close() {
			try {
				lock ( _Lock ) {
					if ( _Writer is not null ) {
						_Writer.Close();
						_Writer = null;
					}
					_Size = 0;
				}
			} catch ( Exception ex ) {
				Trace.WriteLine( "Log.Close: " + ex.ToString() );
			}
		}
		//
		///<summary>Open the file writer. A new file name will be created automatically.</summary>
		private static void Open() {
			try {
				Close();
				if ( _ToFile ) {
					lock ( _Lock ) {
						if ( CheckPath() ) {
							var dt = DateTime.UtcNow.YmdHms();
							var id = HexEx.ToHex( _Process.Id, 8 );
							var sa = new string[] { dt, CS.HY, id, ".log" };
							var name = String.Join( CS.Empty, sa );
							_FileName = Path.Combine( _FilePath, name );
							var stream = File.Open( _FileName, FileMode.Create, FileAccess.Write, FileShare.Read );
							_Writer = new StreamWriter( stream );
							_Size = 0;
						}
					}
				}
			} catch ( Exception ex ) {
				Trace.WriteLine( "Log.Open failed: " + ex.ToString() );
			}
		}
		//
		///<summary>Write one message.
		/// If message is null, nothing is written.
		/// If Flush is on, stream is immediately flushed to disk.
		/// If file size is exceeded, a new file is created.
		///</summary>
		private static void WriteLocked( string message ) {
			lock ( _Lock ) {
				if ( _Writer is not null ) {
					if ( message is not null ) {
						_Writer.WriteLine( message );
						_Size += message.Length;
					}
					if ( _FlushOn ) {
						_Writer.Flush();
					}
				}
				if ( _FileSize > 0 && _Size > _FileSize ) {
					Close();
					Open();
				}
			}
		}
		//
		///<summary>Indicates if the DEBUG preprocessor flag is turned on.</summary>
		public static bool IsDebug {
			get {
#if DEBUG
				return true;
#else
				return false;
#endif
			}
		}
		//
		//
		///<summary>Flush the output buffer to file.</summary>
		public static void Flush() {
			lock ( _Lock ) {
				bool flush = _FlushOn;
				try {
					_FlushOn = true;
					Info( "---Log flushed---" );
				} catch ( Exception ex ) {
					Log.Exception( "Log.Flush", ex );
				} finally {
					_FlushOn = flush;
				}
			}
		}
		//
		//
		///<summary>Trace listener delegate. Applications can register their own to listen to trace output.</summary>
		public delegate void Listener( DateTime time, string thread, Type type, string message, string filename );
		//
		//
		///<summary>Attach Listener. </summary>
		public static void AttachB( Listener listener ) {
			lock ( _Lock ) {
				_Listener = listener;
			}
		}
		///<summary>Detach Listener. </summary>
		public static void Detach() {
			lock ( _Lock ) {
				_Listener = null;
			}
		}
		//
		///<summary>Data structure for keeping the information provided by ListenerB. Not used internally. Only for clients of this library.</summary>
#pragma warning disable CS0169 // Xxx is never used
		public class RecordB { DateTime Time; string Thread; Type Type; string message; string Filename; }
#pragma warning restore CS0169
		//
		//
		///<summary>Just do nothing. Can be used for setting breakpoints or do-nothing callbacks.</summary>
		[MethodImpl( MethodImplOptions.NoOptimization | MethodImplOptions.NoInlining )]
		public static void NOP() { }
		//
		//
		///<summary>Prevent automatic breaking into debugger. This is meant to be used during interactive debugging, to turn off breaks because of exceptions or other errors.</summary>
		public static void NoHalt() {
			IsHalt = false;
		}
		//
		//
		///<summary>Prevent output to Trace.</summary>
		public static void NoTrace() {
			ToTrace = false;
		}
		//
		///<summary>Break into debugger, only if one is attached.</summary>
		public static void DebugBreak( bool expression = true ) {
			if ( _DoHalt && expression ) {
				WriteLocked( null ); // Flush
				if ( Debugger.IsAttached ) {
					Debugger.Break();
				} else {
					WriteLocked( "DebugBreak" );
				}
			}
		}
		//
		///<summary>If the LogBreak flag is set, break into the debugger
		/// or start the debugger if it is not already attached.
		/// This is meant to allow breakpoints in startup code,
		/// before debugger can be attached.</summary>
		private static void CheckLogBreak() {
			if ( _DoBreak ) {
				_Write( Type.Verbose, "LogBreak is turned on. Trying to break into the debugger." );
				WriteLocked( null ); // Flush
				if ( Debugger.IsAttached ) {
					Debugger.Break();
				} else {
					Debugger.Launch();
				}
			}
		}
		//
		///<summary>Write header trace.</summary>
		public static void Header() {
			// Show assembly information
			var entry = AppBase;
			var executing = AssemblyLocation;
			var build = "RELEASE";
#if DEBUG
			build = "DEBUG";
#endif
			_Write( Type.Verbose, Format( "Starting", "build", build, "entry", entry, "executing", executing ) );
		}
		//
		///<summary>Create our date header: 2018-10-24 11:51:26.363 </summary>
		public static string DateHeader( DateTime dt ) {
			var y = dt.Year;
			var m = dt.Month;
			var d = dt.Day;
			var h = dt.Hour;
			var n = dt.Minute;
			var s = dt.Second;
			var z = dt.Millisecond;
			var ca = new char[] {
				// Date
				(char) ( y / 1000 + '0' ),
				(char) ( y / 100 % 10 + '0' ),
				(char) ( y / 10 % 10 + '0' ),
				(char) ( y % 10 + '0' ),
				'-',
				(char) ( m / 10 + '0' ),
				(char) ( m % 10 + '0' ),
				'-',
				(char) ( d / 10 + '0' ),
				(char) ( d % 10 + '0' ),
					' ',
				// Time
				(char) ( h / 10 + '0' ),
				(char) ( h % 10 + '0' ),
				':',
				(char) ( n / 10 + '0' ),
				(char) ( n % 10 + '0' ),
				':',
				(char) ( s / 10 + '0' ),
				(char) ( s % 10 + '0' ),
				'.',
				(char) ( z / 100 + '0' ),
				(char) ( z / 10 % 10 + '0' ),
				(char) ( z % 10 + '0' )
			};
			return new string( ca );
		}
		//
		//
		///<summary>Set minimum scope ("XE"). Meant to be used during interactive debugging.</summary>
		public static void MinScope() {
			Scope = GetScope( "XE" );
			CallerScope = GetScope( "XE" );
		}
		//
		///<summary>Check if the current trace settings allow output of the given type</summary>
		public static bool Can( Type type ) {
			var b = ( (byte) type ) - 1;
			return ( _Scope & ( 1u << b ) ) > 0;
		}
		//
		///<summary>Check if the current trace settings allow caller information for the given type</summary>
		public static bool CanCaller( Type type ) {
			return ( _CallerScope & ( 1u << (byte) type ) ) > 0;
		}
		//
		///<summary>Check if the current trace settings allow output of exceptions.</summary>
		public static bool CanException {
			get {
				return Can( Type.Exception );
			}
		}
		///<summary>Check if the current trace settings allow output of errors traces.</summary>
		public static bool CanError {
			get {
				return Can( Type.Error );
			}
		}
		///<summary>Check if the current trace settings allow output of warning traces.</summary>
		public static bool CanWarning {
			get {
				return Can( Type.Warning );
			}
		}
		///<summary>Check if the current trace settings allow output of information traces.</summary>
		public static bool CanInfo {
			get {
				return Can( Type.Info );
			}
		}
		///<summary>Check if the current trace settings allow output of memory traces.</summary>
		public static bool CanMemory {
			get {
				return Can( Type.Memory );
			}
		}
		///<summary>Check if the current trace settings allow output of verbose traces.</summary>
		public static bool CanVerbose {
			get {
				return Can( Type.Verbose );
			}
		}
		///<summary>Check if the current trace settings allow output of entry traces.</summary>
		public static bool CanEntry {
			get {
				return Can( Type.Entry );
			}
		}
		///<summary>Check if the current trace settings allow output of exit traces.</summary>
		public static bool CanExit {
			get {
				return Can( Type.Exit );
			}
		}
		///<summary>Check if the current trace settings allow output of data traces.</summary>
		public static bool CanData {
			get {
				return Can( Type.Data );
			}
		}
		///<summary>Check if the current trace settings allow message box dialogs. (Currently, message dialogs are not supported.)</summary>
		public static bool CanShowMessage {
			get {
				return Can( Type.ShowMessage );
			}
		}
		///<summary>Check if the current trace settings allow message timing traces.</summary>
		public static bool CanTime {
			get {
				return Can( Type.Time );
			}
		}
		///<summary>Check if the current trace settings allow success traces.</summary>
		public static bool CanSuccess {
			get {
				return Can( Type.Success );
			}
		}
		///<summary>Check if the current trace settings allow literal traces.</summary>
		public static bool CanLiteral {
			get {
				return Can( Type.Literal );
			}
		}
		//
		//
		///<summary>Write literal message to all active targets (File, System.Diagnostics.Trace, System.Console), but not to the custom listeners.</summary>
		private static void _Literal( Type type, string message ) {
			try {
				if ( _Linear ) {
					if ( message is not null ) {
						// Will only create a new string, if modified!
						message = message.Replace( '\r', AltCarriageReturn );
						message = message.Replace( '\n', AltLineFeed );
					}
				}
				if ( _ToFile ) {
					WriteLocked( message );
				}
				//
				if ( message is not null && 0 < message.Length ) {
					// Optionally write to Trace.
					if ( _ToTrace ) {
						Trace.WriteLine( message );
					}
					// Optionally write to console.
					if ( _ToConsole ) {
						switch ( type ) {
							case Type.None:
								break;
							case Type.Exception:
								Console.ForegroundColor = ConsoleColor.Red;
								break;
							case Type.Error:
								Console.ForegroundColor = ConsoleColor.Red;
								break;
							case Type.Warning:
								Console.ForegroundColor = ConsoleColor.Yellow;
								break;
							case Type.Info:
								Console.ForegroundColor = ConsoleColor.Blue;
								break;
							default:
								Console.ForegroundColor = ConsoleColor.Green;
								break;
						}
						Console.WriteLine( message );
					}
				}
			} catch ( Exception ) {
				DebugBreak();
			}
		}
		//
		//
		///<summary>Get Thread name (4 chars).</summary>
		public static string ThreadName() {
			// Get the current thread name
			var thread = Thread.CurrentThread;
			return HexEx.ToHex( thread.ManagedThreadId, 4 );
		}
		//
		//
		///<summary>Write message prefix and message, without filtering. Also write to listeners, if attached and enabled.</summary>
		private static void _Write( Type type, string message ) {
			// Get the current thread name
			var thread = Thread.CurrentThread;
			var tid = HexEx.ToHex( thread.ManagedThreadId, 4 );
			// Format the complete message
			var now = DateTime.UtcNow;
			var NOW = DateHeader( now );
			var sa = new string[] { NOW, tid, SCOPECODES[ (int) type ], message };
			var full_message = String.Join( CS.SP, sa );
			// Write the message
			_Literal( type, full_message );
			// Optionally call listeners.
			if ( _ToListener ) {
				if ( _Listener is not null ) {
					_Listener.Invoke( now, tid, type, message, _FileName );
				}
			}
			// Break for problematic messages
			if ( Type.Warning >= type ) DebugBreak();
		}
		//
		//
		///<summary>Write message to all outputs, if it matches the Scope filter.</summary>
		public static void Write( Type type, string message ) {
			if ( Can( type ) ) _Write( type, message );
		}
		//
		//
		///<summary>Write message and name/value pairs to all outputs, if Type matches the Scope filter.</summary>
		public static void WriteV( Type type, string message, params object[] values ) {
			if ( Can( type ) ) _Write( type, Format( message, values ) );
		}
		//
		//
		///<summary>Write message and caller information to log stream, if Type matches the Scope filter. And caller information is only written if Type matches the CallerScope.</summary>
		public static void Write( Type type, string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			if ( member is not null ) {
				if ( CanCaller( type ) ) {
					file = Path.GetFileName( file );
					var sa = new string[] { message ?? CS.Empty, CS.SP, CS.B1, CS.SP, member, CS.SP, file, CS.SP, CS.DT, line.ToString(), CS.SP, CS.B2 };
					message = String.Join( null, sa );
				}
			}
			if ( Can( type ) ) _Write( type, message );
		}
		//
		//
		///<summary>Utility function for formatting any enumerable.</summary>
		public static string FormatArray( object v ) {
			// Check incoming argument
			if ( v is not null ) {
				if ( v is IEnumerable ) {
					var a = v as IEnumerable;
					if ( a is not null ) {
						// Format all values
						string s;
						var first = true;
						var sb = new StringBuilder();
						foreach ( var item in a ) {
							s = item is null ? CS.null_ : item.ToString();
							if ( first ) first = false; else sb.Append( CC.CM );
							sb.Append( s );
						}
						return sb.ToString();
					}
				}
			}
			return CS.QM;
		}
		//
		//
		///<summary>Build a name=value list from the arguments.</summary>
		public static string Format( string message, params object[] values ) {
			// Guards
			if ( values is null ) return message;
			// Compute total length, including separators
			var total = 0;
			if ( message is not null ) {
				total += message.Length + 2;
			}
			string s; object v; int length = values.Length;
			for ( int i = 0; i < length; i++ ) {
				v = values[ i ];
				if ( v is null ) {
					values[ i ] = s = CS.null_;
				} else {
					if ( v is bool ) {
						s = ( (bool) v ? CS.T : CS.F ); ;
						values[ i ] = s;
					} else if ( v is string ) {
						s = (string) v;
					} else if ( v is DateTime ) {
						s = ( (DateTime) v ).YmdHms();
						values[ i ] = s;
					} else if ( v is IEnumerable ) {
						s = FormatArray( v );
						values[ i ] = s;
					} else if ( v is AggregateException ae ) {
						s = ae.ToString();
						foreach ( var item in ae.InnerExceptions ) {
							s += item.ToString();
						}
						values[ i ] = s;
					} else if ( v is Exception ex ) {
						s = v.ToString();
						values[ i ] = s;
					} else {
						s = v.ToString();
						values[ i ] = s;
					}
				}
				total += s.Length + 2;
			}
			// Allocate target array
			//Span<char> ca = stackalloc char[ total ]; int ci = 0;
			var ca = new char[ total ]; int ci = 0;
			// Append message and separator
			if ( message is not null ) {
				// Copy all characters
				for ( int i = 0, n = message.Length; i < n; i++ ) {
					ca[ ci++ ] = message[ i ];
				}
				ca[ ci++ ] = CC.CL;
				ca[ ci++ ] = CC.SP;
			}
			if ( 0 < length ) {
				// Append all values
				int j = 0;
				while ( j < length ) {
					// Get the current value
					s = (string) values[ j++ ];
					if ( s is not null ) {
						// Copy all characters
						for ( int i = 0, n = s.Length; i < n; i++ ) {
							ca[ ci++ ] = s[ i ];
						}
						if ( 1 == ( j & 1 ) ) {
							ca[ ci++ ] = CC.CL;
						}
						ca[ ci++ ] = CC.SP;
					}
				}
			}
			return new string( ca, 0, ci - 1 );
		}
		//
		///<summary>Build a name=value list from the arguments.</summary>
		public static string VarVal( params object[] values ) {
			return Format( null, values );
		}
		//
		///<summary>Remove characters from string.</summary>
		public static string RemoveCharacters( string v, params char[] unwanted ) {
			if ( v is null ) return null;
			if ( unwanted is null ) return v;
			var sa = v.Split( unwanted );
			return string.Join( string.Empty, sa );
		}
		//
		///<summary>Trim string argument to given length, optionally remove linebreaks.</summary>
		public static string Trim( string value, int length, bool cleanCrLf ) {
			if ( value is null ) return null;
			if ( 0 == value.Length ) return value;
			if ( length > 0 ) value = value.Substring( 0, Math.Min( value.Length, length ) );
			if ( cleanCrLf ) value = RemoveCharacters( value, CC.CR, CC.LF );
			return value;
		}
		//
		//
		///<summary>Format a value with a fixed number of digits.</summary>
		public static string Pad( long v, int digits, char separator = char.MinValue ) {
			int negative = 0;
			if ( v < 0 ) {
				negative = 1;
				v = Math.Abs( v );
			}
			var source = v.ToString();
			var len = source.Length;
			int max = len;
			if ( max < digits ) {
				max = digits;
			}
			var sep = char.MinValue != separator;
			if ( sep ) {
				var groups = max / 3;
				max += groups;
			}
			max += negative;
			// Allocate and fill array
			var ca = new char[ max ];
			var w = max - 1;
			var r = len - 1;
			var x = 0;
			while ( w > -1 ) {
				if ( r > -1 ) {
					ca[ w-- ] = source[ r-- ];
				} else {
					ca[ w-- ] = CC.N0;
				}
				x++;
				if ( sep ) {
					if ( w > 0 && 0 == ( 1 + x ) % 4 ) {
						ca[ w-- ] = CC.UL;
						x++;
					}
				}
			}
			if ( 0 != negative ) ca[ 0 ] = CC.HY;
			return new string( ca );
		}
		//
		///<summary>Format a value with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string Pad( int v, int digits, char separator = char.MinValue ) {
			return Pad( (long) v, digits, separator );
		}
		//
		///<summary>Format a value with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string Pad( uint v, int digits, char separator = char.MinValue ) {
			return Pad( (long) v, digits, separator );
		}
		//
		///<summary>Format a value with a fixed number of digits (between 1 and 9).</summary>
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		public static string Pad( ulong v, int digits, char separator = char.MinValue ) {
			return Pad( (long) v, digits, separator );
		}
		//
		///<summary>Create padding / indentation string from X copies of V.</summary>
		public static string Pad( int x, string v = CS.SP ) {
			if ( v is not null ) {
				if ( 0 < x ) {
					var L = v.Length;
					if ( 0 < L ) {
						var sb = new StringBuilder( L * x );
						for ( int i = 0; i < x; i++ ) {
							sb.Append( v );
						}
						return sb.ToString();
					}
				}
			}
			return CS.Empty;
		}
		//
		//
		//
		private static string _Delta = " D ";
		//
		//
		///<summary>Write memory trace. Format: " M 0041451520 | 0001150976 | This is the comment".</summary>
		public static void Memory( string message ) {
			if ( Can( Type.Memory ) ) {
				if ( _Process is not null ) {
					lock ( _Lock ) {
						_Process.Refresh();
						var ws = _Process.WorkingSet64;
						//var vm = myProcess.VirtualMemorySize64;
						var hc = _Process.HandleCount;
						var d1 = ws - _WorkingSet;
						//var d2 = vm - _VirtualMemory;
						var d3 = hc - _HandleCount;
						_WorkingSet = ws;
						//_VirtualMemory = vm;
						_HandleCount = hc;
						//
						var sa = new string[] {
							" WS ", Pad( ws, 12, CC.UL ),
							_Delta, Pad( d1, 10, CC.UL ),
							//" VM ", Pad( vm, 12, CC.UL ),
							//_Delta, Pad( d2, 10, CC.UL ),
							" HC ", Pad( hc, 6, CC.UL ),
							_Delta, Pad( d3, 4, CC.UL ),
							CS.SP, message };
						var s = String.Join( CS.Empty, sa );
						//
						_Write( Type.Memory, s );
					}
				} else {
					DebugBreak();
				}
			}
		}
		//
		//
		//
		//
		//
		// Character codes for timing information
		public static readonly char TimingBegin = 'B';
		public static readonly char TimingSplit = 'S';
		public static readonly char TimingLap = 'L';
		public static readonly char TimingFinal = 'F';
		//
		//
		///<summary>Write timing information</summary>
		public static void WriteTime( char indicator, string key, string message, uint delta ) {
			_Write( Type.Time, indicator + CS.SP + Log.Pad( delta, 10, CC.UL ) + CS.SP + ( key ?? CS.Empty ) + ( message is null ? CS.Empty : ( CS.ClSp + message ) ) );
		}
		//
		///<summary>Make sure that _Times is created.</summary>
		private static void CheckTimes() {
			if ( _Times is null ) {
				_Times = new Hashtable( 100 );
			}
		}
		//
		///<summary>For programmatic time measurements: Store start time for a key.</summary>
		public static void StartTime( string key, bool show = false ) {
			if ( Log.Assert( key is not null ) ) {
				lock ( _Lock ) {
					CheckTimes();
					_Times[ key ] = Stopwatch.StartNew();
					if ( show && Log.CanTime ) {
						WriteTime( TimingBegin, key, null, 0 );
					}
				}
			}
		}
		///<summary>Write delta time trace. Time is reset if the reset flag is set. Format: " T X 0000012345 key: message".</summary>
		private static void ShowTime( char indicator, string key, bool reset, string message ) {
			if ( CanTime ) {
				if ( Log.Assert( key is not null ) ) {
					lock ( _Lock ) {
						CheckTimes();
						object temporary = _Times[ key ];
						if ( temporary is null ) {
							_Write( Type.Error, "Log.ShowTime: key not found:" + key );
						} else {
							Stopwatch stopwatch = (Stopwatch) temporary;
							WriteTime( indicator, key, message, (uint) stopwatch.ElapsedMilliseconds );
							if ( reset ) {
								_Times.Remove( key );
							}
						}
					}
				}
			}
		}
		///<summary>Write delta time trace. Time is not reset. Format: " T key . 0000012345 message".</summary>
		public static void SplitTime( string key, string message ) {
			if ( CanTime ) {
				if ( Log.Assert( key is not null ) ) {
					ShowTime( TimingSplit, key, false, message );
				}
			}
		}
		///<summary>Write delta time trace. Time is reset! Format: " T key ! 0000012345 message".</summary>
		public static void StopTime( string key, string message ) {
			if ( CanTime ) {
				if ( Log.Assert( key is not null ) ) {
					ShowTime( TimingFinal, key, true, message );
				}
			}
		}
		//
		///<summary>Get the class or type name of this object.</summary>
		public static string TypeName( object o ) {
			return o.GetType().Name;
		}
		//
		///<summary>Get a formatted member name in the form 'ClassName.MethodName'.</summary>
		public static string MemberName( object o, [CallerMemberName] string value = null ) {
			var ct = o.GetType();
			var name = ct.Name;
			var sa = new string[] { name, value };
			var s = string.Join( CS.DT, sa );
			return s;
		}
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		[Obsolete]
		///<summary>Get the name of the calling method. (Identical to CM)</summary>
		public static string CallerMember( [CallerMemberName] string value = null ) { return value; }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Get the name of the calling method.</summary>
		public static string CM( [CallerMemberName] string value = null ) { return value; }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Get the class and member name of the calling method.</summary>
		public static string CCM( object o, [CallerMemberName] string value = null ) {
			string name = o?.GetType()?.Name;
			var a = new string[] { name, value };
			return String.Join( CS.DT, a );
		}
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Get the source file name of the calling method.</summary>
		public static string CallerFile( [CallerFilePath] string value = null ) { return value; }
		//
		[MethodImpl( MethodImplOptions.AggressiveInlining )]
		///<summary>Get the source file line number of the calling method.</summary>
		public static string CallerLine( [CallerLineNumber] int value = 0 ) { return value.ToString(); }
		//
		//
		//
		///<summary>If the expression is false, show message and caller information.</summary>
#if CS_NET_CORE
		public static bool Assert( bool expression, [CallerArgumentExpression( "expression" )] string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#else
		public static bool Assert( bool expression, string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#endif
			if ( !expression ) {
				var msg = "ASSERTION failed";
				if ( message is not null ) msg += CS.ClSp + message;
				Log.Error( msg, member, file, line );
			}
			return expression;
		}
		//
		///<summary>If the expression is false, set ok=false, show message and caller information.</summary>
#if CS_NET_CORE
		public static bool Assert( ref bool ok, bool expression, [CallerArgumentExpression( "expression" )] string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#else
		public static bool Assert( ref bool ok, bool expression, string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#endif
			if ( Assert( expression, message, member, file, line ) ) return true;
			ok = false;
			return false;
		}
		///<summary>If the expression is false, show class name and error message.</summary>
		public static bool Assert( bool expression, string classname, string message ) {
			if ( !expression ) {
				Log.ErrorV( "ASSERTION failed", classname + CS.CL + CS.SP + message );
			}
			return expression;
		}
		///<summary>If the expression is false, show formatted name=value information.</summary>
		public static bool AssertV( bool expression, params object[] values ) {
			if ( !expression ) {
				Log.ErrorV( "ASSERTION failed", values );
			}
			return expression;
		}
		//
		// TASK: [CallerArgumentExpression("condition")] string message = null
		///<summary>If the expression is true, show error message and caller information.</summary>
#if CS_NET_CORE
		public static bool Deny( bool expression, [CallerArgumentExpression( "expression" )] string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#else
		public static bool Deny( bool expression, string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#endif
			return !Assert( !expression, message, member, file, line );
		}
		///<summary>If the expression is true, show class name and error message.</summary>
		public static bool Deny( bool expression, string classname, string message ) {
			return !Assert( !expression, classname, message );
		}
		///<summary>If the expression is true, show formatted name=value information.</summary>
		public static bool DenyV( bool expression, params object[] values ) {
			return !AssertV( !expression, values );
		}
		//
		///<summary>Trigger an assertion. Show message and caller information.</summary>
		public static bool Fail( string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			return Assert( false, message, member, file, line );
		}
		///<summary>Trigger an assertion. Show message and formatted name=value information.</summary>
		public static bool FailV( string message = null, params object[] values ) {
			return AssertV( false, message, values );
		}
		//
		//
		[Conditional( "DEBUG" )]
		///<summary>If the expression is false, show error message and caller information.</summary>
#if CS_NET_CORE
		public static void DebugAssert( bool expression, [CallerArgumentExpression( "expression" )] string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#else
		public static void DebugAssert( bool expression, string message = null, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
#endif
			Assert( expression, message, member, file, line );
		}
		[Conditional( "DEBUG" )]
		///<summary>If the expression is false, show formatted name=value information.</summary>
		public static void DebugAssertV( bool expression, params object[] values ) {
			AssertV( expression, values );
		}
		//
		//
		//
		//
		///<summary>Show error message and caller information.</summary>
		public static void Error( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Error, message, member, file, line );
		}
		///<summary>Show error message and formatted name=value information.</summary>
		public static void ErrorV( string message, params object[] values ) {
			if ( Can( Type.Error ) ) _Write( Type.Error, Format( message, values ) );
		}
		//
		///<summary>Show warning message and caller information.</summary>
		public static void Warning( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Warning, message, member, file, line );
		}
		///<summary>Show morning message and formatted name=value information.</summary>
		public static void WarningV( string message, params object[] values ) {
			if ( Can( Type.Warning ) ) _Write( Type.Warning, Format( message, values ) );
		}
		//
		///<summary>Show informational message and caller information.</summary>
		public static void Info( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Info, message, member, file, line );
		}
		///<summary>Show informational message and formatted name=value information.</summary>
		public static void InfoV( string message, params object[] values ) {
			if ( Can( Type.Info ) ) _Write( Type.Info, Format( message, values ) );
		}
		//
		///<summary>Show verbose message and caller information.</summary>
		public static void Verbose( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Verbose, message, member, file, line );
		}
		///<summary>Show verbose message and formatted name=value information.</summary>
		public static void VerboseV( string message, params object[] values ) {
			if ( Can( Type.Verbose ) ) _Write( Type.Verbose, Format( message, values ) );
		}
		//
		///<summary>Show function entry message and caller information.</summary>
		public static void Entry( [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Entry, CS.Empty, member, file, line );
		}
		///<summary>Show function entry message and formatted name=value information.</summary>
		public static void EntryV( string message, params object[] values ) {
			if ( Can( Type.Entry ) ) _Write( Type.Entry, Format( message, values ) );
		}
		//
		///<summary>Show function exit message and caller information.</summary>
		public static void Exit( [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Exit, CS.Empty, member, file, line );
		}
		///<summary>Show function exit message and formatted name=value information.</summary>
		public static void ExitV( string message, params object[] values ) {
			if ( Can( Type.Exit ) ) _Write( Type.Exit, Format( message, values ) );
		}
		//
		///<summary>Write missing message with caller information.</summary>
		public static void Missing( [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Entry, "MISSING: NOT IMPLEMENTED YET", member, file, line );
		}
		//
		///<summary>Write not-tested message with caller information.</summary>
		public static void NotTested( [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Entry, "NOT TESTED YET", member, file, line );
		}
		//
		///<summary>Show data message and caller information.</summary>
		public static void Data( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Data, message, member, file, line );
		}
		///<summary>Show data message and formatted name=value information.</summary>
		public static void DataV( string message, params object[] values ) {
			if ( Can( Type.Data ) ) _Write( Type.Data, Format( message, values ) );
		}
		//
		[Conditional( "DEBUG" )]
		///<summary>Show data message and caller information, only in DEBUG mode.</summary>
		public static void Debug( string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			Write( Type.Data, message, member, file, line );
		}
		///<summary>Show data message and formatted name=value information, only in DEBUG mode.</summary>
		[Conditional( "DEBUG" )]
		public static void DebugV( string message, params object[] values ) {
			if ( Can( Type.Data ) ) _Write( Type.Data, Format( message, values ) );
		}
		//
		///<summary>Write exception and caller information to Log as verbose message.</summary>
		public static void SmallException( Exception ex, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			if ( Can( Type.Verbose ) ) _Write( Type.Verbose, Format( "EXCEPTION: " + ex.Message, member, file, line ) );
		}
		public static void SmallExceptionV( string message, Exception ex, params object[] values ) {
			if ( Can( Type.Verbose ) ) _Write( Type.Verbose, Format( message + ": EXCEPTION: " + ex.Message, values ) );
		}
		//
		///<summary>Write the exception to the log stream. </summary>
		public static void Exception( Exception ex, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			if ( Can( Type.Exception ) ) _Write( Type.Exception, Format( member, file, line, ex ) );
		}
		///<summary>Write the message and the exception to the log stream. </summary>
		public static void Exception( string message, Exception ex, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			if ( Can( Type.Exception ) ) _Write( Type.Exception, Format( message, member, file, line, ex ) );
		}
		///<summary>Show exception information message and formatted name=value information.</summary>
		public static void ExceptionV( string message, Exception ex, params object[] values ) {
			if ( Can( Type.Exception ) ) _Write( Type.Exception, Format( message + CS.ClSp + ex.ToString(), values ) );
		}
		///<summary>Show exception information message and formatted name=value information.</summary>
		public static void ExceptionV( string classname, string message, Exception ex, params object[] values ) {
			if ( Can( Type.Exception ) ) _Write( Type.Exception, Format( classname + CS.DT + message + CS.ClSp + ex.ToString(), values ) );
		}
		//
		///<summary>Show timing information, message and formatted name=value information.</summary>
		public static void TimeV( uint delta, string message, params object[] values ) {
			if ( Can( Type.Time ) ) {
				var arguments = CS.Empty;
				if ( 0 < values.Length ) {
					arguments = VarVal( values );
				}
				WriteTime( TimingFinal, message, arguments, delta );
			}
		}
		///<summary>Write timing and caller information to the log stream.</summary>
		public static void Time( uint delta, string message, [CallerMemberName] string member = null, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0 ) {
			TimeV( delta, message, member, file, line.ToString() );
		}
		//
		///<summary>Show literal message.</summary>
		public static void Literal( string message ) {
			if ( Can( Type.Literal ) ) {
				_Literal( Type.Literal, message );
			}
		}
		///<summary>Write current logging configuration information to the log stream.</summary>
		public static void ShowConfiguration() {
			Log.InfoV( "Configuration",
				"Scope", Log.GetScope( Log.Scope ),
				"CallerScope", Log.GetScope( Log.CallerScope ),
				"Is",
				( Log.IsBreak ? "Break " : CS.Empty ) +
				( Log.IsDebug ? "Debug " : CS.Empty ) +
				( Log.IsFlush ? "Flush " : CS.Empty ) +
				( Log.IsHalt ? "Halt " : CS.Empty ) +
				( Log.Linear ? "Linear " : CS.Empty ),
				"To",
				( Log.ToConsole ? "Console " : CS.Empty ) +
				( Log.ToFile ? "File " : CS.Empty ) +
				( Log.ToListener ? "Listener " : CS.Empty ) +
				( Log.ToTrace ? "Trace " : CS.Empty ),
				"FilePath", FilePath,
				"AppBase", AppBase
			);
		}
	}
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	//
	///<summary>Automatic time tracing, like "using ( new LogTime( true ) ) { /* your code */ }"</summary>
	public class LogTime : IDisposable {
		// Basic member
		private string _title;
		private bool _memory;
		private uint _clock;
		private uint _last;
		// Caller Info
		private string _member;
		private string _source;
		private int _line;
		//
		///<summary>Get the Title.</summary>
		public string Title => _title;
		//
		///<summary>Get the Member name.</summary>
		public string Member => _member;
		//
		///<summary>Get the Source string.</summary>
		public string Source => _source;
		//
		///<summary>Get the Line value.</summary>
		public int Line => _line;
		//
		///<summary>Get the currently elapsed time in milliseconds.</summary>
		public uint Elapsed => Log.TickCount - _clock;
		//
		///<summary>Get the lap time since last call in milliseconds.</summary>
		public uint LapTime {
			get {
				var now = Log.TickCount;
				var result = now - _last;
				_last = now;
				return result;
			}
		}
		//
		//
		///<summary>Write LogTime related messages to the Log.</summary>
		private void _Write( char tag, string msg, uint delta, bool restart ) {
			var can_time = Log.CanTime;
			var can_memory = _memory && Log.CanMemory;
			//
			if ( can_time || can_memory ) {
				var sa = new string[] { _title, msg, _member, _source, CS.DT, _line.ToString() };
				var s = String.Join( CS.SP, sa );
				if ( can_time ) {
					Log.WriteTime( tag, null, s, delta );
				}
				if ( can_memory ) {
					Log.Memory( s );
				}
			}
			if ( restart ) {
				_last = _clock = Log.TickCount;
			}
		}
		//
		//
		///<summary>Initialize. Store initial information and start the timer.</summary>
		private void Init( string title, bool memory, string member, string source, int line, bool show ) {
			_title = title;
			_memory = memory;
			_last = _clock = Log.TickCount;
			_member = member;
			_source = Path.GetFileName( source );
			_line = line;
			if ( show ) {
				_Write( Log.TimingBegin, title, 0, false );
			}
		}
		//
		///<summary>Constructor with title and options.</summary>
		public LogTime(
			bool show = true,
			string title = null,
			bool memory = false,
			[CallerMemberName] string member = null,
			[CallerFilePath] string source = null,
			[CallerLineNumber] int line = 0 ) {
			// Initialize
			Init( title, memory, member, source, line, show );
		}
		//
		public void Dispose() {
			Final();
			GC.SuppressFinalize( this );
		}
		//
		///<summary>Write additional timing information. The timer is reset, when final is true.</summary>
		public void Write( Tri which, string msg = null ) {
			// Tri: First = 1, Current = 0, Last = -1,
			uint time = 0;
			if ( which == Tri.Current ) {
				time = LapTime;
			} else {
				time = Elapsed;
			}
			var indicator = which.Choose( Log.TimingSplit, Log.TimingLap, Log.TimingFinal );
			_Write( indicator, msg, time, which == Tri.Last );
		}
		//
		///<summary>Write additional timing information (split time).</summary>
		public void Split( string msg = null ) {
			Write( Tri.First, msg );
		}
		//
		///<summary>Write additional timing information (lap time).</summary>
		public void Lap( string msg = null ) {
			Write( Tri.Current, msg );
		}
		//
		///<summary>Write additional timing information (total time). Reset the timer.</summary>
		public void Final( string msg = null ) {
			Write( Tri.Last, msg );
		}
		//
		///<summary>Reset the timer.</summary>
		public void Reset() {
			_last = _clock = Log.TickCount;
		}
	}
}
