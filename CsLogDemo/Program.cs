﻿// Copyright (C) 1996 Cobasoft GmbH, Karl-W.Geitz. For details see licence.cs.
//
// API reference at: http://cobasoft.net/doc/Cobasoft.html
// GIT repository at: https://codeberg.org/cobasoft/lib.
//
using Cobasoft;
//
public class Program {
	//
	static void Main( string[] args ) {
		// Simple logging
		Log.VerboseV( Log.CM(), "Hello World" );
		// Various samples:
		ExceptionSample();
		TimingSample1();
		TimingSample2();
		AssertionSample1();
		AssertionSample2();
		AssertionSample3();
		ScopeSample1();
		ScopeSample2();
		CallerInfoSample();
	}
	//
	private static void ExceptionSample() {
		try {
			throw new Exception( "TEST" );
		} catch ( Exception ex ) {
			Log.Exception( ex );
		}
	}
	//
	private static void TimingSample1() {
		// New 'using' syntax:
		// Show execution duration of this method
		using var lt = new LogTime( false );
		Thread.Sleep( 100 );
	}
	//
	private static void TimingSample2() {
		// Show execution duration of this method
		// and intermediate measurement,
		// with the older 'using' syntax.
		using ( var lt = new LogTime( false ) ) {
			Thread.Sleep( 200 );
			lt.Lap( "intermediate lap time measurement" );
			lt.Split( "intermediate total time measurement" );
			Thread.Sleep( 200 );
		}
	}
	//
	private static void AssertionSample1() {
		// Show assertion in the log file
		// and break into the debugger (if present).
		Log.Assert( false );
	}
	//
	private static void AssertionSample2() {
		// Use assertions within IF
		var a = 123;
		if ( Log.Assert( 123 == a ) ) {
			// Send Info log information with
			// current method name, label and value.
			Log.InfoV( Log.CM(), "Good case", a );
		}
	}
	//
	private static void AssertionSample3() {
		var b = 345;
		if ( Log.DenyV( 345 == b ) ) {
			// Send Info log information with
			// current method name, label and value.
			Log.ErrorV( Log.CM(), "BAD case", b );
		}
	}
	//
	/*	ScopeCodes
		-	None
		X	Exception
		E	Error
		W	Warning
		I	Info
		M	Memory
		V	Verbose
		F	Entry
		G	Exit
		D	Data
		S	ShowMessage
		T	Time
		O	Success
	*/
	private static void ScopeSample1() {
		// Get current scope. (All API is thread-safe)
		var oldScope = Log.Scope;
		try {
			// Compute another, restricted scope
			var newScope = Log.GetScope( "XEWI" );
			// Set new scope
			Log.Scope = newScope;
			// Do something
			Log.Data( "Data: not shown with this scope" );
			Log.Verbose( "Verbose: not shown with this scope" );
			Log.Error( "Error: shown with this scope" );
		} finally {
			// Restore old scope
			Log.Scope = oldScope;
		}
		Log.Verbose( "Verbose: shown again" );
	}
	/*
		CallerScope determines if [CallerMemberName],
		[CallerFilePath] and [CallerLineNumber] 
		are being displayed, per Log.Type.
	*/
	private static void ScopeSample2() {
		// Get current caller info scope.
		var oldScope = Log.CallerScope;
		try {
			// Compute another, restricted scope
			var newScope = Log.GetScope( "XEWI" );
			// Set new scope
			Log.CallerScope = newScope;
			// Do something
			Log.Data( "Data: not caller info shown with this scope" );
			Log.Error( "Error: caller info shown with this scope" );
		} finally {
			// Restore old scope
			Log.CallerScope = oldScope;
		}
		Log.Verbose( "Verbose: shown again" );
	}
	//
	private static void CallerInfoSample() {
		// Most log-writing API methods come in two flavors:
		// Without trailing V: automatic caller info, but restricted arguments.
		Log.Verbose( "BAD case" );
		var value = 12345;
		// Can still write many arguments with Log.Format:
		Log.Verbose( Log.Format( "BAD case", "name", value ) );
		//
		// With trailing V:
		Log.VerboseV( "BAD case", "name", value );
		// So methods with V are usually simpler, but you can have always caller info:
		Log.VerboseV( Log.CM(), "name", value,
			"CCM", Log.CCM( new object() ), // only for testing. Usually 'this'
			"CallerFile", Log.CallerFile(),
			"CallerLine", Log.CallerLine() );
	}
}
