# Cobasoft.Log

Cobasoft.Log is an extremely fast, secure, reliable, efficient logging library.

We have been building and advancing these libraries for many years (with C, C++ and C#) 
and tested them in advanced applications with high data volume and near real-time requirements.

The [Cobasoft](cobasoft.net) logging library is constructed on some basic premises:
- Do not cause harm.
- Be as fast as possible.
- Allocate as little memory as possible.
- Allow logging during startup and termination.
- Enable troubleshooting regarding exceptions, memory and timing problems.

The [Cobasoft](cobasoft.net) logging library supports software engineers by:
- Requiring little ceremony while writing logging statements.
- Promoting assertions to improve software quality.
- Providing simple and powerful utilities for time measurement.

Cobasoft [LogVw](http://cobasoft.net/utl/logvw.zip) is the perfect companion for this library, because:
- It loads and processes gigabyte files quickly.
- It allows sophisticated analysis of log files by combination of multiple regular expressions.
- It can create various extractions from log files for further analysis.

## Installation

Just download the projects from [CodeBerg](https://codeberg.org/cobasoft/lib), 
then include the CsLog project into your solution.

Alternatively, the CsLog project can be built into a NuGet package, 
which then can be included into your projects.

The sources are compatible with Framework 4.8, 5.0 and 6.0.
When using Framework 4.8, please open Cobasoft.Log.csproj.
Otherwise use CsLog.csproj.

## Usage

[Demo source and configuration](http://cobasoft.net/doc/index.htm)

Cobasoft.Log is a static class, which has the advantages, that
is always available, uses minimal memory allocations and 
is available in all modules without ceremony.
So logging in lower-level modules can be written without
having to pass references to logger objects.
And there is no risk of logger pointers being null.

So, without further ado, you can directly use the logger.
One of the simplest statements would be:

	Log.VerboseV( Log.CM(), "Hello Word" );

This would give you the following line in the log file:

	2022-05-17 13:50:03.152 0001 V Main: Hello World

The line basically has 4 parts:
- Date and time in UTC format, including milliseconds.
- Current managed thread ID. You will find this useful during troubleshooting.
- [Scope](http://cobasoft.net/doc/index.htm): indicates logging output type.
- Text: "Main: Hello World".

Please note, that "Main" is the name of the function which contained the above statement.
It was inserted automatically by the "Log.CM()" method call.

Please have a look at [Demo Source](http://cobasoft.net/doc/index.htm)
for further examples.

## Time and Memory Measurements

Cobasoft.Log contains 2 variations of time measurement:
- Automatic measurement for code blocks ('using [LogTime](http://cobasoft.net/doc/Cobasoft.LogTime.html)').
- Indexed measurements across program functionality. ('[Log.StartTime](http://cobasoft.net/doc/Cobasoft.Log.html#Cobasoft_Log_StartTime_System_String_System_Boolean_)').

It also contains 2 variations of memory management:
- Automatic measurement for code blocks ('using [LogTime](http://cobasoft.net/doc/Cobasoft.LogTime.html)').
- Indexed measurements across program functionality. ('[Log.Memory](http://cobasoft.net/doc/Cobasoft.Log.html#Cobasoft_Log_Memory_System_String_)').

The Log.Memory method will show the current working set,
the difference to the last working set,
the current handle count
and the difference to the last handle count.

The automatic measurement based on [LogTime](http://cobasoft.net/doc/Cobasoft.LogTime.html)
is triggered by passing 'true' to a
[constructor](http://cobasoft.net/doc/Cobasoft.LogTime.html#Cobasoft_LogTime__ctor_System_Boolean_System_Boolean_System_String_System_String_System_Int32_)

LogTime (without memory) will give output like the following:

	2022-05-17 14:19:52.239 0001 T F 0_000_000_094 : TimingSample1   Program.cs . 36
	2022-05-17 14:19:52.442 0001 T S 0_000_000_203 : TimingSample2 intermediate measurement  Program.cs . 44
	2022-05-17 14:19:52.646 0001 T F 0_000_000_406 : TimingSample2   Program.cs . 44

All timing output has this structure:
- Scope indicator: 'T'.
- Measurement indicator: 'B' (Begin) ,'F' (Final) or 'S' (Split-time).
- Time used in milliseconds (thousands separated by underscore).
- Label as given in the program code.
- Additional information regarding function, source file and line number.

## Assertions

These are conceptually similar to [Eiffel preconditions](https://www.eiffel.org/doc/eiffelstudio/Precondition).

[Design-By-Contract](https://www.eiffel.com/values/design-by-contract/introduction/)
is a very helpful concept for creating stable, performant software systems,
which are resilient to change and detect problems and errors early.

The assertions of Cobasoft.Log are highly efficient, so they can be used freely within applications.
There are basically 2 Methods:
[Log.Assert](http://cobasoft.net/doc/Cobasoft.Log.html#Cobasoft_Log_Assert_System_Boolean_System_String_System_String_System_String_System_Int32_)
and [Log.Deny](http://cobasoft.net/doc/Cobasoft.Log.html#Cobasoft_Log_Deny_System_Boolean_System_String_System_String_System_String_System_Int32_).
Both have some variations, choose what you need.

While developing applications, you will sprinkle your sources with appropriate assertions.
If then anything goes wrong during debugging, you will be notified in the debugger.
In production, without debugger, failed assertions only will appear in the log output (file).
By default the users will not be notified.

Have a look at the [Demo source](http://cobasoft.net/doc/index.htm) to see some examples.

If there are too many notifications while debugging, you can turn them off by running
'Log.NoHalt()' in the Immediate window. 
Alternatively you can turn them off by modifying the configuration
or temporarily for certain code blocks.


## Output Targets

By default, there are these output targets: File, Trace, Console and Listener.
- File output can be limited in size and there's an optional file age limit.
- Trace output goes to the .NET Trace class, which basically calls
 [OutputDebugString](https://docs.microsoft.com/en-us/windows/win32/api/debugapi/nf-debugapi-outputdebugstringw).
This is rather slow and should not turned on in production.
- Console goes to the .NET Console class. 
This is most useful for console applications and also might be redirected.
but it's also somewhat slower than File.
- Listeners can be implemented by your application.
 There is a simpler one: [ListenerA](http://cobasoft.net/doc/Cobasoft.Log.ListenerA.html).
 And a more powerful one: [ListenerB](http://cobasoft.net/doc/Cobasoft.Log.ListenerB.html).

### Listeners for UI interaction or DB logging

You may implement listeners which display Log messages to the screen, 
write them into a database table or send them over the network.
The log scopes may then be used for UI formatting or filtering, as appropriate.


## Config

You might consider using our [Configuration API](http://cobasoft.net/doc/Cobasoft.Config.html).
It is also very efficient and easy to use.
Besides it covers the differences between Framework 4.8 and the DotNetCore based frameworks.


## License

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL COBASOFT GMBH BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of Cobasoft GmbH shall
not be used in advertising or otherwise to promote the sale, use or
other dealings in this Software without prior written authorization
from Cobasoft GmbH.

"cobasoft" is a registered trademark of Cobasoft GmbH.
Registration number: 302009016225 Deutsches Patent-und Markenamt.

Copyright (C) 1996 Cobasoft GmbH, Karl-W.Geitz
